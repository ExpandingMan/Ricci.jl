
abstract type Diffeomorphism{M<:Manifold} end

_output_with(𝒻::Diffeomorphism, x::Number) = x
_output_with(𝒻::Diffeomorphism, ::Nothing) = nothing
_output_with(𝒻::Diffeomorphism{M}, p′::Point{M}) where {M} = TransformedPoint(𝒻, p′)
_output_with(𝒻::Diffeomorphism{M}, u′::Tensor{M}) where {M} = TransformedTensor(𝒻, u′)

# I'm a bit dubious about this, essentially the idea is this is some kind of projection that we
# don't need to transform back
_output_with(𝒻::Diffeomorphism{M}, p′::Point{N}) where {M,N} = p′

_output_with(𝒻::Diffeomorphism, tpl::Tuple) = ntuple(j -> _output_with(𝒻, tpl[j]), Val(length(tpl)))
_output_with(𝒻::Diffeomorphism{M}, a::At{M}) where {M} = _map(t -> _output_with(𝒻, t), a)

# this is default, free to define more efficient specialized methods
_applyinv(𝒻::Diffeomorphism{M}, p::Point{M}) where {M} = _apply(inv(𝒻), p)

# for god knows what reason the compiler is losing type stability here if I do this generically
with(proc, 𝒻::Diffeomorphism, a)  = _output_with(𝒻, proc(_apply(𝒻, a)))
with(proc, 𝒻::Diffeomorphism, a, b) = _output_with(𝒻, proc(_apply(𝒻, a), _apply(𝒻, b)))
with(proc, 𝒻::Diffeomorphism, a, b, c) = _output_with(𝒻, proc(_apply(𝒻, a), _apply(𝒻, b), _apply(𝒻, c)))
with(proc, 𝒻::Diffeomorphism, a, b, c, d) = _output_with(𝒻, proc(_apply(𝒻, a), _apply(𝒻, b), _apply(𝒻, c), _apply(𝒻, d)))
with(proc, 𝒻::Diffeomorphism, args...) = _output_with(𝒻, proc(map(a -> _apply(𝒻, a), args)))

_apply(𝒻::Diffeomorphism{M}, a::At{M}) where {M} = _map(t -> _apply(𝒻, t), a)
_applyinv(𝒻::Diffeomorphism{M}, a::At{M}) where {M} = _map(t -> _applyinv(𝒻, t), a)

# this should be considered private
struct NoApply{T}
    value::T
end

Base.getindex(x::NoApply) = x.value

_apply(𝒻::Diffeomorphism, x::NoApply) = x[]


struct TransformedPoint{M<:Manifold,D<:Diffeomorphism{M},P<:Point{M},A<:Atlas{M}} <: Point{M}
    diffeo::D
    point::P

    function TransformedPoint(𝒻::Diffeomorphism{M}, p::Point{M}) where {M}
        new{M,typeof(𝒻),typeof(p),typeof(Atlas(p))}(𝒻, p)
    end
end

Atlas(::Type{<:TransformedPoint{M,D,P,A}}) where {M,D,P,A} = A()

(𝒻::Diffeomorphism{M})(p::Point{M}) where {M} = TransformedPoint(𝒻, _apply(𝒻, p))

with(proc, p′::TransformedPoint, args...) = with(proc, p′.diffeo, NoApply(p′.point), args...)

Base.getindex(p′::TransformedPoint) = _applyinv(p′.diffeo, p′.point)

data(p′::TransformedPoint) = data(p′[])

datatype(::Type{<:TransformedPoint{M,D,P}}) where {M,D,P} = datatype(P)

# invariant that's free to use transformed
distance(p′::TransformedPoint{M,D}, q′::TransformedPoint{M,D}) where {M,D} = distance(p′.point, q′.point)


struct TransformedTensor{M<:Manifold,r,s,D<:Diffeomorphism{M},T<:Tensor{M,r,s},R<:Representation{M}} <: Tensor{M,r,s}
    diffeo::D
    tensor::T

    function TransformedTensor(𝒻::Diffeomorphism{M}, u::Tensor{M,r,s}) where {M,r,s}
        new{M,r,s,typeof(𝒻),typeof(u),typeof(Representation(u))}(𝒻, u)
    end
end

Representation(::Type{<:TransformedTensor{M,r,s,D,T,R}}) where {M,r,s,D,T,R} = R()

const TransformedVector{M,D,T,R} = TransformedTensor{M,1,0,D,T,R}
const TransformedOneForm{M,D,T,R} = TransformedTensor{M,0,1,D,T,R}

(𝒻::Diffeomorphism{M})(u::Tensor{M,r,s}) where {M,r,s} = TransformedTensor(𝒻, _apply(𝒻, u))

(𝒻::Diffeomorphism{M})(ts::At{M}) where {M} = _map(𝒻, ts)

with(proc, u′::TransformedTensor, args...) = with(proc, u′.diffeo, NoApply(u′.tensor), args...)
with(proc, u::Tensor, args...)  = with(proc, comoving(u), u, args...)

Base.getindex(u::Tensor) = u
Base.getindex(u′::TransformedTensor) = _applyinv(u′.diffeo, u′.tensor)

Point(u′::TransformedTensor) = _applyinv(u′.diffeo, Point(u′.tensor))

data(u′::TransformedTensor) = data(u′[])

Base.:(*)(a::Number, u′::TransformedTensor) = with(u -> a*u, u′)

# invariant
square(u′::TransformedTensor) = square(u′.tensor)

Base.exp(int::SymplecticIntegrator, u::TransformedVector, s::Number=One(Float)) = with(u -> exp(int, u, s), u′)
Base.exp(u::TransformedVector, s::Number=One(Float)) = with(u -> exp(u, s), u′)

function paralleltransport(int::SymplecticIntegrator, u′::TransformedVector, s::Number=one(Float))
    with(u -> paralleltransport(int, u, s), u′)
end
function paralleltransport(u′::TransformedVector, s::Number=one(Float))
    with(u -> paralleltransport(u, s), u′)
end

struct IdentityDiffeomorphism{M} <: Diffeomorphism{M} end

Base.inv(𝒻::IdentityDiffeomorphism{M}) where {M} = 𝒻

_apply(𝒻::IdentityDiffeomorphism{M}, p::Point{M}) where {M} = p
_apply(𝒻::IdentityDiffeomorphism{M}, u::Tensor{M}) where {M} = u


struct InverseDiffeomorphism{M,D<:Diffeomorphism{M}} <: Diffeomorphism{M}
    diffeo::D
end

Base.inv(𝒻::Diffeomorphism) = InverseDiffeomorphism(𝒻)

# larger composites can be created by chaining these
struct CompositeDiffeomorphism{M,D1<:Diffeomorphism{M},D2<:Diffeomorphism{M}} <: Diffeomorphism{M}
    diffeo1::D1
    diffeo2::D2
end

# it's so long, let's allow a shorthand
const InvDiff{M,D} = InverseDiffeomorphism{M,D}
const CompDiff{M,D1,D2} = CompositeDiffeomorphism{M,D1,D2}

Base.inv(𝒻::InvDiff) = 𝒻.diffeo

# fallbacks, can be extended
_apply(𝒻::InvDiff, p::Point) = _applyinv(𝒻.diffeo, p)
_apply(𝒻::InvDiff, u::Tensor) = _applyinv(𝒻.diffeo, u)

_applyinv(𝒻::InvDiff, p::Point) = _apply(𝒻.diffeo, p)
_applyinv(𝒻::InvDiff, u::Tensor) = _apply(𝒻.diffeo, u)

Base.:(∘)(𝒻2::Diffeomorphism{M}, 𝒻1::Diffeomorphism{M}) where {M} = CompDiff{M,typeof(𝒻1),typeof(𝒻2)}(𝒻1, 𝒻2)

_apply(𝒻::CompDiff{M}, p::Point{M}) where {M} = _apply(𝒻.diffeo2, _apply(𝒻.diffeo1, p))
_apply(𝒻::CompDiff{M}, u::Tensor{M}) where {M} = _apply(𝒻.diffeo2, _apply(𝒻.diffeo1, u))

_applyinv(𝒻::CompDiff{M}, p::Point{M}) where {M} = _applyinv(𝒻.diffeo1, _applyinv(𝒻.diffeo2, p))
_applyinv(𝒻::CompDiff{M}, u::Tensor{M}) where {M} = _applyinv(𝒻.diffeo1, _applyinv(𝒻.diffeo2, u))

Base.inv(𝒻::CompDiff) = inv(𝒻.diffeo1) ∘ inv(𝒻.diffeo2)

"""
    comoving(u::Vector)

Returns the diffeomorophism into the comoving (Gaussian normal) coordinates of the vector `u`.
"""
function comoving end

"""
    invcomoving(u::Vector)

Returns the diffeomorphism *out* of the comoving (Gaussian normal) coordinates of the vector `u`.
Here `u` is to be provided in the coordinates that the returned diffeomorphism transforms *into*.
This is guaranteed to be equivalent to `inv(comoving(u))` but may be more efficient in some cases.
"""
function invcomoving end

"""
    comovingwithinv(u::Vector)

Equivalent to
```julia
(comoving(u), invcomoving(u))
```
but may be more efficient in some cases.
"""
comovingwithinv(u::Vector) = (comoving(u), invcomoving(u))


"""
    coordtransform_ondata(atl′::Atlas, atl::Atlas, x::AbstractArray1, pidx::Integer=1)

Coordinate transform from coordinate atlas `atl` to atlas `atl′`.  `x` is coordinate data and
`pidx` is the patch index.  Defining a method of this function for a pair of atlases is sufficient
to implement the generic diffeomorphism [`CoordTransform`](@ref), however transformation matrices
can be supplied for more efficient computation of transformations on tensors.

Implementations of this function should return a tuple `(data, pidx)` where `data` is coordinate
data in the primed coordinates (typically a `StaticVector`) and `pidx` is the patch index in
the primed atlas.
"""
coordtransform_ondata(::A, ::A, x::AbstractArray1, pidx::Integer=1) where {A<:Atlas} = (x, pidx)

"""
    coordtransform_matrix(atl′::Atlas, atl::Atlas, x::AbstractArray1, pidx::Integer=1)

Computes the *contravariant* coordinate transform jacobian matrix for the transform from coordinates of
atlas `atl` to those of `atl′`.  This is the matrix which can be applied directly to vector data.
The coordinate data `x` and its associated patch index are in the coordinates of `atl`.  The covariant
matrix can be obtained by swapping `atl′` and `atl` and providing instead the transformed data.
Note that one must be careful of transposition in this case.

The default method uses automatic differentiation, `Atlas` implementations can define more
efficient methods.
"""
function coordtransform_matrix(diffbackend::AbstractADType, atl′::Atlas{M}, atl::Atlas{M}, x::AbstractArray1,
                               pidx::Integer=1) where {M}
    jacobian(ξ -> coordtransform_ondata(atl′, atl, ξ, pidx)[1], diffbackend, x)
end

function coordtransform_matrix(atl′::Atlas{M}, atl::Atlas{M}, x::AbstractArray1, pidx::Integer=1;
                               diffbackend::AbstractADType=AutoForwardDiff()) where {M}
    coordtransform_matrix(diffbackend, atl′, atl, x, pidx)
end

@stable function _coordtransform(atl::Atlas{M}, p::Point{M}) where {M}
    coordtransform_ondata(atl, Atlas(p), data(p), patchindex(p))
end


#TODO: document the semantics here a bit more...

"""
    CoordTransform{M,A′,A,P′,P} <: Diffeomorphism{M}

A data structure for representing a general coordinate transformation from atlas of type `A` to atlas
of type `A′`.  This assumes that these are the same as the atlases of the resulting representations,
i.e. despite its generality, the intended use case is for changing the representation of the data.
"""
struct CoordTransform{M<:Manifold,
        A′<:Atlas{M}, A<:Atlas{M},
        P′<:Point{M}, P<:Point{M},
    } <: Diffeomorphism{M}
end

function CoordTransform{M,A′,A}(
        ::Type{P′}=defaulttype(Point, A′), ::Type{P}=defaulttype(Point, A),
    ) where {M,A′<:Atlas{M},A<:Atlas{M},P′<:Point{M},P<:Point{M}}
    CoordTransform{M,A′,A,P′,P}()
end
function CoordTransform(::Type{A′}, ::Type{A},
        ::Type{P′}=defaulttype(Point, A′), ::Type{P}=defaulttype(Point, A),
    ) where {A′<:Atlas,A<:Atlas,P′<:Point,P<:Point}
    CoordTransform{Manifold(A′),A′,A}(P′, P)
end
function CoordTransform(atl′::Atlas, atl::Atlas,
        pt′::Type=defaulttype(Point, atl′), pt::Type=defaulttype(Point, atl),
    )
    CoordTransform(typeof(atl′), typeof(atl), pt′, pt)
end

toatlas(::Type{<:CoordTransform{M,A′,A}}) where {M,A′,A} = A′()
toatlas(𝒻::CoordTransform) = toatlas(typeof(𝒻))
fromatlas(::Type{<:CoordTransform{M,A′,A}}) where {M,A′,A} = A()
fromatlas(𝒻::CoordTransform) = fromatlas(typeof(𝒻))
tofromatlas(𝒻::CoordTransform) = (toatlas(𝒻), fromatlas(𝒻))

# we take the liberty of ignoring the origin atlas here as long as the transform is available
# this is because this method will be used by tensors which are allowed to have points
# on atlases different than their own data
@stable function _apply(𝒻::CoordTransform{M,A′,A,P′}, p::Point{M}) where {M,A′,A,P′}
    (x, pidx) = _coordtransform(toatlas(𝒻), p)
    P′(x, pidx)
end
@stable function _applyinv(𝒻::CoordTransform{M,A′,A,P′,P}, p′::Point{M}) where {M,A′,A,P′,P}
    (x, pidx) = _coordtransform(fromatlas(𝒻), p′)
    P(x, pidx)
end

# obviously it would be betterto have these come from generic code for tensors,
# but general tensors are fucked anyway and we'll need a reckoning

@stable function _apply(𝒻::CoordTransform{M}, u::Vector{M}) where {M}
    (atl′, atl) = tofromatlas(𝒻)
    Atlas(atl, Atlas(u))
    p = Point(u)
    p′ = _apply(𝒻, p)
    rep′ = Representation(atl′, BasisType(u))
    Λ = coordtransform_matrix(atl′, atl, data(p), patchindex(p))
    defaulttype(Vector, rep′)(p′, Λ*data(u))
end

@stable function _applyinv(𝒻::CoordTransform{M}, u′::Vector{M}) where {M}
    (atl′, atl) = tofromatlas(𝒻)
    Atlas(atl′, Atlas(u′))
    p′ = Point(u′)
    p = _applyinv(𝒻, p′)
    rep = Representation(atl, BasisType(u′))
    Λinv = coordtransform_matrix(atl, atl′, data(p′), patchindex(p′))
    defaulttype(Vector, rep)(p, Λinv*data(u′))
end

@stable function _apply(𝒻::CoordTransform{M}, u::OneForm{M}) where {M}
    (atl′, atl) = tofromatlas(𝒻)
    Atlas(atl, Atlas(u))
    p = Point(u)
    p′ = _apply(𝒻, p)
    rep′ = Representation(atl′, BasisType(u))
    Λ = coordtransform_matrix(atl, atl′, data(p′), patchindex(p′))
    defaulttype(OneForm, rep′)(p′, data(u)*Λ)
end

@stable function _applyinv(𝒻::CoordTransform{M}, u′::OneForm{M}) where {M}
    (atl′, atl) = tofromatlas(𝒻)
    Atlas(atl′, Atlas(u′))
    p′ = Point(u′)
    p = _applyinv(𝒻, p′)
    rep = Representation(atl, BasisType(u′))
    Λinv = coordtransform_matrix(atl′, atl, data(p), patchindex(p))
    defaulttype(OneForm, rep)(p, data(u′)*Λinv)
end

