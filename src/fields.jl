
abstract type FieldFunction{M} end

Manifold(::Type{<:FieldFunction{M}}) where {M} = M()
Manifold(ff::FieldFunction) = Manifold(typeof(ff))

"""
    FieldFuncOnPoint

Wrapper for a function indicating that its input signature is `f(::Point)`. See
[`FieldFuncOnData`](@ref) which provides a fully compatible API for a function that instead takes
data directly.
"""
struct FieldFuncOnPoint{M,F} <: FieldFunction{M}
    func::F

    FieldFuncOnPoint{M}(𝒻) where {M} = new{M,typeof(𝒻)}(𝒻)
end

(𝒻::FieldFuncOnPoint{M})(x::Point{M}) where {M} = 𝒻.func(x)

# this might look stupid but remember it's principly for compatibility
function (𝒻::FieldFuncOnPoint{M})(atl::Atlas{M}, x::AbstractArray1, pidx::Integer=1) where {M}
    𝒻.func(Point(atl, x, pidx))
end


"""
    FieldFuncOnData

Wrapper for a function indicating that its input signature is
```
f(atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer)
```
where `xdat` is the point data and `pidx` is the patch index.
See [`FieldFuncOnPoint`](@ref) which provides a fully compatible API for a function that
instead takes `Point` arguments.
"""
struct FieldFuncOnData{M,A<:Atlas{M},F} <: FieldFunction{M}
    atlas::A
    func::F
end

function FieldFuncOnData{M}(𝒻, atl::Atlas{M}) where {M}
    FieldFuncOnData{M,typeof(atl),typeof(𝒻)}(atl, 𝒻)
end
FieldFuncOnData(𝒻, atl::Atlas{M}) where {M} = FieldFuncOnData{M}(𝒻, atl)

# yes, the redundancy here is deliberate
function (ff::FieldFuncOnData{M,A})(atl::A, x::AbstractArray1, pidx::Integer=1) where {M,A<:Atlas{M}}
    ff.func(atl, x, pidx)
end

(ff::FieldFuncOnData{M})(x::Point{M}) where {M} = ff.func(ff.atlas, data(x, ff.atlas), patchindex(x, ff.atlas))

"""
    Field{M<:Manifold}

A data structure representing a field ``\\phi : M \\to Y`` where the codomain ``Y`` is
specified as part of the interface of the `Field` type.  The codomain is given by the
[`codomain`](@ref) function.

In terms of implementation this is a simple function wrapper, the purpose of which is
to make the return value type known by the type system.  Wrapping field functions
in a `Field` object is not manditory, but it is required for some methods such as computing
gradients.
"""
abstract type Field{M<:Manifold} end

# inner FieldFunction object should handle dispatch for these
(ϕ::Field{M})(x::Point{M}) where {M} = ϕ.func(x)
(ϕ::Field{M})(atl::Atlas{M}, x::AbstractArray, pidx::Integer=1) where {M} = ϕ.func(atl, x, pidx)

domain(::Type{<:Field{M}}) where {M} = M
domain(ϕ::Field) = domain(typeof(ϕ))

codomain(ϕ::Field) = codomain(typeof(ϕ))

# defined in tensors.jl
defaultdiffbackend(::Type{<:Field}, args...) = defaultdiffbackend()

defaultdiffbackend(ϕ::Field, args...) = defaultdiffbackend(typeof(ϕ), args...)


abstract type ScalarField{M} <: Field{M} end

# not great that this is non-specific...
codomain(::Type{<:ScalarField}) = Number


struct GScalarField{M,F<:FieldFunction{M}} <: ScalarField{M}
    func::F

    function GScalarField{M}(ff::FieldFunction{M}) where {M}
        new{M,typeof(ff)}(ff)
    end
end

GScalarField{M}(𝒻) where {M} = GScalarField{M}(FieldFuncOnPoint{M}(𝒻))
GScalarField(ff::FieldFunction{M}) where {M} = GScalarField{M}(ff)

ScalarField(ff::FieldFunction{M}) where {M} = GScalarField{M}(ff)
ScalarField{M}(𝒻) where {M} = ScalarField(FieldFuncOnPoint{M}(𝒻))

@inline function dir∂data(diffbackend::AbstractADType, ϕ::ScalarField{M}, val::Val{k},
                          atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1) where {k,M}
    dϕ = DI.pushforward(diffbackend, xdat, (DI.basis(xdat,k),)) do ξdat
        ϕ.func(atl, ξdat, pidx)
    end
    dϕ[1]
end

@inline function dir∂data(ϕ::ScalarField{M}, val::Val, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                    diffbackend::AbstractADType=defaultdiffbackend(ϕ)) where {M}
    dir∂data(diffbackend, ϕ, val, atl, xdat, pidx)
end

function ∂data(diffbackend::AbstractADType, ϕ::ScalarField{M}, atl::Atlas{M}, xdat::AbstractArray1,
               pidx::Integer=1) where {M}
    SVector(DI.gradient(ξdat -> ϕ.func(atl, ξdat, pidx), diffbackend, xdat))
end
function ∂data(ϕ::ScalarField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
               diffbackend::AbstractADType=defaultdiffbackend(ϕ)) where {M}
    ∂data(diffbackend, ϕ, atl, xdat, pidx)
end
function ∂data(ϕ::ScalarField{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(ϕ)) where{M}
    ∂data(ϕ, Atlas(x), data(x), patchindex(x); diffbackend)
end

function ∇(ϕ::GScalarField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
           diffbackend::AbstractADType=defaultdiffbackend(ϕ)) where {M}
    vdat = ∂data(ϕ, atl, xdat, pidx; diffbackend)
    x = Point(atl, xdat, pidx)
    OneForm(atl, x, vdat)
end

function ∇(ϕ::ScalarField{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(ϕ)) where {M}
    vdat = ∂data(ϕ, x; diffbackend)
    OneForm(Atlas(x), x, vdat)
end

∇(ϕ::ScalarField{M}; diffbackend::AbstractADType=defaultdiffbackend(ϕ)) where {M} = OneFormField{M}(x -> ∇(ϕ, x; diffbackend))


abstract type TensorField{M,r,s} <: Field{M} end

codomain(::Type{<:TensorField{M,r,s}}) where {M,r,s} = Tensor{M,r,s}

data(u::TensorField{M}, x::Point{M}) where {M} = data(u(x))


struct GTensorField{M,r,s,F<:FieldFunction{M}} <: TensorField{M,r,s}
    func::F
end

GTensorField{M,r,s}(ff::FieldFunction) where {M,r,s} = GTensorField{M,r,s,typeof(ff)}(ff)
TensorField{M,r,s}(ff::FieldFunction) where {M,r,s} = GTensorField{M,r,s}(ff)

GTensorField{M,r,s}(𝒻) where {M,r,s} = GTensorField{M,r,s}(FieldFuncOnPoint{M}(𝒻))
TensorField{M,r,s}(𝒻) where {M,r,s} = TensorField{M,r,s}(FieldFuncOnPoint{M}(𝒻))

const VectorField{M} = TensorField{M,1,0}
const OneFormField{M} = TensorField{M,0,1}

const GVectorField{M,F} = GTensorField{M,1,0,F}
const GOneFormField{M,F} = GTensorField{M,0,1,F}

# it can't infer these constructors
GVectorField{M}(ff::FieldFunction{M}) where {M} = GVectorField{M,typeof(ff)}(ff)
GOneFormField{M}(ff::FieldFunction{M}) where {M} = GOneFormField{M,typeof(ff)}(ff)

GVectorField(ff::FieldFunction{M}) where {M} = GVectorField{M}(ff)
GOneFormField(ff::FieldFunction{M}) where {M} = GOneFormField{M}(ff)

VectorField(ff::FieldFunction) = GVectorField(ff)
OneFormField(ff::FieldFunction) = GOneFormField(ff)

GVectorField{M}(𝒻) where {M} = GVectorField(FieldFuncOnPoint{M}(𝒻))
GOneFormField{M}(𝒻) where {M} = GOneFormField(FieldFuncOnPoint{M}(𝒻))

VectorField{M}(𝒻) where {M} = VectorField(FieldFuncOnPoint{M}(𝒻))
OneFormField{M}(𝒻) where {M} = OneFormField(FieldFuncOnPoint{M}(𝒻))

"""
    dir∂data(t::TensorField, varr::AbstractArray1, atlas, xdata, patchindex; diffbackend)

Compute the component data for the directional (partial) derivative in direction `varr`.  This is a low-level
function where the provided direction is an `AbstractArray` and not a true vector.

The return value is an array in the *same shape* as the data of the field, having the same indices.
"""
@inline function dir∂data(diffbackend::AbstractADType, t::TensorField{M}, varr::AbstractArray1,
                          atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1) where {M}
    dt = DI.pushforward(diffbackend, xdat, (varr,)) do ξdat
        data(t.func(atl, ξdat, pidx), Representation(atl))
    end
    dt[1]
end

@inline function dirdata∂data(diffbackend::AbstractADType, t::TensorField{M}, varr::AbstractArray1,
                              atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1) where {M}
    (val, dt) = DI.value_and_pushforward(diffbackend, xdat, (varr,)) do ξdat
        data(t.func(atl, ξdat, pidx), Representation(atl))
    end
    (val, dt[1])
end

@inline function dir∂data(t::TensorField{M}, varr::AbstractArray1, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                          diffbackend::AbstractADType=defaultdiffbackend(t)) where {M}
    dir∂data(diffbackend, t, varr, atl, xdat, pidx)
end

@inline function dirdata∂data(t::TensorField{M}, varr::AbstractArray1, atl::Atlas{M}, xdat::AbstractArray1,
                              pidx::Integer=1; diffbackend::AbstractADType=defaultdiffbackend(t)) where {M}
    dirdata∂data(diffbackend, t, varr, atl, xdat, pidx)
end

"""
    dir∂data(t::TensorField, ::Val{k}, atlas, xdata, patchindex; diffbackend)

This version of the function computes the directional (partial) derivative in the `k`th coordinate direction.
"""
@inline function dir∂data(diffbackend::AbstractADType, t::TensorField{M}, ::Val{k},
                          atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1) where {k,M}
    dir∂data(diffbackend, t, DI.basis(xdat, k), atl, xdat, pidx)
end

@inline function dir∂data(t::TensorField{M}, val::Val{k}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                          diffbackend::AbstractADType=defaultdiffbackend(t)) where {k,M}
    dir∂data(diffbackend, t, val, atl, xdat, pidx)
end

@inline function dirdata∂data(diffbackend::AbstractADType, t::TensorField{M}, ::Val{k},
                              atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1) where {k,M}
    dirdata∂data(diffbackend, t, DI.basis(xdat, k), atl, xdat, pidx)
end

@inline function dirdata∂data(t::TensorField{M}, val::Val{k}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                              diffbackend::AbstractADType=defaultdiffbackend(t)) where {k,M}
    dirdata∂data(diffbackend, t, val, atl, xdat, pidx)
end


#TODO: ∇ for general tensors

"""
    data∂data(v::VectorField, atl::Atlas, xdat::AbstractArray1, pidx::Integer=1)
    data∂data(v::VectorField, x::Point)

Evaluates the components of the coordinate partial derivative gradient of the vector field `v` at
the point described by `(atl, xdat, pidx)`.  Returns a tuple `(v, ∂v)` where `v` is an array
with the components of `v` at the point `x` and `∂v` is the *TRANSPOSE* (matrix) gradient.
The transpose occurs because of standard conventions for computing gradients via autodiff in Julia.
"""
@inline function data∂data(diffbackend::AbstractADType, v::GVectorField{M}, atl::Atlas{M}, xdat::AbstractArray1,
                           pidx::Integer=1) where {M}
    rep = Representation(atl, Holonomic())
    DI.value_and_jacobian(ξdat -> data(v.func(atl, ξdat, pidx), rep), diffbackend, xdat)
end
@inline function data∂data(v::GVectorField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                           diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    data∂data(diffbackend, v, atl, xdat, pidx)
end

#fallback
@inline function data∂data(diffbackend::AbstractADType, v::VectorField{M}, x::Point{M}) where {M}
    data∂data(diffbackend, v, Atlas(x), data(x), patchindex(x))
end

@inline function data∂data(u::TensorField{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    data∂data(u, Atlas(x), data(x), patchindex(x); diffbackend)
end

@inline function _christoffel_contract_vec(::Val{n}, Γ, vdat) where {n}
    grad2 = similar(Γ, Size(n, n))
    @tullio grad2[α,β] = Γ[β,α,μ] * vdat[μ] threads=false
    SArray(grad2)
end

#WARN: I'm tempted to change convention to covariant-contravariant entirely for the sake of being
#able to represent vector gradients, but need to think it through more before committing to such a
#disruptive change
#
# biggest problem I see with this is then we lose vector projectors

# fallback but should work for all VectorField that define data∂data
"""
    ∇(v::VectorField, x::Point)

Computes the gradient of the vector field `v` at point `x`.  Note that this returns the double up-index
gradient ``\\nabla^\\mu v^\\nu`` in order to satisfy the limitations of the Ricci.jl tensor type system.
"""
function ∇(v::VectorField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
           diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    (vdat, grad1) = data∂data(v, atl, xdat, pidx; diffbackend)
    Γ = christoffel2data(atl, xdat, pidx; diffbackend)
    grad2 = _christoffel_contract_vec(Val(ndimensions(M)), Γ, vdat)
    ginv = invmetricdata(atl, xdat, pidx)
    x = Point(atl, xdat, pidx)
    GTensor{M,Representation{M,typeof(atl),Holonomic},2,0}(x, ginv*grad1' + grad2*ginv)
end
function ∇(v::VectorField{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    (vdat, grad1) = data∂data(v, x; diffbackend)
    Γ = christoffel2data(x; diffbackend)
    grad2 = _christoffel_contract_vec(Val(ndimensions(M)), Γ, vdat)
    ginv = invmetricdata(x)
    atl = Atlas(x)
    # recall  that data∂data is giving *transpose* due to its conventions
    GTensor{M,Representation{M,typeof(atl),Holonomic},2,0}(x, ginv*grad1' + grad2*ginv)
end

function ∇(v::GVectorField{M}; diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    GTensorField{M,2,0}(x -> ∇(v, x; diffbackend))
end

# this is specialized to eliminate the index raising in ∇
function divergence(v::VectorField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                    diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    (vdat, grad1) = data∂data(v, atl, xdat, pidx; diffbackend)
    Γ = christoffel2data(atl, xdat, pidx; diffbackend)
    grad2 = _christoffel_contract_vec(Val(ndimensions(M)), Γ, vdat)
    tr(grad1) + tr(grad2)
end

function divergence(v::VectorField{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    atl = Atlas(x)
    divergence(v, atl, data(x, atl), patchindex(x, atl); diffbackend)
end

function divergence(v::GVectorField{M}; diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    GScalarField{M}(x -> divergence(v, x; diffbackend))
end


@inline function data∂data(diffbackend::AbstractADType, u::GOneFormField{M}, atl::Atlas{M}, xdat::AbstractArray1,
                           pidx::Integer=1) where {M}
    rep = Representation(atl, Holonomic())
    DI.value_and_jacobian(ξdat -> data(u.func(atl, ξdat, pidx), rep), diffbackend, xdat)
end
@inline function data∂data(u::GOneFormField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                           diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    data∂data(diffbackend, u, atl, xdat, pidx)
end

@inline function _christoffel_contract_oneform(::Val{n}, Γ, udat) where {n}
    grad2 = similar(Γ, Size(n, n))
    @tullio grad2[α,β] = Γ[μ,α,β] * udat[μ] threads=false
    SArray(grad2)
end

function ∂data(u::TensorField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
               diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    data∂data(u, atl, xdat, pidx; diffbackend)[2]
end
function ∂data(u::TensorField{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    data∂data(u, x; diffbackend)[2]
end


# fallback but should work for all OneFormField that define data∂data
"""
    ∇(u::OneFormField, x::Point; diffbackend=defaultdiffbackend(u))

Computes the gradient of the 1-form field `u` at point `x`, ``\\nabla_a u_b``.
If the point argument `x` is omitted this will return a tensor field.
"""
function ∇(u::OneFormField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
           diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    (udat, grad1) = data∂data(u, atl, xdat, pidx; diffbackend)
    Γ = christoffel2data(atl, xdat, pidx; diffbackend)
    grad2 = _christoffel_contract_oneform(Val(ndimensions(M)), Γ, udat)
    x = Point(atl, xdat, pidx)
    GTensor{M,Representation{M,typeof(atl),Holonomic},0,2}(x, grad1' - grad2)
end
function ∇(u::OneFormField{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    (udat, grad1) = data∂data(u, x; diffbackend)
    Γ = christoffel2data(x; diffbackend)
    grad2 = _christoffel_contract_oneform(Val(ndimensions(M)), Γ, udat)
    atl = Atlas(x)
    GTensor{M,Representation{M,typeof(atl),Holonomic},0,2}(x, grad1' - grad2)
end

function ∇(u::GOneFormField{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    GTensorField{M,0,2}(x -> ∇(u, x; diffbackend))
end

"""
    divergence(u::OneFormField, x::Point; diffbackend=defaultdiffbackend(u))

Computes the divergence of the 1-form field `u` at point `x`, ``g^{a b}\\nabla_a u_b``.
If the point argument `x` is omitted this will return a scalar field.
"""
function divergence(u::OneFormField{M}, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                    diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    tr(∇(u, atl, xdat, pidx; diffbackend))
end

function divergence(u::OneFormField{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    atl = Atlas(x)
    divergence(u, atl, data(x, atl), patchindex(x, atl); diffbackend)
end

function divergence(u::GOneFormField{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    GScalarField{M}(x -> divergence(u, x; diffbackend))
end

#TODO: will almost certainly need higher order divergence, will have to decide whether to start
#doing the generated functions...
# might cheat and just do it manually for now


# BasisType is important for dispatch on Lie derivatives
struct BasisVectorField{k,M,BT<:BasisType,A<:Atlas{M}} <: TensorField{M,1,0} end

Atlas(::Type{<:BasisVectorField{k,M,BT,A}}) where {k,M,BT,A} = A()
Atlas(e::BasisVectorField) = Atlas(typeof(e))

Representation(::Type{<:BasisVectorField{k,M,BT,A}}) where {k,M,BT,A} = Representation{M}(A(), BT())
Representation(e::BasisVectorField) = Representation(typeof(e))

function BasisVectorField{k}(rep::Representation{M,A,BT}) where {k,M,A,BT}
    BasisVectorField{k,M,BT,A}()
end
BasisVectorField{k}(atl::Atlas) where {k} = BasisVectorField{k}(Representation(atl, Holonomic()))

(e::BasisVectorField{k,M})(x::Point{M}) where {k,M} = BasisVector{k}(x, Representation(e))

# takes AD type to satisfy interface
@inline function _basis_∂data(::AbstractADType, ::Val{n}, ::Val{k}, ::Type{T}) where {n,k,T}
    ∂dat = zero(SMatrix{n,n,T})
    setindex(∂dat, one(T), CartesianIndex(k,k))
end

function ∂data(diffbackend::AbstractADType, e::BasisVectorField{k,M}, x::Point{M}) where {k,M}
    _basis_∂data(diffbackend, Val(ndimensions(M)), Val(k), dataeltype(x))
end
function ∂data(e::BasisVectorField{k,M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(e)) where {k,M}
    ∂data(diffbackend, e, x)
end

function data∂data(diffbackend::AbstractADType, e::BasisVectorField{k,M}, x::Point{M}) where {k,M}
    (data(e), ∂data(diffbackend, e, x))
end
function data∂data(e::BasisVectorField{k,M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(e)) where {k,M}
    data∂data(diffbackend, e, x)
end

dual(e::BasisVectorField{k,M,BT,A}) where {k,M,BT,A} = BasisOneFormField{k,M,BT,A}()


struct BasisOneFormField{k,M,BT<:BasisType,A<:Atlas{M}} <: TensorField{M,0,1} end

Atlas(::Type{<:BasisOneFormField{k,M,BT,A}}) where {k,M,BT,A} = A()
Atlas(e::BasisOneFormField) = Atlas(typeof(e))

Representation(::Type{<:BasisOneFormField{k,M,BT,A}}) where {k,M,BT,A} = Representation{M}(A(), BT())
Representation(e::BasisOneFormField) = Representation(typeof(e))

function BasisOneFormField{k}(rep::Representation{M,A,BT}) where {k,M,A,BT}
    BasisOneFormField{k,M,BT,A}()
end
BasisOneFormField{k}(atl::Atlas) where {k} = BasisOneFormField{k}(Representation(atl, Holonomic()))

(e::BasisOneFormField{k,M})(x::Point{M}) where {k,M} = BasisOneForm{k}(x, Representation(e))

function ∂data(diffbackend::AbstractADType, e::BasisOneFormField{k,M}, x::Point{M}) where {k,M}
    _basis_∂data(diffbackend, Val(ndimensions(M)), Val(k), dataeltype(x))
end
function ∂data(e::BasisOneFormField{k,M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(e)) where {k,M}
    ∂data(diffbackend, e, x)
end

function data∂data(diffbackend::AbstractADType, e::BasisOneFormField{k,M}, x::Point{M}) where {k,M}
    (data(e), ∂data(diffbackend, e, x))
end
function data∂data(e::BasisOneFormField{k,M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(e)) where {k,M}
    data∂data(diffbackend, e, x)
end

dual(e::BasisOneFormField{k,M,BT,A}) where {k,M,BT,A} = BasisVectorField{k,M,BT,A}()


#WARN: *NONE* of these results have been thoroughly checked...
# very easy for something to go wrong because of shapes here


@inline function liedata(u::VectorField{M}, ϕ::ScalarField{M}, x::Point{M}; kw...) where {M}
    vdat = ∂data(ϕ, x; kw...)
    data(u(x)) ⋅ vdat
end

# directional isn't actually that useful here because need to evaluate first
@inline function liedata(u::VectorField{M}, v::VectorField{M}, x::Point{M}; kw...) where {M}
    (udat, ∂udat) = data∂data(u, x; kw...)
    (vdat, ∂vdat) = data∂data(v, x; kw...)
    ∂vdat'*udat - ∂udat'*vdat
end

@inline function liedata(u::VectorField{M}, v::OneFormField{M}, x::Point{M}; kw...) where {M}
    (udat, ∂udat) = data∂data(u, x; kw...)
    (vdat, ∂vdat) = data∂data(v, x; kw...)
    ∂vdat'*udat + ∂udat*vdat
end

@inline function liedata(e::BasisVectorField{k,M,<:Holonomic}, ϕ::ScalarField{M}, x::Point{M};
                         diffbackend::AbstractADType=defaultdiffbackend(e, ϕ)) where {k,M}
    atl = Atlas(e)
    dir∂data(ϕ, Val(k), atl, data(x, atl), patchindex(x, atl); diffbackend)
end

for FT ∈ (:VectorField, :OneFormField)
    @eval @inline function liedata(e::BasisVectorField{k,M,<:Holonomic}, v::$FT{M}, x::Point{M};
                             diffbackend::AbstractADType=defaultdiffbackend(e, v)) where {k,M}
        atl = Atlas(e)
        dir∂data(v, Val(k), atl, data(x, atl), patchindex(x, atl); diffbackend)
    end
end


function lie(u::VectorField{M}, ϕ::ScalarField{M}, x::Point{M};
             diffbackend::AbstractADType=defaultdiffbackend(u, ϕ)) where {M}
    liedata(u, ϕ, x; diffbackend)
end

function lie(u::VectorField{M}, ϕ::GScalarField{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    GScalarField{M}(ξ -> liedata(u, ϕ, ξ; diffbackend))
end

# this is a fallback returning GVector, specific fields can have more specific return types
"""
    lie(u, v[, x])

Compute the Lie derivative ``\\mathcal{L}_{\\mathbf{u}} v^a =: [u^a, v^a]``.  If `Point`
`x` is passed, this will return the Lie derivative evaluated at that point, otherwise
it will return a tensor field.
"""
function lie(u::VectorField{M}, t::TensorField{M}, x::Point{M};
             diffbackend::AbstractADType=defaultdiffbackend(u, t)) where {M}
    ldat = liedata(u, t, x; diffbackend)
    rep = Representation(Atlas(x), Holonomic())
    codomain(t)(rep, x, ldat)
end

function lie(u::VectorField{M}, t::TensorField{M,r,s};
             diffbackend::AbstractADType=defaultdiffbackend(u, v)) where {M,r,s}
    TensorField{M,r,s}(ξ -> lie(u, v, ξ; diffbackend))
end

#====================================================================================================
would of course be nice to have just written a completely general Lie derivative,
but for the time being I don't want to go through the pain of the generated functions and shit
====================================================================================================#

#TODO: Lie for rank 2 needed for navier-stokes, not necessarily euler

"""
    ℒ(u, v[, x])

Alias for [`lie`](@ref).
"""
const ℒ = lie


#TODO: exterior derivatives
