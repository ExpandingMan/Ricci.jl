module Ricci

using LinearAlgebra, Statistics, Random
using StaticArrays, Bumper
using Tullio
using FastSymplecticIntegrators
using ADTypes
import DifferentiationInterface as DI
using StaticDiffInterface

using DispatchDoctor: @stable

using FastSymplecticIntegrators: Tao

import LorentzGroup

using Infiltrator

using Base: RefValue

using Base.Threads

const Float = Float32

const AbstractArray1{T} = Base.AbstractVector{T}
const Array1{T} = Base.Vector{T}

const (∞) = typemax(Float)

reals(args::Number...) = promote(map(float, args)...)

macro reals(args...)
    lhs = Expr(:tuple, args...)
    rhs = Expr(:call, :reals, args...)
    quote
        $lhs = $rhs
    end |> esc
end

"""
    Checked{T}

Type signifying that the wrapped value has been checked for validity.
`Checked` objects can be unwrapped with `getindex`, i.e. `cv[]`.

Note that `Checked` is interpreted as confirmation that *all* values are
valid to an arbitrary nesting level.  For example, passing a `Checked`
`AbstractVector` for polar coordinate data indicates not only that
the `AbstractVector` is of the appropriate length, but that all
the polar coordinate data it contains is valid.
"""
struct Checked{T}
    value::T
end

Base.getindex(cv::Checked) = cv.value


abstract type Manifold{n} end

abstract type Riemannian{n} <: Manifold{n} end

abstract type Lorentzian{n} <: Manifold{n} end


struct Euclidean{n} <: Riemannian{n} end
struct Sphere{n} <: Riemannian{n} end

struct deSitter{α} <: Lorentzian{4} end

ndimensions(::Type{<:Manifold{n}}) where {n} = n
ndimensions(M::Manifold) = ndimensions(typeof(M))


abstract type MetricSignature{n} end
struct RiemannianSignature{n,T<:Number} <: MetricSignature{n} end
struct LorentzianSignature{n,T<:Number} <: MetricSignature{n} end

# inner product for orthonormal cases. Tensor interface extends this in a way that makes sense
(::RiemannianSignature)(u::AbstractArray1, v::AbstractArray1) = u⋅v

function (::LorentzianSignature{n})(u::AbstractArray1, v::AbstractArray1) where {n}
    -u[end]*v[end] + sum(ntuple(j -> u[j]*v[j], Val(n-1)))
end

MetricSignature(::Type{T}, M::Manifold) where {T} = MetricSignature(T, typeof(M))

MetricSignature(::Type{T}, ::Type{<:Riemannian{n}}) where {n,T} = RiemannianSignature{n,T}()
MetricSignature(::Type{T}, ::Type{<:Lorentzian{n}}) where {n,T} = LorentzianSignature{n,T}()

function data(::RiemannianSignature{n,T}) where {n,T}
    SMatrix{n,n,T}(ntuple(j -> T(mod1(j, n+1) == 1), Val(n^2)))
end

function data(::LorentzianSignature{n,T}) where {n,T}
    t = ntuple(Val(n*n)) do j
        j == n^2 ? -one(T) : T(mod1(j, n+1) == 1)
    end
    SMatrix{n,n,T}(t)
end

"""
    Character

Abstract type for the singleton types `Timelike`, `Spacelike` and `Null`, the sole values of
which are `timelike`, `spacelike` and `null` respectively.  These are used as arguments to
functions where appropriate, particularly in the context of the ADM 3+1 formalism.
"""
abstract type Character end
struct Spacelike <: Character end
const spacelike = Spacelike()

struct Timelike <: Character end
const timelike = Timelike()

struct Null <: Character end
const null = Null()

squaredsign(::Type{Timelike}, ::Type{T}=Float) where {T} = -one(T)
squaredsign(::Type{Spacelike}, ::Type{T}=Float) where {T} = one(T)
squaredsign(::Type{Null}, ::Type{T}=Float) where {T} = zero(T)

squaredsign(c::Character, ::Type{T}=Float) where {T} = squaredsign(typeof(c), T)


abstract type ManifoldSubset{M<:Manifold} end


abstract type BasisType end  # of the representation in the data
struct Orthonormal <: BasisType end
struct Holonomic <: BasisType end


"""
    Atlas{M<:Manifold}

Coordinate atlas on manifold ``M``.  We'd like these to cover the entire manifold, but we are very
laissez-faire in enforcement of this because it can drastically simplify some use cases to simply
allow an incomplete atlas and have special handling of any holes.

`Atlas` objects can be thought of more specifically as software representations of `Point` objects,
in the sense that one can define multiple `Atlas` types corresponding to the same abstract
mathematical coordinate atlas.  These would essentially govern the dispatch on `Point` and
`Tensor` types.
"""
abstract type Atlas{M<:Manifold} end

Atlas(::A, ::A) where {A<:Atlas} = A()

ndimensions(::Type{<:Atlas{M}}) where {M} = ndimensions(M)
ndimensions(atl::Atlas) = ndimensions(typeof(atl))

# by default, atlas has only one patch
npatches(::Type{<:Atlas}) = Val(1)
npatches(atl::Atlas) = npatches(typeof(atl))

Manifold(::Type{<:Atlas{M}}) where {M} = M
Manifold(atl::Atlas) = Manifold(typeof(atl))


#TODO: docs
struct Representation{M<:Manifold,A<:Atlas{M},B<:BasisType}
    atlas::A
    basistype::B

    Representation{M,A,B}(a::A, b::B) where {M,A,B} = new{M,A,B}(a, b)
end

function Representation{M}(atl::Atlas{M}, bt::BasisType) where {M<:Manifold}
    Representation{M,typeof(atl),typeof(bt)}(atl, bt)
end
Representation(atl::Atlas{M}, bt::BasisType) where {M<:Manifold} = Representation{M}(atl, bt)
Representation{M,A,B}() where {M,A,B} = Representation{M}(A(), B())

# we use holonomic by default so this is just convenient
Representation(atl::Atlas) = Representation(atl, Holonomic())

Representation(r1::R, r2::R) where {R<:Representation} = R(r1.atlas, r1.basistype)

ndimensions(::Type{<:Representation{M}}) where {M} = ndimensions(M)
ndimensions(rep::Representation) = ndimensions(typeof(rep))

Manifold(::Type{<:Representation{M}}) where {M} = M()
Manifold(rep::Representation) = Manifold(typeof(rep))

Atlas(::Type{<:Representation{M,A}}) where {M,A} = A()
Atlas(rep::Representation) = rep.atlas
BasisType(::Type{<:Representation{M,A,B}}) where {M,A,B} = B()
BasisType(rep::Representation) = rep.basistype


"""
    SymplecticIntegrator

Abstract type for symplectic integrators used for solving geodesic equations.
"""
abstract type SymplecticIntegrator end

"""
    datatype(x)

Returns the type used to store data of a tensor or a point.  The value is to be considered an
implementation detail, and the behavior of objects should not depend on the returned type.
"""
datatype(x) = datatype(typeof(x))

"""
    dataeltype(x)

Returns the element type of the type used to store data of a tensor or point.  The returned value
is to be considered an implementation detail, the behavior of objects should not depend on this type.
"""
dataeltype(x) = eltype(datatype(x))

_imports() = [
    :Manifold, :Riemannian, :Lorentzian, :Euclidean,
    :ndimensions,
    :MetricSignature, :RiemannianSignature, :LorentzianSignature,
    :Orthonormal, :Holonomic,
    :data, :patchindex, :datatype, :dataeltype, :component, :constructortype, :defaulttype,
    :Character, :Spacelike, :Timelike, :Null, :spacelike, :timelike, :null,
    :Atlas, :Point,
    :Coord1Point, :CoordPoint,
    :square, :squaredistance, :distance,
    :Representation,
    :Tensor, :Vector, :OneForm, :Metric, :InverseMetric, :InvMetric,
    :LeviCivita, :LeviCivitaUp,
    :riemann, :riemanndown, :ricci, :einstein,
    :vielbeindata, :covielbeindata,
    :GTensor, :GVector, :GOneForm, :GMetric,
    :BasisVector, :BasisOneForm,
    :exponential, :paralleltransport, :pointsto, :nullpointsto, :nullpointsfrom,
    :dual, :mult, :∇, :divergence,
    :Diffeomorphism, :TransformedPoint, :TransformedTensor,
    :IdentityDiffeomorphism, :InverseDiffeomorphism, :CompositeDiffeomorphism, :InvDiff, :CompDiff,
    :comoving, :invcomoving, :comovingwithinv,
    :coordtransform_ondata, :CoordTransform,
    :Worldline, :InertialWorldline,
    :pointofdistance, :tangentofdistance, :frameofdistance, :invframeofdistance, :framewithinvofdistance,
    :velocity, :timeofdistance,
    :𝐠, :𝐠inv,
    :FieldFunction, :FieldFuncOnPoint, :FieldFuncOnData, :defaultdiffbackend,
    :Field, :ScalarField, :TensorField, :VectorField, :OneFormField,
    :BasisVectorField, :BasisOneFormField,
    :lie, :ℒ,
    :ADMBasis, :ADMCoordBasis, :SpacelikeComponents, :TimelikeComponents, :lapse, :lapse2, :conormal,
    :normal, :timevector, :spatialmetric, :invspatialmetric, :spatialprojector, :temporalprojector,
    :TempAccelCovector, :TempAccelVector, :ExtCurvature, :ExtCurvatureUp,
    :extcurvature, :extcurvatureup, :meancurvature,
    :D,
]

macro _submodule_imports()
    imps = map(s -> :(import ..Ricci: $s), _imports())
    esc(quote
        using LinearAlgebra, StaticArrays
        using DispatchDoctor: @stable
        import ..Ricci
        using ..Ricci: Float, AbstractArray1
        using ..Ricci: negatelast, staticrow, minkowski_inner
        $(imps...)
        import ..Ricci: with
        import ..Ricci: _apply, _applyinv
        import ..Ricci: hamiltonian, ∂qhamiltonian_ondata, ∂phamiltonian_ondata, ∂qhamiltonian, ∂phamiltonian
        import ..Ricci: defaultintegrator
        import ..Ricci: metricdata, invmetricdata
    end)
end

include("arrays.jl")
include("points.jl")
include("tensors.jl")
include("integrators.jl")
include("diffeomorphisms.jl")
include("worldlines.jl")
include("fields.jl")
include("adm.jl")
include("macros.jl")

include("MinkowskiImpl/MinkowskiImpl.jl")
include("SchwarzschildImpl/SchwarzschildImpl.jl")
include("deSitterImpl/deSitterImpl.jl")

#include("Meshes/Meshes.jl")

for sym ∈ _imports()
    @eval export $sym
end

end
