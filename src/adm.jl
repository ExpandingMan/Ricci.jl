#====================================================================================================
WARN:

The original idea here was to have special tensor types for those that only need to store temporal
or spatial data, and to have the various projection tensors create tensors of those types.
Basically that would mean at API level everything is fully covariant, but at implementation
level it's doing component indices.

I'm deliberately dragging my feet on this because I don't really know if it'll be worth it.
It's not clear how many operations it'll eliminate in realistic use cases.

For the time being, everything is returning ordinary tensors.  This is encoded in the
`constructortype` stuff as well as the mult methods.

This stuff also raises uncomfortable questions about whether we should have a more sophisticated
way of representing the tensor basis than the Orthonormal/Holonomic thing.

Note also that pure algebra might be important: for example, you could probably have it derive
equations for you *at compile time* simply by making sure it uses the appropriate zeros.

#TODO: will probably at very least want some trait system to help with ADM algebra
====================================================================================================#

abstract type ADMBasis{M<:Manifold} end


struct ADMCoordBasis{M,A<:Atlas{M}} <: ADMBasis{M}
    atlas::A
end

Atlas(::Type{<:ADMCoordBasis{M,A}}) where {M,A} = A()
Atlas(basis::ADMCoordBasis) = basis.atlas


struct SpacelikeComponents{B<:ADMBasis}
    basis::B
end

struct TimelikeComponents{B<:ADMBasis}
    basis::B
end

Atlas(::Type{<:SpacelikeComponents{B}}) where {B} = Atlas(B)
Atlas(sco::SpacelikeComponents) = Atlas(sco.basis)

Atlas(::Type{<:TimelikeComponents{B}}) where {B} = Atlas(B)
Atlas(tco::TimelikeComponents) = Atlas(tco.basis)

data(::Spacelike, u::Tensor) = data(SpacelikeComponents(ADMCoordBasis(Atlas(u))), u)
data(::Timelike, u::Tensor) = data(TimelikeComponents(ADMCoordBasis(Atlas(u))), u)

component(::Timelike, u::Tensor) = component(TimelikeComponents(ADMCoordBasis(Atlas(u))), u)


# most of this stuff is intended as fallbacks because it may not be super efficient
function lapse2(basis::ADMCoordBasis, x::AbstractArray1, pidx::Integer=1)
    g = invmetricdata(Atlas(basis), x, pidx)
    -(1/g[end])
end
function lapse2(basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    g = invmetricdata(x, Atlas(basis))
    -(1/g[end])  # this is true even for non-diagonal metrics
end
lapse2(x::Point) = lapse2(ADMCoordBasis(Atlas(x)), x)

lapse(basis::ADMCoordBasis, x::AbstractArray1, pidx::Integer=1) = √(lapse2(basis, x, pidx))

lapse(basis::ADMCoordBasis{M}, x::Point{M}) where {M} = √(lapse2(basis, x))
lapse(x::Point) = √(lapse2(x))

function lapse(basis::ADMCoordBasis{M}) where {M}
    ScalarField(FieldFuncOnData((_, xdat, pidx) -> lapse(basis, xdat, pidx), Atlas(basis)))
end
lapse(atl::Atlas) = lapse(ADMCoordBasis(atl))

function lapse2(basis::ADMCoordBasis{M}) where {M}
    ScalarField(FieldFuncOnData((_, xdat, pidx) -> lapse2(basis, xdat, pidx), Atlas(basis)))
end
lapse2(atl::Atlas) = lapse2(ADMCoordBasis(atl))


# this is useful in multiple places
function temporalconormaldata(basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    ω0 = zero(SVector{ndimensions(M),dataeltype(x)})
    setindex(ω0, -lapse(basis, x), lastindex(ω0))
end
function temporalconormaldata(::SpacelikeComponents{<:ADMCoordBasis{M}}, x::Point{M}) where {M}
    zero(SVector{ndimensions(M)-1,dataeltype(x)})
end
function temporalconormaldata(::TimelikeComponents{<:ADMCoordBasis{M}}, x::Point{M}) where {M}
    α = lapse(basis, x)
    @SVector dataeltype(x)[-α]
end

function temporalnormaldata(basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    n = temporalconormaldata(basis, x)
    g = invmetricdata(x, Atlas(x))
    g*n
end


#====================================================================================================
This components stuff *looks* confusing but is pretty simple.

You are free to specify the basis when accessing data which keeps with our general philosophy of
abstract tensors.

The reason we still keep the basis is because it defines our default rep (i.e. `data`).
====================================================================================================#


struct TemporalConormal{M,B<:ADMBasis{M},P<:Point{M}} <: OneForm{M}
    basis::B
    point::P
end

TemporalConormal(x::Point) = TemporalConormal(ADMCoordBasis(Atlas(x)), x)

Atlas(::Type{<:TemporalConormal{M,B}}) where {M,B} = Atlas(B)

pointtype(::Type{<:TemporalConormal{M,B,P}}) where {M,B,P} = P

data(n::TemporalConormal) = temporalconormaldata(n.basis, Point(n))

data(sco::SpacelikeComponents, n::TemporalConormal) = temporalconormaldata(sco, Point(n))

data(tco::TimelikeComponents, n::TemporalConormal) = temporalconormaldata(tco, Point(n))

function component(::TimelikeComponents{<:ADMCoordBasis{M}}, n::TemporalConormal{M}) where {M}
    -lapse(n.basis, Point(n))
end

conormal(::Timelike, basis::ADMBasis{M}, x::Point{M}) where {M} = TemporalConormal(basis, x)
conormal(::Timelike, x::Point) = conormal(timelike, ADMCoordBasis(Atlas(x)), x)

function conormal(::Timelike, basis::ADMBasis{M}) where {M}
    OneFormField{M}(ξ -> conormal(timelike, basis, ξ))
end


struct TemporalNormal{M,B<:ADMBasis{M},P<:Point{M}} <: Vector{M}
    basis::B
    point::P
end

TemporalNormal(x::Point) = TemporalNormal(ADMCoordBasis(Atlas(x)), x)

Atlas(::Type{<:TemporalNormal{M,B}}) where {M,B} = Atlas(B)

pointtype(::Type{<:TemporalNormal{M,B,P}}) where {M,B,P} = P

@stable function data(n::TemporalNormal{M,<:ADMCoordBasis}) where {M}
    ginv = invmetricdata(Point(n), Atlas(n))
    ndat = temporalconormaldata(n.basis, Point(n))
    ginv * ndat
end

normal(::Timelike, basis::ADMBasis{M}, x::Point{M}) where {M} = TemporalNormal(basis, x)
normal(::Timelike, x::Point{M}) where {M} = normal(timelike, ADMCoordBasis(Atlas(x)), x)

function normal(::Timelike, basis::ADMBasis{M}) where {M}
    VectorField(FieldFuncOnPoint(ξ -> normal(timelike, basis, ξ), Atlas(basis)))
end

Vector(n::TemporalConormal) = TemporalNormal(n.basis, Point(n))
OneForm(n::TemporalNormal) = TemporalConormal(n.basis, Point(n))

function mult(::Representation{M}, ndown::TemporalConormal{M}, nup::TemporalNormal{M}) where {M}
    -one(dataeltype(ndown))
end
function mult(::Representation{M}, nup::TemporalNormal{M}, ndown::TemporalConormal{M}) where {M}
    -one(dataeltype(nup))
end

function metricdata(sco::SpacelikeComponents{<:ADMCoordBasis{M}}, x::Point{M}) where {M}
    g = metricdata(x, Atlas(sco))
    SMatrix(g[SOneTo(ndimensions(M)-1),SOneTo(ndimensions(M)-1)])
end
metricdata(::Spacelike, x::Point) = metricdata(SpacelikeComponents(ADMCoordBasis(Atlas(x))), x)

function spatialmetricdata(sco::SpacelikeComponents{<:ADMCoordBasis{M}}, x::Point{M}) where {M}
    metricdata(sco, x)
end
spatialmetricdata(::Spacelike, x::Point) = spatialmetricdata(SpacelikeComponents(ADMCoordBasis(Atlas(x))), x)


# this is a somewhat dubious approach, but simplest I could think of for general case
function spatialmetricdata(basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    γ1 = metricdata(SpacelikeComponents(basis), x)
    γ2 = hcat(γ1, zero(SVector{ndimensions(M)-1,eltype(γ1)}))
    vcat(γ2, zero(SMatrix{1,ndimensions(M),eltype(γ1)}))
end
spatialmetricdata(x::Point{M}) where {M} = spatialmetricdata(ADMCoordBasis(Atlas(x)), x)

@stable function shiftcovectordata(sco::SpacelikeComponents{<:ADMCoordBasis{M}}, x::Point{M}) where {M}
    g = metricdata(x, Atlas(sco))
    n = ndimensions(M)
    SVector{n-1,dataeltype(x)}(ntuple(j -> g[j,n], Val(n-1)))
end

@stable function shiftcovectordata(basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    g = metricdata(x, Atlas(basis))
    β = shiftvectordata(basis, x)
    g*β
end

@stable function shiftvectordata(sco::SpacelikeComponents{<:ADMCoordBasis{M}}, x::Point{M}) where {M}
    α2 = lapse2(sco.basis, x)
    g = invmetricdata(x, Atlas(sco))
    n = ndimensions(M)
    T = dataeltype(x)
    SVector{n-1,T}(ntuple(j -> α2*g[j,n], Val(n-1)))
end

@stable function shiftvectordata(basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    βi = shiftvectordata(SpacelikeComponents(basis), x)
    push(βi, zero(dataeltype(x)))
end

@stable function spatialprojectordata(basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    δ = SDiagonal(@SVector(ones(ndimensions(M))))
    n = temporalnormaldata(basis, x)
    ndown = temporalconormaldata(basis, x)
    δ + n .* ndown'
end


@stable function invspatialmetricdata(basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    g = invmetricdata(x, Atlas(basis))
    n = temporalnormaldata(basis, x)
    g + n .* n'
end

@stable function invspatialmetricdata(sco::SpacelikeComponents{<:ADMCoordBasis{M}}, x::Point{M}) where {M}
    γ = invspatialmetricdata(sco.basis, x)
    SMatrix(γ[SOneTo(ndimensions(M)-1),SOneTo(ndimensions(M)-1)])
end

@stable function _tempacceldata(γ, diffbackend::AbstractADType, basis::ADMCoordBasis{M}, x::Point{M}) where {M}
    γ = spatialprojectordata(basis, x)
    pidx = patchindex(x, Atlas(basis))
    xdat = data(x, Atlas(basis))
    α = lapse(basis, xdat, pidx)
    (α, _∇α) = value_and_gradient(ξdat -> lapse(basis, ξdat, pidx), diffbackend, xdat)
    ∇α = SVector(_∇α)
    γ*∇α ./ α
end

@stable function tempaccelcovectordata(basis::ADMCoordBasis{M}, x::Point{M};
                                       diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    γ = spatialprojectordata(basis, x)
    _tempacceldata(γ, diffbackend, basis, x)
end

@stable function tempaccelvectordata(basis::ADMCoordBasis{M}, x::Point{M};
                                     diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    γ = invspatialmetricdata(basis, x)
    _tempacceldata(γ, diffbackend, basis, x)
end

@stable function extcurvaturedata(basis::ADMCoordBasis{M}, x::Point{M};
                                  diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    γ = spatialprojectordata(basis, x)
    ∇n = data(∇(conormal(timelike, basis), x; diffbackend))
    -γ*∇n*γ'
end

@stable function extcurvatureupdata(basis::ADMCoordBasis{M}, x::Point{M};
                                    diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    γ = invspatialmetricdata(basis, x)
    ∇n = data(∇(conormal(timelike, basis), x; diffbackend))
    -γ*∇n*γ'
end

@stable function meancurvature(basis::ADMCoordBasis{M}, x::Point{M};
                               diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    g = invmetricdata(x, Atlas(basis))
    K = extcurvaturedata(basis, x; diffbackend)
    tr(g*K)
end

function meancurvature(basis::ADMCoordBasis{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    ScalarField{M}(FieldFuncOnPoint(x -> meancurvature(basis, x; diffbackend), Atlas(basis)))
end


struct SpatialMetric{M,B<:ADMBasis{M},P<:Point{M}} <: Tensor{M,0,2}
    basis::B
    point::P
end

SpatialMetric(x::Point) = SpatialMetric(ADMCoordBasis(Atlas(x)), x)

# these exist so that type can be overridden by impls
spatialmetric(basis::ADMBasis{M}, x::Point{M}) where {M} = SpatialMetric(basis, x)
spatialmetric(x::Point) = spatialmetric(ADMCoordBasis(Atlas(x)), x)

Atlas(::Type{<:SpatialMetric{M,B}}) where {M,B} = Atlas(B)

pointtype(::Type{<:SpatialMetric{M,B,P}}) where {M,B,P} = P

data(γ::SpatialMetric) = spatialmetricdata(γ.basis, Point(γ))

data(sco::SpacelikeComponents, γ::SpatialMetric) = spatialmetricdata(sco, Point(γ))


# remember, this is not the true inverse!
struct InvSpatialMetric{M,B<:ADMBasis{M},P<:Point{M}} <: Tensor{M,2,0}
    basis::B
    point::P
end

InvSpatialMetric(x::Point) = InvSpatialMetric(ADMCoordBasis(Atlas(x)), x)

invspatialmetric(basis::ADMBasis{M}, x::Point{M}) where {M} = InvSpatialMetric(basis, x)
invspatialmetric(x::Point) = invspatialmetric(ADMCoordBasis(Atlas(x)), x)

Atlas(::Type{<:InvSpatialMetric{M,B}}) where {M,B} = Atlas(B)

pointtype(::Type{<:InvSpatialMetric{M,B,P}}) where {M,B,P} = P

data(γ::InvSpatialMetric) = invspatialmetricdata(γ.basis, Point(γ))

data(sco::SpacelikeComponents, γ::InvSpatialMetric) = invspatialmetricdata(sco, Point(γ))

function mult(::Representation{M}, γinv::InvSpatialMetric{M,B}, γ::SpatialMetric{M,B}) where {M,B}
    spatialprojector(γinv.basis, Point(γinv))
end

function mult(rep::Representation{M}, γ::SpatialMetric{M,B}, n::TemporalNormal{M,B}) where {M,B}
    ZeroOneForm{M,typeof(rep)}(Point(γ))
end

function mult(rep::Representation{M}, n::TemporalConormal{M,B}, γ::InvSpatialMetric{M,B}) where {M,B}
    ZeroVector{M,typeof(rep)}(Point(n))
end


struct TemporalProjector{M,B<:ADMBasis{M},P<:Point{M}} <: Tensor{M,1,1}
    basis::B
    point::P
end

TemporalProjector(x::Point) = TemporalProjector(ADMCoordBasis(Atlas(x)), x)

# can be overridden by basis impls
temporalprojector(basis::ADMBasis{M}, x::Point{M}) where {M} = TemporalProjector(basis, x)
temporalprojector(x::Point) = temporalprojector(ADMCoordBasis(Atlas(x)), x)

Atlas(::Type{<:TemporalProjector{M,B}}) where {M,B} = Atlas(B)

pointtype(::Type{<:TemporalProjector{M,B,P}}) where {M,B,P} = P

function data(N::TemporalProjector)
    ndown = temporalconormaldata(N.basis, Point(N))
    nup = temporalnormaldata(N.basis, Point(N))
    - nup .* ndown
end

function mult(::Representation{M}, N::TemporalProjector{M}, u::Vector{M}) where {M}
    ndown = conormal(timelike, N.basis, Point(N))
    nup = normal(timelike, N.basis, Point(N))
    - (ndown * u) * nup
end

function mult(::Representation{M}, u::OneForm{M}, N::TemporalProjector{M}) where {M}
    ndown = conormal(timelike, N.basis, Point(N))
    nup = normal(timelike, N.basis, Point(N))
    - (u * nup) * ndown
end


#think we should keep this as special object because lots of opportunities for efficiency
struct SpatialProjector{M,B<:ADMBasis{M},P<:Point{M}} <: Tensor{M,1,1}
    basis::B
    point::P
end

SpatialProjector(x::Point) = SpatialProjector(ADMCoordBasis(Atlas(x)), x)

# can be overridden by basis impls
spatialprojector(basis::ADMBasis{M}, x::Point{M}) where {M} = SpatialProjector(basis, x)
spatialprojector(x::Point) = spatialprojector(ADMCoordBasis(Atlas(x)), x)

Atlas(::Type{<:SpatialProjector{M,B}}) where {M,B} = Atlas(B)

pointtype(::Type{<:SpatialProjector{M,B,P}}) where {M,B,P} = P

data(γ::SpatialProjector) = spatialprojectordata(γ.basis, Point(γ))

function mult(::Representation{M}, γ::SpatialProjector{M}, u::Vector{M}) where {M}
    ndown = conormal(timelike, γ.basis, Point(γ))
    nup = normal(timelike, γ.basis, Point(γ))
    u + (ndown * u) * nup
end

function mult(::Representation{M}, u::OneForm{M}, γ::SpatialProjector{M}) where {M}
    ndown = TemporalConormal(γ.basis, Point(γ))
    nup = TemporalNormal(γ.basis, Point(γ))
    u + (u * nup) * ndown
end


#TODO: more projector mult methods


"""
    timevector([basis::ADMBasis], x::Point)

Returns the standard ADM basis vector ``t^a`` at point ``x``.
"""
timevector(basis::ADMCoordBasis{M}, x::Point{M}) where {M} = BasisVector{ndimensions(M)}(x)
timevector(x::Point) = timevector(ADMCoordBasis(Atlas(x)), x)


struct ShiftCovector{M,B<:ADMBasis{M},P<:Point{M}} <: OneForm{M}
    basis::B
    point::P
end

ShiftCovector(x::Point) = ShiftCovector(ADMCoordBasis(Atlas(x)), x)

Atlas(::Type{<:ShiftCovector{M,B}}) where {M,B} = Atlas(B)

dataeltype(::Type{<:ShiftCovector{M,B,P}}) where {M,B,P} = dataeltype(P)

data(β::ShiftCovector) = shiftcovectordata(β.basis, Point(β))

data(sco::SpacelikeComponents, β::ShiftCovector) = shiftcovectordata(sco, Point(β))


struct ShiftVector{M,B<:ADMBasis{M},P<:Point{M}} <: Vector{M}
    basis::B
    point::P
end

ShiftVector(x::Point) = ShiftVector(ADMCoordBasis(Atlas(x)), x)

Atlas(::Type{<:ShiftVector{M,B}}) where {M,B} = Atlas(B)

dataeltype(::Type{<:ShiftVector{M,B,P}}) where {M,B,P} = dataeltype(P)

data(β::ShiftVector) = shiftvectordata(β.basis, Point(β))

data(sco::SpacelikeComponents, β::ShiftVector) = shiftvectordata(sco, Point(β))


function data(sco::SpacelikeComponents{<:ADMCoordBasis{M}}, u::Vector{M}) where {M}
    γproj = spatialprojectordata(sco.basis, Point(u))
    (γproj * data(u))[SOneTo(ndimensions(M)-1)]
end
function data(sco::SpacelikeComponents{<:ADMCoordBasis{M}}, u::OneForm{M}) where {M}
    γproj = spatialprojectordata(sco.basis, Point(u))
    (data(u) * γproj)[1,SOneTo(ndimensions(M)-1)]
end


struct TempAccelCovector{M,B<:ADMBasis{M},P<:Point{M},DT<:AbstractADType} <: OneForm{M}
    basis::B
    point::P
    diffbackend::DT
end

function TempAccelCovector(basis::ADMBasis{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    TempAccelCovector{M,typeof(basis),typeof(x),typeof(diffbackend)}(basis, x, diffbackend)
end
function TempAccelCovector(x::Point; diffbackend::AbstractADType=defaultdiffbackend())
    TempAccelCovector(ADMCoordBasis(Atlas(x)), x; diffbackend)
end

Atlas(::Type{<:TempAccelCovector{M,B}}) where {M,B} = Atlas(B)

dataeltype(::Type{<:TempAccelCovector{M,B,P}}) where {M,B,P} = dataeltype(P)

data(a::TempAccelCovector) = tempaccelcovectordata(a.basis, Point(a); diffbackend=a.diffbackend)


struct TempAccelVector{M,B<:ADMBasis{M},P<:Point{M},DT<:AbstractADType} <: Vector{M}
    basis::B
    point::P
    diffbackend::DT
end

function TempAccelVector(basis::ADMBasis{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    TempAccelVector{M,typeof(basis),typeof(x),typeof(diffbackend)}(basis, x, diffbackend)
end
function TempAccelVector(x::Point; diffbackend::AbstractADType=defaultdiffbackend())
    TempAccelVector(ADMCoordBasis(Atlas(x)), x; diffbackend)
end

Atlas(::Type{<:TempAccelVector{M,B}}) where {M,B} = Atlas(B)

dataeltype(::Type{<:TempAccelVector{M,B,P}}) where {M,B,P} = dataeltype(P)

data(a::TempAccelVector) = tempaccelvectordata(a.basis, Point(a); diffbackend=a.diffbackend)


struct ExtCurvature{M,B<:ADMBasis{M},P<:Point{M},DT<:AbstractADType} <: Tensor{M,0,2}
    basis::B
    point::P
    diffbackend::DT
end

function ExtCurvature(basis::ADMBasis{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    ExtCurvature{M,typeof(basis),typeof(x),typeof(diffbackend)}(basis, x, diffbackend)
end
function ExtCurvature(x::Point; diffbackend::AbstractADType=defaultdiffbackend())
    ExtCurvature(ADMCoordBasis(Atlas(x)), x; diffbackend)
end

Atlas(::Type{<:ExtCurvature{M,B}}) where {M,B} = Atlas(B)

dataeltype(::Type{<:ExtCurvature{M,B,P}}) where {M,B,P} = dataeltype(P)

data(K::ExtCurvature) = extcurvaturedata(K.basis, Point(K); diffbackend=K.diffbackend)


struct ExtCurvatureUp{M,B<:ADMBasis{M},P<:Point{M},DT<:AbstractADType} <: Tensor{M,0,2}
    basis::B
    point::P
    diffbackend::DT
end

function ExtCurvatureUp(basis::ADMBasis{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    ExtCurvatureUp{M,typeof(basis),typeof(x),typeof(diffbackend)}(basis, x, diffbackend)
end
function ExtCurvatureUp(x::Point; diffbackend::AbstractADType=defaultdiffbackend())
    ExtCurvatureUp(ADMCoordBasis(Atlas(x)), x; diffbackend)
end

Atlas(::Type{<:ExtCurvatureUp{M,B}}) where {M,B} = Atlas(B)

dataeltype(::Type{<:ExtCurvatureUp{M,B,P}}) where {M,B,P} = dataeltype(P)

data(K::ExtCurvatureUp) = extcurvatureupdata(K.basis, Point(K); diffbackend=K.diffbackend)


function extcurvature(basis::ADMBasis{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    ExtCurvature(basis, x; diffbackend)
end
function extcurvature(x::Point; diffbackend::AbstractADType=defaultdiffbackend())
    extcurvature(ADMCoordBasis(Atlas(x)), x; diffbackend)
end
function extcurvature(basis::ADMBasis{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    TensorField{M,0,2}(x -> extcurvature(basis, x; diffbackend))
end

function extcurvatureup(basis::ADMBasis{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    ExtCurvatureUp(basis, x; diffbackend)
end
function extcurvatureup(x::Point; diffbackend::AbstractADType=defaultdiffbackend())
    extcurvatureup(ADMCoordBasis(Atlas(x)), x; diffbackend)
end
function extcurvatureup(basis::ADMBasis{M}; diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    TensorField{M,2,0}(x -> extcurvatureup(basis, x; diffbackend))
end


for FT ∈ (:OneFormField, :VectorField)
    @eval function D(basis::ADMBasis{M}, u::$FT{M}, x::Point{M};
                     diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
        γ = spatialprojector(basis, x)
        γ*∇(u, x; diffbackend)*γ
    end
    
    @eval function D(u::$FT{M}, x::Point{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
        D(ADMCoordBasis(Atlas(x)), u, x; diffbackend)
    end
end


function D(basis::ADMBasis{M}, u::OneFormField{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    TensorField{M,0,2}(x -> D(basis, u, x; diffbackend))
end

function D(u::OneFormField{M}; diffbackend::AbstractADType=defaultdiffbackend(u)) where {M}
    TensorField{M,0,2}(x -> D(u, x; diffbackend))
end

function D(basis::ADMBasis{M}, v::VectorField{M}; diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    TensorField{M,2,0}(x -> D(basis, u, x; diffbackend))
end

function D(v::VectorField{M}; diffbackend::AbstractADType=defaultdiffbackend(v)) where {M}
    TensorField{M,2,0}(x -> D(v, x; diffbackend))
end
