
#====================================================================================================
       NOTE:

Have been investigating using Tensorial.jl.  Biggest problem I can find is that ForwardDiff
doesn't now how to deal with it, so it creates lots of `Array`.  Hacks for fixing this are
rather ugly.
====================================================================================================#


#TODO: can we make this more generic?
"""
    staticrow(q::StaticVector)

Construct the `StaticArray` row matrix containing the same elements as `q`.  Surprisingly hard to do this
without creating a wrapper using existing library functions.
"""
function staticrow(q::StaticVector{n}) where {n}
    SMatrix{1,n}(ntuple(j -> q[j], Val(n)))
end

#TODO: more generic, would be needed for Tensorial
"""
    negatelast(u::StaticArray)

Negate the last element of the array, i.e. raise/lower indices for Minkowski.  This is useful in multiple
contexts because of the fundamental importance of Minkowski space, so it is provided here as a utility function.
"""
negatelast(u::StaticArray) = setindex(u, -u[end], length(u))

"""
    minkowski_inner(u, v)

Compute the Minkowski inner product on two arrays, regardless of their shape.  This can be used for example
on 1-form data as well as on vector data.  The user is entirely responsible for whether the way they
use this function makes any sense.
"""
minkowski_inner(u::AbstractArray, v::AbstractArray) = vec(u) ⋅ negatelast(vec(v))


# from Tensorial.jl
"""
    sgn(x...)

Computes the permutation parity of the index permutation given by `x`, i.e. the elements of
the Levi-Civita symbol.
"""
function sgn(x::Integer...)
    N = length(x)
    even = true
    @inbounds for i ∈ 1:N, j ∈ (i+1):N
        x[i] == x[j] && return 0
        even ⊻= x[i] > x[j]
    end
    even ? 1 : -1
end


#TODO: this is a baked in StaticArrays dependency
"""
    LeviCivitaArray

A `StaticArray` the elements of which are the elements of the Levi-Civita symbol ``\\epsilon``.
"""
struct LeviCivitaArray{S,T<:Number,n} <: StaticArray{S,T,n}
    coeff::T
end

function LeviCivitaArray(::Val{n}, coeff::Number=one(Float)) where {n}
    LeviCivitaArray{NTuple{n,n},typeof(coeff),n}(coeff)
end

Base.IndexStyle(::Type{<:LeviCivitaArray}) = IndexCartesian()

function Base.getindex(ϵ::LeviCivitaArray{S,T,n}, idx::Vararg{Int,n}) where {S,T,n}
    ϵ.coeff * sgn(idx...)
end

# StaticArrays seems to need this method for SArray conversion
function Base.getindex(ϵ::LeviCivitaArray{S,T,n}, j::Int) where {S,T,n}
    # we've stolen this code from what *would* be called via Base
    Base.@_propagate_inbounds_meta
    Base._getindex(IndexCartesian(), ϵ, Base.to_indices(ϵ, (j,))...)
end

