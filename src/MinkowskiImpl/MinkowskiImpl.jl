module MinkowskiImpl

using ..Ricci: @_submodule_imports
@_submodule_imports

import LorentzGroup
using LorentzGroup: Lorentz, LorMatrix, RotX, RotY, RotZ, BoostX, BoostY, BoostZ
using LorentzGroup: minkowski


struct Minkowski{n} <: Lorentzian{n} end
const Minkowski4 = Minkowski{4}


struct Cartesian{n} <: Atlas{Minkowski{n}} end
const Cartesian4 = Cartesian{4}

struct Polar1Patch4 <: Atlas{Minkowski{4}} end

# this is a super special case because it's only one for which basis type is *both*
# cartesian and holonomic...
# for various reasons it's easier to call it holonomic 
const CartesianRep{n} = Representation{Minkowski{n},Cartesian{n},Holonomic}
const CartesianRep4 = CartesianRep{4}

const P1HolRep4 = Representation{Minkowski4,Polar1Patch4,Holonomic}
const P1OrthRep4 = Representation{Minkowski4,Polar1Patch4,Orthonormal}

const MCPoint{n} = Coord1Point{Minkowski{n},Cartesian{n}}

const MC4Point = Coord1Point{Minkowski4,Cartesian4}

const MP1Point4 = Coord1Point{Minkowski4,Polar1Patch4}


function squaredistance(p::MCPoint{n}, q::MCPoint{n}) where {n}
    δ = data(q) - data(p)
    LorentzianSignature{n,dataeltype(p)}()(δ, δ)
end

distance(p::MCPoint{n}, q::MCPoint{n}) where {n} = √(abs(squaredistance(p, q)))

function distance(p::MP1Point4, q::MP1Point4)
    with(CoordTransform(Cartesian4(), Polar1Patch4()), p, q) do p′, q′
        distance(p′, q′)
    end
end

defaulttype(::Type{Point}, ::Type{Cartesian{n}}) where {n} = MCPoint{n}
defaulttype(::Type{Point}, ::Type{Polar1Patch4}) = MP1Point4

@inline _polar_coord_r(x::AbstractArray1) = √(x[1]^2 + x[2]^2 + x[3]^2)
@inline _polar_coord_θ(x::AbstractArray1, r::Number=_polar_coord_r(x)) = asin(x[3]/r)
@inline function _polar_coord_ϕ(x::AbstractArray1)
    b = √(x[1]^2 + x[2]^2)
    sign(x[2])*acos(x[1]/b)
end

function coordtransform_ondata(::Polar1Patch4, ::Cartesian4, x::AbstractArray1, pidx::Integer=1)
    r = _polar_coord_r(x)
    θ = _polar_coord_θ(x, r)
    ϕ = _polar_coord_ϕ(x)
    dat = @SVector eltype(x)[r, θ, ϕ, x[4]]
    (dat, 1)
end

function coordtransform_ondata(::Cartesian4, ::Polar1Patch4, ξ::AbstractArray1, pidx::Integer=1)
    (r, θ, ϕ, t) = ξ
    (sθ, cθ) = sincos(θ)
    (sϕ, cϕ) = sincos(ϕ)
    x = r*cθ*cϕ
    y = r*cθ*sϕ
    z = r*sθ
    dat = @SVector eltype(ξ)[x, y, z, t]
    (dat, 1)
end

include("tensors.jl")
include("diffeomorphisms.jl")
include("worldlines.jl")


end
