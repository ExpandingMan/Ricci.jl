
struct InertMCWorldline{V<:Vector{Minkowski{4}},D<:Diffeomorphism{Minkowski{4}}} <: InertialWorldline{Minkowski{4}}
    vector::V  # tangent at s=0, must be normed
    # we keep the boost in the data structure so that we don't have to keep re-calculating
    boost::D  # stored so it doesn't have to be re-computed
end

InertMCWorldline(u::Vector{Minkowski{4}}) = InertMCWorldline(u, comoving(u))

InertialWorldline(u::MCVector) = InertMCWorldline(u)

(l::InertMCWorldline)(s::Number) = geodesic(l.vector, s)

tangent(l::InertMCWorldline, s::Number) = paralleltransport(l.vector, s)

(𝒻::Diffeomorphism{Minkowski{4}})(l::InertMCWorldline) = InertMCWorldline(𝒻(l.vector))

comoving(l::InertMCWorldline, s::Number) = l.boost

invcomoving(l::InertMCWorldline, s::Number) = inv(l.boost)

function timeofdistance(l::InertMCWorldline, p::MCPoint)
    let u = data(l.vector), x = data(p), y = data(Point(l.vector))
        minkowski(u, y - x)
    end
end

function distance(l::InertMCWorldline, p::MCPoint)
    s = timeofdistance(l, p)
    distance(p, l(s))
end

# this is very efficient in MC
velocity(l::InertialWorldline, p::MCPoint) = paralleltransport(l.vector, p)

