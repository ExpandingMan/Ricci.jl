
struct MCTranslation{n,P<:Point{Minkowski{n}}} <: Diffeomorphism{Minkowski{n}}
    origin::P
end

_apply(𝒻::MCTranslation{n}, p::MCPoint{n}) where {n} = MCPoint{n}(data(p) - data(𝒻.origin))
_applyinv(𝒻::MCTranslation{n}, p::MCPoint{n}) where {n}  = MCPoint{n}(data(p) + data(𝒻.origin))

_apply(𝒻::MCTranslation{n}, u::MCVector{n}) where {n} = MCVector{n}(_apply(𝒻, Point(u)), data(u))
_applyinv(𝒻::MCTranslation{n}, u::MCVector{n}) where {n} = MCVector{n}(_applyinv(𝒻, Point(u)), data(u))


struct MCLorentz{L<:Lorentz} <: Diffeomorphism{Minkowski{4}}
    transform::L
    invtransform::L  # we need this so much it's easier to just keep it here; might need to change type
end

function MCLorentz(u::Vector{Minkowski{4}})
    Λ = LorentzGroup.comoving(data(u))
    MCLorentz(Λ, inv(Λ))
end

_apply(𝒻::MCLorentz, p::MCPoint{4}) = MCPoint{4}(𝒻.transform*data(p))
_applyinv(𝒻::MCLorentz, p::MCPoint{n}) where {n} = MCPoint{4}(𝒻.invtransform*data(p))

_apply(𝒻::MCLorentz, u::MCVector{4}) = MCVector{4}(_apply(𝒻, Point(u)), 𝒻.transform*data(u))
_applyinv(𝒻::MCLorentz, u::MCVector{4}) = MCVector{4}(_applyinv(𝒻, Point(u)), 𝒻.invtransform*data(u))

function comoving(u::MCVector{4})
    ϕ1 = MCLorentz(u)
    ϕ2 = MCTranslation(Point(_apply(ϕ1, u)))
    ϕ2 ∘ ϕ1
end

function invcomoving(u::MCVector{4})
    # this isn't great, maybe change in LorentzGroup.jl
    ϕ1 = MCLorentz(-project(spacelike, u) + project(timelike, u))
    ϕ2 = inv(MCTranslation(Point(u)))
    ϕ2 ∘ ϕ1
end

