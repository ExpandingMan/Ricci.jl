
const MCTensor{n,r,s,P,D} = GTensor{Minkowski{n},CartesianRep{n},r,s,P,D}
const MCVector{n,P,D} = GVector{Minkowski{n},CartesianRep{n},P,D}
const MCOneForm{n,P,D} = GOneForm{Minkowski{n},CartesianRep{n},P,D}

const MC4Tensor{r,s,P,D} = MCTensor{4,r,s,P,D}
const MC4Vector{P,D} = MCVector{4,P,D}
const MC4OneForm{P,D} = MCOneForm{4,P,D}

const MCMetric{n} = GMetric{Minkowski{n},CartesianRep{n}}
const MC4Metric = MCMetric{4}

const MP1H4Tensor{r,s,P,D} = GTensor{Minkowski4,P1HolRep4,r,s,P,D}
const MP1H4Vector{P,D} = GVector{Minkowski4,P1HolRep4,P,D}
const MP1H4OneForm{P,D} = GOneForm{Minkowski4,P1HolRep4,P,D}
const MP1H4Metric = GMetric{Minkowski4,P1HolRep4}

const MP1O4Tensor{r,s,P,D} = GTensor{Minkowski4,P1OrthRep4,r,s,P,D}
const MP1O4Vector{P,D} = GVector{Minkowski4,P1OrthRep4,P,D}
const MP1O4OneForm{P,D} = GOneForm{Minkowski4,P1OrthRep4,P,D}

# these are a bit more efficient than generic ADM impl
function data(::SpacelikeComponents{<:ADMCoordBasis{Minkowski{n}}}, v::MCVector{n}) where {n}
    T = dataeltype(v)
    SVector{n-1,T}(ntuple(j -> v.data[j], n-1))
end
function data(::TimelikeComponents{<:ADMCoordBasis{Minkowski{n}}}, v::MCVector{n}) where {n}
    T = dataeltype(v)
    SVector{1,T}((v.data[end],))
end
function data(::SpacelikeComponents{<:ADMCoordBasis{Minkowski{n}}}, v::MCOneForm{n}) where {n}
    T = dataeltype(v)
    SMatrix{1,n-1,T}(ntuple(j -> v.data[j], n-1))
end
function data(::TimelikeComponents{<:ADMCoordBasis{Minkowski{n}}}, v::MCOneForm{n}) where {n}
    T = dataeltype(v)
    SMatrix{1,1}((v.data[end],))
end

Base.:(*)(Λ::Lorentz, u::MCVector) = MCVector(Point(u), Λ*data(u))

@stable Vector(v::MCOneForm{n}) where {n} = MCVector{n}(Point(v), negatelast(data(v)))

@stable function Vector(u::MP1H4OneForm)
    x = data(Point(u), Polar1Patch4())
    (r, θ) = (x[1], x[2])
    r2 = r^2
    ud = data(u)
    dat = @SVector eltype(ud)[
        ud[1], ud[2]/r2, ud[3]/(r2*cos(θ)^2), -ud[4],
    ]
    MP1H4Vector(Point(u), dat)
end

@stable OneForm(v::MCVector{n}) where {n} = MCOneForm{n}(Point(v), negatelast(data(v)))

@stable function OneForm(v::MP1H4Vector)
    x = data(Point(v), Polar1Patch4())
    (r, θ) = (x[1], x[2])
    r2 = r^2
    vd = data(v)
    dat = @SMatrix eltype(vd)[
        vd[1] vd[2]*r2 vd[3]*r2*cos(θ)^2 -vd[4]
    ]
    MP1H4OneForm(Point(v), dat)
end

function (η::MCMetric{n})(u::Vector{Minkowski{n}}, v::Vector{Minkowski{n}}) where {n}
    udat = data(u, CartesianRep{n}())
    vdat = data(v, CartesianRep{n}())
    udat ⋅ negatelast(vdat)
end

function (η::MP1H4Metric)(u::Vector{Minkowski4}, v::Vector{Minkowski4})
    x = data(Point(η), Polar1Patch4())
    (r, θ) = (x[1], x[2])
    r2 = r^2
    ud = data(u, Representation(η))
    vd = data(v, Representation(η))
    ud[1]*vd[1] + ud[2]*vd[2]*r2 + ud[3]*vd[3]*r2*cos(θ)^2 - ud[4]*vd[4]
end

function (η::InvMetric{Minkowski{n},<:MCMetric{n}})(u::OneForm{Minkowski{n}}, v::OneForm{Minkowski{n}}) where {n}
    u = data(u, CartesianRep{n}())
    v = data(v, CartesianRep{n}())
    u ⋅ negatelast(v)
end

function (η::InvMetric{Minkowski4,<:MP1H4Metric})(u::OneForm{Minkowski4}, v::OneForm{Minkowski4})
    x = data(Point(η), Polar1Patch4())
    (r, θ) = (x[1], x[2])
    r2 = r^2
    ud = data(u, Representation(η))
    vd = data(v, Representation(η))
    ud[1]*vd[1] + ud[2]*vd[2]/r2 + ud[3]*vd[3]/(r2*cos(θ)^2) - ud[4]*vd[4]
end

metricdata(::Cartesian{n}, x::AbstractArray1, ::Integer=1) where {n} = data(LorentzianSignature{n,eltype(x)}())
invmetricdata(::Cartesian{n}, x::AbstractArray1, ::Integer=1) where {n} = data(LorentzianSignature{n,eltype(x)}())


@stable function metricdata(::Polar1Patch4, x::AbstractArray1, pidx::Integer=1)
    (r, θ) = (x[1], x[2])
    r2 = r^2
    @SMatrix eltype(x)[
        1  0           0  0
        0 r2           0  0
        0  0 r2*cos(θ)^2  0
        0  0           0 -1
    ]
end
@stable function invmetricdata(::Polar1Patch4, x::AbstractArray1, pidx::Integer=1)
    (r, θ) = (x[1], x[2])
    r2 = r^2
    @SMatrix eltype(x)[
        1    0               0  0
        0 1/r2               0  0
        0    0 1/(r2*cos(θ)^2)  0
        0    0               0 -1
    ]
end

@stable function vielbeindata(p::Point{Minkowski{n}}, ::Type{<:Cartesian{n}}) where {n}
    Diagonal(ones(SVector{n,dataeltype(p)}))
end

@stable function covielbeindata(p::Point{Minkowski{n}}, ::Type{<:Cartesian{n}}) where {n}
    Diagonal(ones(SVector{n,dataeltype(p)}))
end

@stable function vielbeindata(p::Point{Minkowski4}, ::Type{<:Polar1Patch4})
    x = data(p, Polar1Patch4())
    (r, θ) = (x[1], x[2])
    @SMatrix eltype(x)[
        1 0        0 0
        0 r        0 0
        0 0 r*cos(θ) 0
        0 0        0 1
    ]
end

@stable function covielbeindata(p::Point{Minkowski4}, ::Type{<:Polar1Patch4})
    x = data(p, Polar1Patch4())
    (r, θ) = (x[1], x[2])
    @SMatrix eltype(x)[
        1   0            0 0
        0 1/r            0 0
        0   0 1/(r*cos(θ)) 0
        0   0            0 1
    ]
end

∂phamiltonian_ondata(::Cartesian, q, p) = negatelast(p)

_integrate_ondata(q0, p0, s::Real) = (q0 + s*negatelast(p0), p0)

# confirmed this does *not* seem to have any performance penalty as opposed to direct
defaultintegrator(::CartesianRep) = Ricci.SymplIntegratorSolution(_integrate_ondata)

defaultintegrator(::P1HolRep4) = Ricci.CoordChangeSymplIntegrator{Cartesian4}(defaultintegrator(CartesianRep4()))

pointsto(p::MCPoint{n}, q::MCPoint{n}) where {n} = normalize(MCVector(p, data(q) - data(p)))

"""
    paralleltransport(u::Vector, p::Point)

Parallel transport the vector ``u`` to the point ``p`` along the geodesic connecting them.
"""
paralleltransport(u::Vector{Minkowski{n}}, p::Point{Minkowski{n}}) where {n} = constructortype(u)(p, data(u))

#WARN: this is fucked now, need to rework with ADM
function nullpointsto(c::Union{Spacelike,Timelike}, p::MCPoint{n}, q::MCPoint{n}) where {n}
    normalizeproj(c, MCVector(p, data(q) - data(p)))
end

function nullpointsfrom(c::Union{Spacelike,Timelike}, p::MCPoint{n}, q::MCPoint{n}) where {n}
    normalizeproj(c, MCVector(q, data(q) - data(p)))
end

∂qhamiltonian_ondata(::Cartesian, q::AbstractArray1, p::AbstractArray1) = zero(p)

