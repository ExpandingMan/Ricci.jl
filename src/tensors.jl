
#WARN: some of the numerical type promotion stuff might already have problems with grassmann numbers,
# see for example the `zero` stuff...

"""
    Tensor{M<:Manifold,r,s}

A tensor of the tangent space of a point ``p`` on the manifold ``M``.

All subtypes of `Tensor` are required to hold a [`Point`](@ref) object referencing the point to which they are tangent
which can be accessed via `Point(v::Tensor)`.

These tensors do *not* satisfy the `Base.AbstractArray` interface, but their data must be accessible as an
`Base.AbstractArray`, via [`data`](@ref).  The result is guaranteed to be an array with
`size(data(v)) == ntuple(ndimensions(M), r+s)`.  The tensor data needn't be stored permanently in the `Tensor`
object and may be computed on a call to `data`.

Tensor data requires specification of a vector basis to be useful.  This basis may either be holonomic or
orthonormal.  The basis type of a `Tensor` object is given by [`BasisType`](@ref).

TODO: like a lot of other stuff, this documentation gets rather subtle, be sure to fill it out in
the typst paper before writing too much here.

All tensors should have, at minimum, a constructor
```julia
TensorType(p::Point{M}, data::AbstractArray)
```
where `data` is tensor data in the representation described by `BasisType(TensorType)`.
"""
abstract type Tensor{M<:Manifold,r,s} end

abstract type Metric{M<:Manifold} <: Tensor{M,0,2} end

abstract type InverseMetric{M<:Manifold} <: Tensor{M,2,0} end

const Vector{M} = Tensor{M,1,0}
const OneForm{M} = Tensor{M,0,1}

Manifold(::Type{<:Tensor{M}}) where {M} = M()
Manifold(u::Tensor) = Manifold(typeof(u))

Point(u::Tensor) = u.point  # fallback

# we are pretty far down the road of this being a default
BasisType(::Type{<:Tensor}) = Holonomic()

BasisType(u::Tensor) = BasisType(typeof(u))

BasisType(::Type{T1}, ::Type{T2}) where {T1<:Tensor,T2<:Tensor} = BasisType(BasisType(T1), BasisType(T2))
BasisType(u::Tensor{M}, v::Tensor{M}) where {M<:Manifold} = BasisType(typeof(u), typeof(v))

Representation(::Type{T}) where {M,T<:Tensor{M}} = Representation{M,typeof(Atlas(T)),typeof(BasisType(T))}()

Atlas(u::Tensor) = Atlas(typeof(u))

function Tensor{M,r,s}(rep::Representation{M}, x::Point{M}, dat::AbstractArray) where {M,r,s}
    defaulttype(Tensor{M,r,s}, rep)(x, dat)
end
Vector(rep::Representation{M}, x::Point{M}, dat::AbstractArray1) where {M} = defaulttype(Vector, rep)(x, dat)

#====================================================================================================
1-form data is a `AbstractArray1`, *not* a row vector.
The reason is that, were we to follow that logic, matrices would *always* be like A^a_b,
but in practice we will frequently have matrices representing fully contra or covariant

this will all change if we ever take the huge step of doing tensor data "correctly"
====================================================================================================#
OneForm(rep::Representation{M}, x::Point{M}, dat::AbstractArray1) where {M} = defaulttype(OneForm, rep)(x, dat)

function Tensor{M,r,s}(atl::Atlas{M}, x::Point{M}, dat::AbstractArray) where {M,r,s}
    Tensor{M,r,s}(Representation(atl, Holonomic()), x, dat)
end
Vector(atl::Atlas{M}, x::Point{M}, dat::AbstractArray1) where {M} = Vector(Representation(atl, Holonomic()), x, dat)
OneForm(atl::Atlas{M}, x::Point{M}, dat::AbstractArray) where {M} = OneForm(Representation(atl, Holonomic()), x, dat)

Tensor{M,r,s}(x::Point{M}, dat::AbstractArray) where {M,r,s} = Tensor{M,r,s}(Atlas(x), x, dat)
Vector(x::Point, dat::AbstractArray1) = Vector(Atlas(x), x, dat)
OneForm(x::Point, dat::AbstractArray) = OneForm(Atlas(x), x, dat)

pointtype(u::Tensor) = pointtype(typeof(u))

# this is a fallback that can be overridden, though not really tested so may break
dataeltype(::Type{T}) where {T<:Tensor} = dataeltype(pointtype(T))
dataeltype(u::Tensor) = dataeltype(typeof(u))


"""
    data(u::Tensor)

Returns the data of the tensor.  The array shape of the result is guaranteed to match the tensor shape.
If no `Representation` argument is given, this is guaranteed to return the data in the representation
given by `Representation(u)`.
"""
data(u::Tensor) = u.data

_data(::R, u::Tensor, ::R) where {R<:Representation} = data(u)

# this is fallback that will cause error if rep not available
function _data(ownrep::ownR, u::Tensor, rep::R) where {ownR<:Representation,R<:Representation}
    # usinng the diffeo here might seem overly ellaborate, but most of the time will need
    # all of its machinations and it shouldn't be any more expensive
    # specific implementations can overload this if needed
    𝒻 = CoordTransform(Atlas(rep), Atlas(ownrep))
    data(_apply(𝒻, u))
end

data(u::Tensor, rep::Representation) = _data(Representation(u), u, rep)

datatype(u::Tensor) = typeof(data(u))  # fallback

_atlas(::A, ::A, u::Tensor{M}) where {M,A<:Atlas{M}} = u
function _atlas(atl′::Atlas{M}, atl::Atlas{M}, u::Tensor{M}) where {M}
    # again, no reason not to use diffeo here, even though it looks ellaborate
    𝒻 = CoordTransform(atl′, atl)
    _apply(𝒻, u)
end

(atl::Atlas{M})(u::Tensor{M}) where {M} = _atlas(atl, Atlas(u), u)

_basistype(::B, ::B, u::Tensor) where {B<:BasisType} = u

function _basistype(::Orthonormal, ::Holonomic, u::Vector)
    p = Point(u)
    e = vielbeindata(p, Atlas(u))
    rep = Representation(Atlas(u), Orthonormal())
    defaulttype(Vector, rep)(p, e*data(u))
end
function _basistype(::Holonomic, ::Orthonormal, u::Vector)
    p = Point(u)
    e = covielbeindata(p, Atlas(u))
    rep = Representation(Atlas(u), Holonomic())
    defaulttype(Vector, rep)(p, e*data(u))
end

function _basistype(::Orthonormal, ::Holonomic, u::OneForm)
    p = Point(u)
    e = covielbeindata(p, Atlas(u))
    rep = Representation(Atlas(u), Orthonormal())
    defaulttype(OneForm, rep)(p, data(u)*e)
end
function _basistype(::Holonomic, ::Orthonormal, u::OneForm)
    p = Point(u)
    e = vielbeindata(p, Atlas(u))
    rep = Representation(Atlas(u), Holonomic())
    defaulttype(OneForm, rep)(p, data(u)*e)
end

(bt::BasisType)(u::Tensor) = _basistype(bt, BasisType(u), u)

_representation(::R, ::R, u::Tensor{M}) where {M,R<:Representation{M}} = u
function _representation(rep′::Representation{M}, rep::Representation{M}, u::Tensor{M}) where {M}
    u′ = Atlas(rep′)(u)
    BasisType(rep′)(u′)
end

(rep::Representation{M})(u::Tensor{M}) where {M} = _representation(rep, Representation(u), u)

vielbeindata(p::Point{M}, atl::Atlas{M}=Atlas(p)) where {M} = vielbeindata(p, typeof(atl))

covielbeindata(p::Point{M}, atl::Atlas{M}=Atlas(p)) where {M} = covielbeindata(p, typeof(atl))

# this is somewhat analogous to Base.similar
"""
    constructortype(u::Tensor)

Returns a type used as a constructor for tensors with the same [`Representation`](@ref) as `u`.
By default, this is merely `typeof(u)`, but tensors are free to define alternatives as long as they
are of the same `Representation`.
"""
constructortype(::Type{T}) where {M,r,s,T<:Tensor{M,r,s}} = GTensor{M,typeof(Representation(T)),r,s}
constructortype(u::Tensor) = constructortype(typeof(u))

patchindex(u::Tensor) = patchindex(Point(u))

defaulttype(::Type{Vector}, ::Type{R}) where {M,R<:Representation{M}} = defaulttype(Tensor{M,1,0}, R)
defaulttype(::Type{OneForm}, ::Type{R}) where {M,R<:Representation{M}} = defaulttype(Tensor{M,0,1}, R)

"""
    defaulttype(Point, ::Representation)
    defaulttype(Vector, ::Representation)
    defaulttype(OneForm, ::Representation)

Returns an appropriate default type of the object for the given representation, typically one
that explicitly stores the data.  This is required to return a type that can be used directly as
a constructor, see also [`constructortype`](@ref).
"""
defaulttype(::Type{T}, rep::Representation) where {T} = defaulttype(T, typeof(rep))

"""
    defaulttype(Vector, p::Point)
    defaulttype(OneForm, p::Point)

Return an appropriate default type of the object with a representation matching that of the provided
point, typically one that explicitly stores the data.  This is required to return a type that can
be used directly as a constructor.
"""
defaulttype(::Type{T}, p::Point{M}) where {M,T<:Tensor{M}} = defaulttype(T, Representation(p))

Base.:(+)(u::Tensor) = u

struct InvalidTensorComparison{U,V} <: Exception
    u::U
    v::V
end

function Base.showerror(io::IO, err::InvalidTensorComparison)
    println(io, "InvalidTensorComparison: executing code tried to compare tensors on different tangent spaces")
    print(io, "1st tensor point:\t")
    show(io, Point(err.u))
    println(io)
    print(io, "2nd tensor point:\t")
    show(io, Point(err.v))
end

function tangent_space_check(u::Tensor{M}, v::Tensor{M}; kw...) where {M}
    # this debug log is ok because we should never do it in performant code anyway
    @debug("DEBUG WARNING - tangent space check is being computed")
    if !(≈)(Point(u), Point(v); kw...)
        throw(InvalidTensorComparison(u, v))
    end
end
function tangent_space_check(u::Tensor{M}...; kw...) where {M}
    for j ∈ 2:length(u)
        tangent_space_check(u[1], u[j]; kw...)
    end
end

Representation(u::Tensor) = Representation(Atlas(u), BasisType(u))

Representation(x::Tensor, y::Tensor) = Representation(Representation(x), Representation(x))

Base.:(*)(a::Number, u::Tensor) = constructortype(u)(Point(u), a*data(u))

Base.:(/)(u::Tensor, a::Number) = inv(a)*u

add(::Representation{M}, u::Tensor{M}, v::Tensor{M}) where {M} = constructortype(u)(Point(u), data(u) + data(v))

Base.:(+)(u::Tensor, v::Tensor) = add(Representation(u, v), u, v)

Base.:(-)(u::Tensor) = constructortype(u)(Point(u), -data(u))

subtract(r::Representation{M}, u::Tensor{M}, v::Tensor{M}) where {M} = constructortype(u)(Point(u), data(u) - data(v))

Base.:(-)(u::Tensor, v::Tensor) = subtract(Representation(u, v), u, v)

#TODO: I'm nervous about defining both of these methods before we really have tensor operations worked out
mult(rep::Representation{M}, u::OneForm{M}, v::Vector{M}) where {M} = Metric(rep, Point(u))(Vector(u), v)
mult(rep::Representation{M}, u::Vector{M}, v::OneForm{M}) where {M} = Metric(rep, Point(u))(u, Vector(v))

function mult(rep::Representation{M}, P::Tensor{M,1,1}, t::Tensor{M,0,2}) where {M}
    dat = data(P, rep)'*data(t, rep)
    Tensor{M,0,2}(rep, Point(P), dat)
end

function mult(rep::Representation{M}, P::Tensor{M,1,1}, t::Tensor{M,2,0}) where {M}
    dat = data(P, rep)*data(t, rep)
    Tensor{M,2,0}(rep, Point(P), dat)
end

function mult(rep::Representation{M}, t::Tensor{M,0,2}, P::Tensor{M,1,1}) where {M}
    dat = data(t, rep)*data(P, rep)
    Tensor{M,0,2}(rep, Point(t), dat)
end

function mult(rep::Representation{M}, t::Tensor{M,2,0}, P::Tensor{M,1,1}) where {M}
    dat = data(t, rep)*data(P, rep)'
    Tensor{M,2,0}(rep, Point(t), dat)
end

Base.:(*)(u::Tensor, v::Tensor) = mult(Representation(u), u, v)

function inner(rep::Representation{M,A,<:Orthonormal}, u::Vector{M}, v::Vector{M}) where {M,A} 
    MetricSignature(dataeltype(u), M)(data(u, rep), data(v, rep))
end
inner(::Representation{M,A,<:Holonomic}, u::Vector{M}, v::Vector{M}) where {M,A} = Metric(Point(u))(u, v)

function inner(rep::Representation{M,A,<:Orthonormal}, u::OneForm{M}, v::OneForm{M}) where {M,A}
    MetricSignature(dataeltype(u), M)(data(u, rep)', data(v, rep)')
end
function inner(rep::Representation{M,A,<:Holonomic}, u::OneForm{M}, v::OneForm{M}) where {M,A}
    InverseMetric(rep, Point(u))(u, v)
end

#NOTE: this will get changed as I deem appropriate
"""
    defaultdiffbackend()

Returns an appropriate `AbstractADType` for use within Ricci.jl.  This is for "small", static uses
such as computing Christoffel symbols and curvature tensors.

**NOTE** As of writing, whatever backend this points to will _not_ be a dependency of Ricci.jl and
should be added by users.
"""
defaultdiffbackend() = StaticDiff(AutoForwardDiff())  #StaticDiff(AutoEnzyme())
defaultdiffbackend(x...) = defaultdiffbackend()

function dual(u::Vector)
    udown = OneForm(u)
    n = udown*u
    udown/n
end

function dual(u::OneForm)
    uup = Vector(u)
    n = u*uup
    uup/n
end

LinearAlgebra.:(⋅)(u::Vector{M}, v::Vector{M}) where {M} = inner(Representation(u), u, v)

hamiltonian(p::OneForm) = 𝐠inv(p, p)/2
hamiltonian(p::Vector) = hamiltonian(𝐠*p)

function ∂qhamiltonian_ondata end
function ∂phamiltonian_ondata end

@stable function ∂qhamiltonian_ondata(diffbackend::AbstractADType, rep::Representation, q, p)
    T = defaulttype(OneForm, rep)
    o = DI.gradient(diffbackend, q) do x
        u = T(Point(Atlas(rep), x), p)
        square(u)
    end
    o/2
end

@stable function ∂qhamiltonian_ondata(rep::Representation, q, p; diffbackend::AbstractADType=AutoForwardDiff())
    ∂qhamiltonian_ondata(diffbackend, rep, q, p)
end

#fallback: this generic method is cost-free in all tested cases
@stable function ∂phamiltonian_ondata(rep::Representation, q, p)
    x = Point(Atlas(rep), q)
    u = defaulttype(OneForm, rep)(x, p)
    data(Vector(u))
end

function ∂qhamiltonian(p::OneForm)
    xdata = data(Point(p), Atlas(p))
    pdata = data(p)
    dqdata = ∂qhamiltonian_ondata(Representation(p), xdata, pdata)
    constructortype(p)(Point(p), dqdata)
end
∂qhamiltonian(p::Vector) = ∂qhamiltonian(𝐠*p)

∂phamiltonian(p::OneForm) = 𝐠inv*p
∂phamiltonian(p::Vector) = p

"""
    exponential([,int], u::Vector, s::Number)

The exponential map of a vector.  This method takes an integrator as an argument, as opposed
to `Base.exp` which uses a default integrator.  The integrator is required to be a
[`SymplecticIntegrator`](@ref) object.  Integration is necessarily performed in the representation
of the provided vector.
"""
function exponential(int::SymplecticIntegrator, u::Vector, s::Number=one(Float))
    Point(integrate(int, u, s))
end

"""
    pointsto([null, ], p::Point, q::Point)

Returns a vector ``v`` at ``p`` which points toward the point ``q``, in the sense that ``q``
lies along the geodesic to which ``v`` is tangent.  The type of the returned vector will depend
on the type of points.

If the separation is specified to be null via the `null` argument, the resulting vector will have normalized
timelike and spacelike components.  Be warned that failing to pass `null` for null separations will cause
the program to attempt to normalize a null vector.
"""
function pointsto end

"""
    exp(v::Vector, s::Number=1.0)

Compute the exponential map of tangent vector `v`, returning a point on the manifold
with the same coordinates system as `v`.  The real parameter `s` extends the definition
of the exponential map to arbitrary points along the geodesic to which `v` is tangent.
Therefore, ``\\exp(v_p, t)`` returns the point in the direction of ``v`` at a proper distance
``s`` if ``v`` is normalized.
"""
Base.exp(int::SymplecticIntegrator, u::Vector, s::Number=one(Float)) = exponential(int, u, s)
Base.exp(u::Vector, s::Number=one(Float)) = exp(defaultintegrator(Representation(u)), u, s)

"""
    geodesic(u::Vector, t::Number)
    geodesic(u::Vector)

Returns the geodesic curve to which ``u`` is tangent as a real function returning `Point`.  The type of
the returned point will deend on the type of `u`.  If the argument ``t`` is omitted, the function
returns the geodesic as a function.
"""
geodesic(int::SymplecticIntegrator, u::Vector{M}, s::Number) where {M} = exponential(int, u, s)
geodesic(u::Vector, s::Number) = geodesic(defaultintegrator(Representation(u)), u, s)
geodesic(int::SymplecticIntegrator, u::Vector) = s::Number -> geodesic(int, u, s)
geodesic(u::Vector) = geodesic(defaultintegrator(Representation(u)), u)

"""
    paralleltransport(u::Vector, v::Vector, t::Number=1)
    paralleltransport(u::Vector, t::Number=1)

Parallel transport tangent vector ``u`` along the geodesic to which ``v`` is tangent a proper distance ``t``.
Returns a vector ``v_q`` at point ``q = \\exp(v_p, t)`` which is parallel to ``v_p``.
If the second vector argument is omitted, the vector is parallel transported along itself.

Note that ``v`` is *not* normalized i.e. longer vectors are transported further.
See [`normedparalleltransport`](@ref)
"""
function paralleltransport(int::SymplecticIntegrator, u::Vector{M}, v::Vector{M}, s::Number=one(Float)) where {M}
    constructortype(u)(exponential(int, v, s), data(u, Representation(v)))
end
function paralleltransport(u::Vector{M}, v::Vector{M}, s::Number=one(Float)) where {M} 
    paralleltransport(defaultintegrator(Representation(v)), u, v, s)
end
paralleltransport(int::SymplecticIntegrator, u::Vector, s::Number=one(Float)) = paralleltransport(int, u, u, s)
paralleltransport(u::Vector, s::Number=one(Float)) = paralleltransport(defaultintegrator(Representation(u)), u, s)

"""
    nullpointsto(::Character, p::Point{M}, q::Point{M})

For null-separated points ``p,q``, create a null vector pointing from ``p`` to ``q`` which is normalized
according to [`normalizeproj`](@ref).
"""
function nullpointsto end

function nullpointsfrom end


struct Identity{M<:Manifold,R<:Representation{M},P<:Point{M}} <: Tensor{M,1,1}
    point::P
end

function Identity(rep::Representation, p::Point{M}) where {M<:Manifold}
    Identity{M,typeof(rep),typeof(p)}(p)
end

Representation(::Identity{M,R}) where {M,R} = R()

data(::Identity{M}) where {M} = SDiagonal(@SVector(ones(ndimensions(M))))

# we don't have other methods because of our tensor algebra problems
# identity is technically lr(delta^a)_b right now
mult(::Representation, ::Identity{M}, v::Vector{M}) where {M} = v
mult(::Representation, v::OneForm{M}, ::Identity{M}) where {M} = v


# this is a fallback that reps can change
Metric(rep::Representation{M}, p::Point{M}) where {M<:Manifold} = GMetric{M,typeof(rep)}(p)
Metric(rep::Representation, p::Point{M}) where {M} = Metric(rep, dataeltype(p), p)

# this is a reasonable default because doesn't make a lot of sense to want orthonormal
function Metric(p::Point{M}) where {M}
    rep = Representation(Atlas(p), Holonomic())
    Metric(rep, p)
end

@inline function data(g::Metric{M}, rep::Representation{M,A}=Representation(g)) where {M<:Manifold,A<:Atlas{M}}
    atl = Atlas(rep)
    xdat = data(Point(g), atl)
    pidx = patchindex(Point(g), atl)
    metricdata(atl, xdat, pidx)
end

LinearAlgebra.det(g::Metric) = det(data(g))

"""
    metricdata(atl::Atlas, x::AbstractArray1, pidx::Integer=1)

A low level method for returning the metric component data at a point parameterized by coordinate array
`x` on coordinate patch indexed `pidx`.  Atlas implementations should implement this.  Note that the
element type of the resulting matrix is required to be the same as that of `x`.
"""
metricdata(x::Point{M}, atl::Atlas{M}=Atlas(x)) where {M} = metricdata(atl, data(x, atl), patchindex(x, atl))

struct InvMetric{M,G<:Metric{M}} <: InverseMetric{M}
    metric::G
end

# this is a fallback that reps can change
InverseMetric(rep::Representation, ::Type{T}, p::Point{M}) where {T<:Number,M<:Manifold} = inv(Metric(rep, T, p))
InverseMetric(rep::Representation, p::Point) = inv(Metric(rep, p))
InverseMetric(p::Point) = inv(Metric(p))

Atlas(::Type{<:InvMetric{M,G}}) where {M,G} = Atlas(G)
BasisType(::Type{<:InvMetric{M,G}}) where {M,G} = BasisType(G)

Point(g::InvMetric) = Point(g.metric)

Base.inv(g::Metric) = InvMetric(g)
Base.inv(g::InvMetric) = g.metric

@inline function data(g::InverseMetric{M}, rep::Representation{M,A}=Representation(g)) where {M<:Manifold,A<:Atlas{M}}
    atl = Atlas(rep)
    xdat = data(Point(g), atl)
    pidx = patchindex(Point(g), atl)
    invmetricdata(atl, xdat, pidx)
end

"""
    invmetricdata(atl::Atlas, x::AbstractArray1, pidx::Integer=1)

A low level method for returning the inverse metric componenent data at a point parameterized by coordinate
array `x` on coordinate patch indexed `pidx`.  By default, the result is obtained by computing the matrix
inverse of `metricdata(atl, x, pidx)`.  Atlas implementations preferrably implement their own method
to avoid the cost of matrix inversion.
"""
invmetricdata(atl::Atlas, xdat::AbstractArray1, pidx::Integer=1) = inv(metricdata(atl, xdat, pidx))
invmetricdata(x::Point{M}, atl::Atlas{M}=Atlas(x)) where {M} = invmetricdata(atl, data(x, atl), patchindex(x, atl))

mult(rep::Representation, ginv::InvMetric{M}, g::Metric{M}) where {M} = Identity(rep, Point(g))
mult(rep::Representation, g::Metric{M}, ginv::InvMetric{M}) where {M} = Identity(rep, Point(g))

# these are fallbacks, will typically be defined with more efficient methods
function mult(rep::Representation, g::Metric{M}, u::Vector{M}) where {M}
    gdat = data(g, rep)
    udat = data(u, rep)
    OneForm(Point(u), gdat*udat)
end
function mult(rep::Representation, ginv::InverseMetric{M}, u::OneForm{M}) where {M}
    gdat = data(ginv, rep)
    udat = data(u, rep)
    OneForm(Point(u), u*gdat)
end

square(v::Vector) = Metric(Representation(v), Point(v))(v, v)
square(v::OneForm) = InverseMetric(Representation(v), Point(v))(v, v)

# square is separate from inner in case we want more efficient method
Base.@constprop :aggressive function Base.:(^)(u::Vector, n::Integer)
    n == 2 || throw(ArgumentError("vectors only support squaring, not being raised to other powers"))
    square(u)
end

"""
    Character(u::Vector; kw...)

Returns whether `u` is `null`, `spacelike` or `timelike`.  This requires run-time checking of vector
data for most vector implementations, so it can introduce type instability.  Keyword arguments
are passed to `Base.isapprox` and are only used for checking the null case.
"""
function Character(u::Vector; kw...)
    u2 = square(u)
    if (≈)(u2, zero(dataeltype(u)); kw...)
        null
    elseif u2 > 0
        spacelike
    else
        timelike
    end
end

# obviously this silently blows up for null vectors
LinearAlgebra.normalize(u::Vector) = (one(dataeltype(u))/√(abs(square(u))))*u
LinearAlgebra.normalize(u::OneForm) = (one(dataeltype(u))/√(abs(square(u))))*u


"""
    AutoMetric (𝐠)

This object allows for simple shorthand notation, especially for raising and lowering indices.
See also [`InvAutoMetric`](@ref).
"""
struct AutoMetric end
const 𝐠 = AutoMetric()

"""
    InvAutoMetric (𝐠inv)

This object allows for simple shorthand notation, especially for raising and lowering indcies.
See also [`AutoMetric`](@ref).
"""
struct InvAutoMetric end
const 𝐠inv = InvAutoMetric()

(::AutoMetric)(u::Vector, v::Vector) = Metric(Point(u))(u, v)
(::InvAutoMetric)(u::OneForm, v::OneForm) = InverseMetric(Point(u))(u, v)

# these fallbacks will typically be replaced with more efficient methods
# at some point we will have a more general way of raising/lowering indices
OneForm(u::Vector) = Metric(Point(u))*u
OneForm(u::OneForm) = u

Vector(u::OneForm) = InverseMetric(Point(u))*u
Vector(u::Vector) = u

Base.:(*)(::AutoMetric, u::Vector) = OneForm(u)
Base.:(*)(u::Vector, ::AutoMetric) = 𝐠*u
Base.:(*)(::InvAutoMetric, u::OneForm) = Vector(u)
Base.:(*)(::OneForm, ::InvAutoMetric) = 𝐠inv*u


function equals(::Representation{M}, u::Tensor{M}, v::Tensor{M}) where {M}
    Point(u) == Point(v) || return false
    data(u) == data(v)
end

function Base.:(==)(u::Tensor, v::Tensor)
    R = Representation(u)
    R == Representation(v) || return false
    equals(R, u, v)
end

function approx(::Representation{M}, u::Tensor{M}, v::Tensor{M}; kw...) where {M}
    (≈)(Point(u), Point(v); kw...) || return false
    (≈)(data(u), data(v); kw...)
end

function Base.:(≈)(u::T, v::T; kw...) where {T<:Tensor}
    R = Representation(u)
    R == Representation(v) || return false
    approx(u, v; kw...)
end

#TODO: does this still make sense? don't even remember what this was for
function randnat(rng::AbstractRNG, ::Type{V}, ::Type{DT}, p::P) where {M,DT<:AbstractArray1,V<:Vector{M},P<:Point{M}}
    V(p, randn(rng, DT))
end


# type system forces us to store ndimensions(M) which maybe isn't so bad
struct LeviCivita{M,R<:Representation{M},P<:Point{M},n} <: Tensor{M,0,n}
    point::P

    LeviCivita{M,R}(x::Point{M}) where {M,R<:Representation{M}} = new{M,R,typeof(x),ndimensions(M)}(x)
end

LeviCivita(rep::Representation{M}, x::Point{M}) where {M} = LeviCivita{M,typeof(rep)}(x)
LeviCivita(atl::Atlas{M}, x::Point{M}) where {M} = LeviCivita(Representation(atl, Holonomic()), x)
LeviCivita(x::Point) = LeviCivita(Atlas(x), x)

Atlas(::Type{<:LeviCivita{M,R}}) where {M,R} = Atlas(R)
BasisType(::Type{<:LeviCivita{M,R}}) where {M,R} = BasisType(R)

# I had considered storing the data here instead of doing this, but it is suprisingly fast as is
function data(ϵ::LeviCivita{M,R,P,n}) where {M,R,P,n}
    gdet = det(Metric(Representation(ϵ), Point(ϵ)))
    LeviCivitaArray(Val(n), √(abs(gdet)))
end


struct LeviCivitaUp{M,R<:Representation{M},P<:Point{M},n} <: Tensor{M,n,0}
    point::P

    LeviCivitaUp{M,R}(x::Point{M}) where {M,R<:Representation{M}} = new{M,R,typeof(x),ndimensions(M)}(x)
end

LeviCivitaUp(rep::Representation{M}, x::Point{M}) where {M} = LeviCivitaUp{M,typeof(rep)}(x)
LeviCivitaUp(atl::Atlas{M}, x::Point{M}) where {M} = LeviCivitaUp(Representation(atl, Holonomic()), x)
LeviCivitaUp(x::Point) = LeviCivitaUp(Atlas(x), x)

Atlas(::Type{<:LeviCivitaUp{M,R}}) where {M,R} = Atlas(R)
BasisType(::Type{<:LeviCivitaUp{M,R}}) where {M,R} = BasisType(R)

function data(ϵ::LeviCivitaUp{M,R,P,n}) where {M,R,P,n}
    gdet = det(Metric(Representation(ϵ), Point(ϵ)))
    LeviCivitaArray(Val(n), sign(gdet)*√(abs(gdet)))
end


"""
    Projector <: Tensor{M,1,1}

A tensor of the form ``\\pm v^a w_b`` which acts as a projector from ``w^a`` to ``v^a``.  The type
signature contains `Character` which is intended to describe the character of ``w^a``, so that
e.g. projectors from timelike directions do not flip the sign of the result.
"""
struct Projector{M,C<:Character,V<:Vector{M},W<:OneForm{M}} <: Tensor{M,1,1}
    vector::V
    oneform::W
end

function Projector(c::Character, v::Vector{M}, w::OneForm{M}=OneForm(v)) where {M}
    Projector{M,typeof(c),typeof(v),typeof(w)}(v, w)
end
Projector(c::Character, w::OneForm{M}) where {M} = Projector(c, Vector(w), w)

Point(P::Projector) = Point(P.vector)

pointtype(::Type{<:Projector{M,C,V}}) where {M,C,V} = pointtype(V)

Representation(::Type{<:Projector{M,C,V}}) where {M,C,V} = Representation(V)

dataeltype(::Type{<:Projector{M,C,V}}) where {M,C,V} = dataeltype(V)

function data(P::Projector{M,C}) where {M,C}
    v = P.vector
    w = P.oneform
    s = squaredsign(C, dataeltype(P))
    s .* data(v) .* data(w)
end

function mult(::Representation{M}, P::Projector{M,C}, u::Vector{M}) where {M,C}
    v = P.vector
    w = P.oneform
    s = squaredsign(C, dataeltype(P))
    s * (w*u) * v
end

function mult(::Representation{M}, u::OneForm{M}, P::Projector{M,C}) where {M,C}
    v = P.vector
    w = P.oneform
    s = squaredsign(C, dataeltype(P))
    s * (u*v) * w
end


"""
    CoProjector <: Tensor{M,1,1}

A tensor of the form ``\\delta^a_b \\pm v^a w_b`` which acts as a projector onto the complement of
``v^a``.  This is the complement of the [`Projector`](@ref) type.  The type signature contains
`Character` which is intended to describe the character of ``w^a``, so that e.g. projectors
from timelike directions do not flip the sign of the result.
"""
struct CoProjector{M,C<:Character,V<:Vector{M},W<:OneForm{M}} <: Tensor{M,1,1}
    vector::V
    oneform::W
end

function CoProjector(c::Character, v::Vector{M}, w::OneForm{M}=OneForm(v)) where {M}
    CoProjector{M,typeof(c),typeof(v),typeof(w)}(v, w)    
end
CoProjector(c::Character, w::OneForm{M}) where {M} = CoProjector(c, Vector(w), w)

Point(P::CoProjector) = Point(P.vector)

pointtype(::Type{<:CoProjector{M,C,V}}) where {M,C,V} = pointtype(V)

Representation(::Type{<:CoProjector{M,C,V}}) where {M,C,V} = Representation(V)

dataeltype(::Type{<:CoProjector{M,C,V}}) where {M,C,V} = dataeltype(V)

function data(P::CoProjector{M,C}) where {M,C}
    δ = SDiagonal(@SVector(ones(ndimensions(M))))
    v = P.vector
    w = P.oneform
    s = squaredsign(C, dataeltype(P))
    δ - s * data(v) .* data(w)
end

function mult(::Representation{M}, P::CoProjector{M}, u::Vector{M}) where {M}
    v = P.vector
    w = P.oneform
    s = squaredsign(C, dataeltype(P))
    u - s * (w*u)*v
end

function mult(::Representation{M}, u::OneForm{M}, P::CoProjector{M}) where {M}
    v = P.vector
    w = P.oneform
    s = squaredsign(C, dataeltype(P))
    u - s * (u*v)*w
end


_at(p::Point, td::Pair{Type{T},A}) where {T,A} = constructortype(T)(p, td[2])
_at(p::Point, td::Pair{DataType,A}) where {A} = constructortype(td[1])(p, td[2])
_at(p::Point{M}, data::AbstractArray1) where {M} = defaulttype(Vector, p)(p, data)

"""
    At

This is a wrapper for tuples of tensors which are guaranteed to be at the same point.
"""
struct At{M,N,T<:Tuple}
    tensors::T
end

function At(p::Point{M}, args...) where {M} 
    tpl = map(a -> _at(p, a), args)
    At{M,length(args),typeof(tpl)}(tpl)
end

Point(a::At) = Point(a.tensors[1])

Base.length(a::At) = length(a.tensors)
Base.iterate(a::At, s::Integer=1) = iterate(a.tensors, s)

Base.getindex(a::At, k::Integer) = a.tensors[k]

# this is private because it can easily move one of the tensors
function _map(𝒻, a::At{M}) where {M}
    tpl = map(𝒻, a.tensors)
    At{M,length(tpl),typeof(tpl)}(tpl)
end

Base.getindex(a::At) = _map(getindex, a)


"""
    GTensor{M,R,r,s}

Generic tensor type on manifold ``M`` with `Representation` of type `R` having `r` contravariant
indices followed by `s` covariant indices.  This type explicitly stores the point (as any `Point` type)
and data (as any `AbstractArray` type).
"""
struct GTensor{M,R<:Representation{M},r,s,P<:Point{M},D} <: Tensor{M,r,s}
    point::P 
    data::D

    function GTensor{M,R,r,s}(p::Point{M}, d::AbstractArray) where {M,R,r,s}
        new{M,R,r,s,typeof(p),typeof(d)}(p, d)
    end
end

Atlas(::Type{<:GTensor{M,R}}) where {M,R} = Atlas(R)
BasisType(::Type{<:GTensor{M,R}}) where {M,R} = BasisType(R)

pointtype(::Type{<:GTensor{M,R,r,s,P}}) where {M,R,r,s,P} = P

datatype(::Type{<:GTensor{M,R,r,s,P,D}}) where {M,R,r,s,P,D} = D

# rep implementations can override this
function defaulttype(::Type{Tensor{M,r,s}}, ::Type{R}) where {M<:Manifold,R<:Representation{M},r,s}
    GTensor{M,R,r,s}
end

const GVector{M,R,P,D} = GTensor{M,R,1,0,P,D}
const GOneForm{M,R,P,D} = GTensor{M,R,0,1,P,D}

GVector{M,R}(p::Point{M}, d::StaticVector) where {M,R} = GTensor{M,R,1,0}(p, d)
GOneForm{M,R}(p::Point{M}, d::StaticVector) where {M,R} = GTensor{M,R,0,1}(p, d)

function GVector{M,R}(p::Point{M}, data::AbstractArray1) where {M,R}
    GTensor{M,R,1,0}(p, convert(SVector{ndimensions(R),eltype(data)}, data))
end
function GOneForm{M,R}(p::Point{M}, data::AbstractArray1) where {M,R}
    GTensor{M,R,0,1}(p, convert(SMatrix{1,ndimensions(R),eltype(data)}, data))
end

function GTensor(u::Tensor{M,r,s}) where {M,r,s}
    GTensor{M,typeof(Representation(u)),r,s}(Point(u), data(u))
end
GVector(u::Vector{M}) where {M} = GVector{M,typeof(Representation(u))}(Point(u), data(u))
GOneForm(u::OneForm{M}) where {M} = GOneForm{M,typeof(Representation(u))}(Point(u), data(u))


struct GMetric{M,R<:Representation{M},P<:Point{M}} <: Metric{M}
    point::P
end

GMetric{M,R}(p::Point{M}) where {M,R} = GMetric{M,R,typeof(p)}(p)

pointtype(::Type{<:GMetric{M,R,P}}) where {M,R,P} = P

Atlas(::Type{<:GMetric{M,R}}) where {M,R} = Atlas(R)
BasisType(::Type{<:GMetric{M,R}}) where {M,R} = BasisType(R)

constructortype(::Type{<:GMetric{M,R}}) where {M,R} = GTensor{M,R,0,2}

# this is a fallback that should be overridden in most cases
function (g::GMetric{M})(u::Vector{M}, v::Vector{M}) where {M}
    rep = Representation(g)
    ud = data(u, rep)
    vd = data(v, rep)
    gd = data(g)
    ud' * gd * vd
end

# again, a fallback that should be overridden in most cases
function (g::InvMetric{M,<:GMetric{M}})(u::OneForm{M}, v::OneForm{M}) where {M}
    inv(g)(Vector(u), Vector(v))
end

"""
    tr(t::Tensor)

The _invariant_ trace of a 2-index tensor, i.e. ``g^{\\mu\\nu}t_{\\mu\\nu}``, ``g_{\\mu\\nu}t^{\\mu\\nu}``
or ``{t^\\mu}_\\mu``.
"""
function LinearAlgebra.tr(t::Tensor{M,0,2}) where {M}
    ginv = invmetricdata(Point(t), Atlas(t))
    tdat = data(t)
    tr(ginv*tdat)
end

function LinearAlgebra.tr(t::Tensor{M,2,0}) where {M}
    g = metricdata(Point(t), Atlas(t))
    tdat = data(t)
    tr(g*tdat)
end

LinearAlgebra.tr(t::Tensor{M,1,1}) where {M} = tr(data(t))


# the rep doesn't really matter here, but we need it for impl consistency
struct ZeroTensor{M,R,r,s,P} <: Tensor{M,r,s}
    point::P
end

Atlas(::Type{<:ZeroTensor{M,R}}) where {M,R} = Atlas(R)
BasisType(::Type{<:ZeroTensor{M,R}}) where {M,R} = BasisType(R)

pointtype(::Type{<:ZeroTensor{M,R,r,s,P}}) where {M,R,r,s,P} = P

#TODO: will need generated funcs for other data methods of this


const ZeroVector{M,R,P} = ZeroTensor{M,R,1,0,P}
const ZeroOneForm{M,R,P} = ZeroTensor{M,R,0,1,P}

ZeroVector{M,R}(x::Point{M}) where {M,R} = ZeroVector{M,R,typeof(x)}(x)
ZeroOneForm{M,R}(x::Point{M}) where {M,R} = ZeroOneForm{M,R,typeof(x)}(x)

function data(v::ZeroVector{M}) where {M}
    n = ndimensions(M)
    @SVector(zeros(dataeltype(v), n))
end

# these tensors are unique in that this is possible to define generally
data(v::ZeroVector{M}, rep::Representation{M}) where {M} = data(v)

function data(u::ZeroOneForm{M}) where {M}
    n = ndimensions(M)
    @SMatrix(zeros(dataeltype(u), 1, n))
end

data(u::ZeroOneForm{M}, rep::Representation{M}) where {M} = data(u)

function Base.zero(u::Tensor{M,r,s}) where {M,r,s}
    x = Point(u)
    ZeroTensor{M,typeof(Representation(u)),r,s,typeof(x)}(x)
end

Base.:(*)(a::Number, z::ZeroTensor) = z
Base.:(/)(z::ZeroTensor, a::Number) = z

Base.:(-)(z::ZeroTensor) = z

add(::Representation{M}, u::Tensor{M,r,s}, z::ZeroTensor{M,R,r,s}) where {M,R,r,s} = u
add(::Representation{M}, z::ZeroTensor{M,R,r,s}, u::Tensor{M,r,s}) where {M,R,r,s} = u

mult(::Representation{M}, u::OneForm{M}, z::ZeroVector{M}) where {M} = zero(dataeltype(u))
mult(::Representation{M}, z::ZeroVector{M}, u::OneForm{M}) where {M} = zero(dataeltype(z))
mult(::Representation{M}, z::ZeroOneForm{M}, u::Vector{M}) where {M} = zero(dataeltype(z))
mult(::Representation{M}, u::Vector{M}, z::ZeroOneForm{M}) where {M} = zero(dataeltype(u))

inner(::Representation{M}, u::Vector{M}, z::ZeroVector{M}) where {M} = zero(dataeltype(u))
inner(::Representation{M}, z::ZeroVector{M}, u::Vector{M}) where {M} = zero(dataeltype(z))

inner(::Representation{M}, u::OneForm{M}, z::ZeroOneForm{M}) where {M} = zero(dataeltype(u))
inner(::Representation{M}, z::ZeroOneForm{M}, u::OneForm{M}) where {M} = zero(dataeltype(z))

# resolve ambiguities
for bt ∈ (:Orthonormal, :Holonomic)
    @eval function inner(::Representation{M,A,$bt}, z::ZeroVector{M}, ::ZeroVector{M}) where {M,A<:Atlas{M}}
        zero(dataeltype(z))
    end

    @eval function inner(::Representation{M,A,$bt}, z::ZeroOneForm{M}, ::ZeroOneForm{M}) where {M,A<:Atlas{M}}
        zero(dataeltype(z))
    end
end


# type signature is like this because basistype is important for dispatch on these
"""
    BasisVector{k,M,BT,A}

The ``k``th basis vector of the basis type `BT` according to atlas of type `A`.  For holonomic bases
these are defined as the partial derivatives in the direction of the corresponding coordinate.

Note that, despite loose language, lowering the index is *not* the same thing as a dual in
non-orthonormal bases.  Recall that `OneForm` returns the *lowered index* version of this.
To get the dual use `dual`.
"""
struct BasisVector{k,M,BT<:BasisType,A<:Atlas{M},P<:Point{M}} <: Vector{M}
    point::P
end

function BasisVector{k}(x::Point{M},
                        rep::Representation{M,A,BT}=Representation(Atlas(x), Holonomic())) where {k,M,A,BT}
    BasisVector{k,M,BT,A,typeof(x)}(x)
end

Atlas(::Type{<:BasisVector{k,M,BT,A}}) where {k,M,BT,A} = A()
BasisType(::Type{<:BasisVector{k,M,BT}}) where {k,M,BT} = BT()

pointtype(::Type{<:BasisVector{k,M,BT,A,P}}) where {k,M,BT,A,P} = P

dataeltype(::Type{<:BasisVector{k,M,BT,A,P}}) where {k,M,BT,A,P} = dataeltype(P)
dataeltype(e::BasisVector) = dataeltype(typeof(e))

function data(e::BasisVector{k,M}) where {k,M}
    n = ndimensions(M)
    T = dataeltype(e)
    u = zero(SVector{n,T})
    setindex(u, one(T), n)
end

dual(e::BasisVector{k,M,BT,A}) where {k,M,BT,A} = BasisOneForm{k,M,BT,A}(Point(e))

@inline _basis_oneform(::Holonomic, e::BasisVector) = OneForm(GVector(e))
@inline _basis_oneform(::Orthonormal, e::BasisVector{k}) where {k} = dual(e)

OneForm(e::BasisVector) = _basis_oneform(BasisType(e), e)


"""
    BasisOneForm{k,M,BT,A}

The ``k``th basis 1-form of the basis type `BT` according to atlas of type `A`.  For holonomic bases
these are defined as the exterior derivatives of the coordinate functions.

Recall that calling `Vector` on this raises the index, which for non-orthonormal bases is *not*
the same thing as `dual`, see also [`BasisVector`](@ref)
"""
struct BasisOneForm{k,M,BT<:BasisType,A<:Atlas{M},P<:Point{M}} <: OneForm{M}
    point::P
end

function BasisOneForm{k}(x::Point{M},
                         rep::Representation{M,A,BT}=Representation(Atlas(x), Holonomic())) where {k,M,A,BT}
    BasisOneForm{k,M,BT,A,typeof(x)}(x)
end

Atlas(::Type{<:BasisOneForm{k,M,BT,A}}) where {k,M,BT,A} = A()
BasisType(::Type{<:BasisOneForm{k,M,BT}}) where {k,M,BT} = BT()

dataeltype(::Type{<:BasisOneForm{k,M,BT,A,P}}) where {k,M,BT,A,P} = dataeltype(P)
dataeltype(e::BasisOneForm) = dataeltype(typeof(e))

function data(e::BasisOneForm{k,M}) where {k,M}
    n = ndimensions(M)
    T = dataeltype(e)
    u = zero(SVector{n,T})
    setindex(u, one(T), n)
end

dual(e::BasisOneForm{k,M,BT,A}) where {k,M,BT,A} = BasisVector{k,M,BT,A}(Point(e))

_basis_vector(::Holonomic, e::BasisOneForm) = Vector(GOneForm(e))
_basis_vector(::Orthonormal, e::BasisOneForm{k}) where {k} = BasisVector{k}(Point(e), Representation(e))

Vector(e::BasisOneForm) = _basis_vector(BasisType(e), e)

function ∂metricdata(diffbackend::AbstractADType, atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1) where {M}
    ∂g = DI.jacobian(ξdat -> parent(metricdata(atl, ξdat, pidx)), diffbackend, xdat)
    n = ndimensions(M)
    reshape(∂g, Size(n, n, n))
end

"""
    ∂metricdata(atl::Atlas=Atlas(x), x::Point)

Compute the components of ``\\partial_{\\mu}g_{\\mu\\sigma}`` in coordinate atlas `atl` at the
point `x`.

**WARNING** Contrary to convention, the index of the ``\\partial`` is the *last*, rather
than the first index here.  This is to obviate the need to permute indices after
automatic differentiation.
"""
function ∂metricdata(atl::Atlas, xdat::AbstractArray1, pidx::Integer=1; diffbackend::AbstractADType=defaultdiffbackend())
    ∂metricdata(diffbackend, atl, xdat, pidx)
end

function ∂metricdata(x::Point{M}, atl::Atlas{M}=Atlas(x); diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    ∂metricdata(atl, data(x, atl), patchindex(x, atl); diffbackend)
end


# tullio weirdness doesn't happen here because there are no products
@inline function _inner_christoffel1data!(Γ, ∂g)
    # note this looks weird because of index ordering of ∂g
    @tullio Γ[σ,μ,ν] = (∂g[ν,σ,μ] + ∂g[μ,σ,ν] - ∂g[μ,ν,σ])/2 threads=false
end

@inline function _inner_christoffel1data(::Val{n}, ∂g) where {n}
    Γ = similar(∂g, Size(n, n, n))
    _inner_christoffel1data!(Γ, ∂g)
    SArray(Γ)
end

@inline function christoffel1data(atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                                  diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    ∂g = ∂metricdata(atl, xdat, pidx; diffbackend)
    SArray(_inner_christoffel1data(Val(ndimensions(M)), ∂g))
end


"""
    christoffel1data(x::Point, atl::Atlas=Atlas(x))

Returns an ``n \\times n \\times n`` array containing the corresponding of the elements of the
Christoffel symbols of the first kind (i.e. no raised index) in atlas `atl` at point `x` on
manfiold ``M``.
"""
@inline function christoffel1data(x::Point{M}, atl::Atlas{M}=Atlas(x);
                                  diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    christoffel1data(atl, data(x, atl), patchindex(x, atl); diffbackend)
end


@inline function _inner_christoffel2data(g, Γ)
    o = similar(Γ)
    @tullio o[σ,μ,ν] = g[σ,τ] * Γ[τ,μ,ν] threads=false
    SArray(o)
end

@inline function christoffel2data(atl::Atlas, xdat::AbstractArray1, pidx::Integer=1;
                                  diffbackend::AbstractADType=defaultdiffbackend())
    Γ = christoffel1data(atl, xdat, pidx; diffbackend)
    g = invmetricdata(atl, xdat, pidx)
    _inner_christoffel2data(g, Γ)
end


"""
    christoffel2data(x::Point, atl::Atlas=Atlas(x))

Returns an ``n \\times n \\times n`` array containing the corresponding elements of the Christoffel
symbols of the second kind (i.e. ``{\\Gamma^\\sigma}_{\\mu\\nu}``) in atlas `atl` at point `x`
on manifold ``M``.

Note that because we have to explicitly raise the index this calls [`christoffel1data`](@ref) and
is therefore less efficient.
"""
function christoffel2data(x::Point{M}, atl::Atlas{M}=Atlas(x);
                          diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    christoffel2data(atl, data(x, atl), patchindex(x, atl); diffbackend)
end


#TODO: this is still allocating but we are not taking advantage of symmetry properties and the
#tensor is huge, don't think it's a big deal

@inline function _∂christoffel2_jacobian_eval(atl::Atlas, xdat, pidx::Integer;
                                              diffbackend::AbstractADType=defaultdiffbackend(),
                                              inner_diffbackend::AbstractADType=defaultdiffbackend(),
                                             )
    DI.jacobian(ξ -> christoffel2data(atl, ξ, pidx; diffbackend=inner_diffbackend), diffbackend, xdat)
end

@inline function ∂christoffel2data(atl::Atlas{M}, xdat::AbstractArray1, pidx::Integer=1;
                                   diffbackend::AbstractADType=defaultdiffbackend(),
                                   inner_diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    n = ndimensions(M)
    o = _∂christoffel2_jacobian_eval(atl, xdat, pidx; diffbackend, inner_diffbackend)
    SArray(reshape(o, Size(n, n, n, n)))
end

function ∂christoffel2data(x::Point{M}, atl::Atlas{M}=Atlas(x);
                           diffbackend::AbstractADType=defaultdiffbackend(),
                           inner_diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    ∂christoffel2data(Val(n), atl, data(x, atl), patchindex(x, atl); diffbackend, inner_diffbackend)
end

# tullio forces us to do this this way
@inline function _inner_riemann!(R, ∂Γ, Γ)
    @tullio R[ρ,σ,μ,ν] = ∂Γ[ρ,ν,σ,μ] - ∂Γ[ρ,μ,σ,ν] threads=false
    @tullio R[ρ,σ,μ,ν] += Γ[ρ,μ,τ]*Γ[τ,ν,σ] threads=false
    @tullio R[ρ,σ,μ,ν] += -Γ[ρ,ν,τ]*Γ[τ,μ,σ] threads=false
    R
end

@inline function _inner_riemann(∂Γ, Γ)
    R = similar(∂Γ)
    _inner_riemann!(R, ∂Γ, Γ)
    SArray(R)
end

"""
    riemanndata(x::Point, atl::Atlas=Atlas(x))

Returns the componenent data of the Riemann tensor ``{R^{\\mu}}_{\\nu\\tau\\sigma}`` at point `x` in
coordinate atlas `atl`.  See [`riemann`](@ref) for a function that returns the Riemann tensor as
a `Tensor`.  `Atlas` implementations may choose to overload this function for efficiency, e.g.
for Minkowski, dS or AdS.

**WARNING** while we aim for this function to be as efficient as possible, it uses naive automatic
differentiation and cannot use symmetry properties of the Riemann tensor to elide any computation.
It is therefore probably very inefficient compared to what would be possible with direct symbolic
computation of only the non-zero components.  On the other hand, it can potentially accommodate
highly non-trivial implementations of the metric, i.e. where an analytic form of the metric cannot
be obtained.
"""
function riemanndata(x::Point{M}, atl::Atlas{M}=Atlas(x);
                     diffbackend::AbstractADType=defaultdiffbackend(),
                     inner_diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    n = ndimensions(M)
    xdat = data(x, atl)
    pidx = patchindex(x, atl)
    ∂Γ = ∂christoffel2data(atl, xdat, pidx; diffbackend, inner_diffbackend)
    Γ = christoffel2data(atl, xdat, pidx)
    _inner_riemann(∂Γ, Γ)
end

@inline function _inner_riemanndown(g, R)
    Rd = similar(R)
    @tullio Rd[ρ,σ,μ,ν] = g[ρ,τ]*R[τ,σ,μ,ν] threads=false
    SArray(Rd)
end

function riemanndowndata(x::Point{M}, atl::Atlas{M}=Atlas(x);
                         diffbackend::AbstractADType=defaultdiffbackend(),
                         inner_diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    R = riemanndata(x, atl; diffbackend, inner_diffbackend)
    g = metricdata(atl, data(x, atl), patchindex(x, atl))
    _inner_riemanndown(g, R)
end

@inline function _inner_ricci(::Val{n}, R) where {n}
    Ri = similar(R, Size(n, n))
    @tullio Ri[μ,ν] = R[ρ,μ,ρ,ν] threads=false
    SMatrix(Ri)
end

function riccidata(x::Point{M}, atl::Atlas{M}=Atlas(x);
                   diffbackend::AbstractADType=defaultdiffbackend(),
                   inner_diffbackend::AbstractADType=defaultdiffbackend()) where {M}
    R = riemanndata(x, atl; diffbackend, inner_diffbackend)
    _inner_ricci(Val(ndimensions(M)), R)
end

"""
    riemann(x::Point, rep::Representation=Representation(Atlas(x), Holonomic()))

Returns the Riemann tensor ``{R^{\\mu}}_{\\nu\\tau\\sigma}`` at point `x` in representation `rep`.
"""
function riemann(x::Point{M},
        rep::Representation{M,A,<:Holonomic}=Representation(Atlas(x), Holonomic());
        diffbackend::AbstractADType=defaultdiffbackend(),
        inner_diffbackend::AbstractADType=defaultdiffbackend(),
    ) where {M,A<:Atlas{M}}
    R = riemanndata(x, Atlas(rep); diffbackend, inner_diffbackend)
    GTensor{M,typeof(rep),1,3}(x, R)
end

"""
    riemanndown(x::Point, rep::Representation=Representation(Atlas(x), Holonomic()))

Returns the fully covariant Riemann tensor ``R_{\\mu\\nu\\tau\\sigma}`` at point `x` in representation
`rep`.  Computing this tensor requires an extra metric contraction compared to the ``(1,3)`` Riemann
tensor (see [`riemann`](@ref)) and therefore `riemann` is preferred for performance reasons.
This method allows for particularly easy checking of symmetry properties of the Riemann tensor
(i.e. for debugging that it is computed correctly).
"""
function riemanndown(x::Point{M},
                     rep::Representation{M,A,<:Holonomic}=Representation(Atlas(x), Holonomic());
                     diffbackend::AbstractADType=defaultdiffbackend(),
                     inner_diffbackend::AbstractADType=defaultdiffbackend(),
                    ) where {M,A<:Atlas{M}}
    R = riemanndowndata(x, Atlas(rep); diffbackend, inner_diffbackend)
    GTensor{M,typeof(rep),0,4}(x, R)
end

"""
    ricci(x::Point, rep::Representation=Representation(Atlas(x), Holonomic()))

Compute the Ricci tensor at the point `x`.  Note that this requires computation of the Riemann tensor,
see [`riemann`](@ref).

The Ricci scalar can be computed simply from the trace of this tensor, i.e. `tr(ricci(x))`.  This is
the most efficient available method for computing the Ricci scalar.
"""
function ricci(x::Point{M},
               rep::Representation{M,A,<:Holonomic}=Representation(Atlas(x), Holonomic());
               diffbackend::AbstractADType=defaultdiffbackend(),
               inner_diffbackend::AbstractADType=defaultdiffbackend(),
              ) where {M,A<:Atlas{M}}
    R = riccidata(x, Atlas(rep); diffbackend, inner_diffbackend)
    GTensor{M,typeof(rep),0,2}(x, R)
end

"""
    einstein(x::Point, rep::Representation=Representation(Atlas(x), Holonomic()))

Compute the Einstein tensor at the point `x`.  Note that this requires computation of the Riemann tensor,
see [`riemann`](@ref).
"""
function einstein(x::Point{M},
                  rep::Representation{M,A,<:Holonomic}=Representation(Atlas(x), Holonomic());
                  diffbackend::AbstractADType=defaultdiffbackend(),
                  inner_diffbackend::AbstractADType=defaultdiffbackend(), 
                 ) where {M,A<:Atlas{M}}
    R = ricci(x, rep; diffbackend, inner_diffbackend)
    g = Metric(rep, x)
    R - tr(R)*g/2
end
