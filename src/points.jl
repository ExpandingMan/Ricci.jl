
"""
    Point{M<:Manifold}

Abstract types for points on the manifold ``M``.  All implementations contain coordinate data.
Therefore, in some contexts `Point` can be thought of as "point with coordinate system".
For convenience, some methods might depend on the coordinates in which `p` is specified.  For
example, the function `normal(timelike, p)` returns a timelike normal vector proportional to
the timelike coordinate vector at `p`.  This should be used with care and should be considered
more of a convenient shortcut for `normal(timelike, p)` than a property of
`p`.
"""
abstract type Point{M<:Manifold} end

Atlas(p::Point) = Atlas(typeof(p))

Manifold(::Type{<:Point{M}}) where {M} = M()
Manifold(p::Point) = Manifold(typeof(p))

Point(atl::Atlas, x::AbstractArray1, pidx::Integer=1) = defaulttype(Point, atl)(x, pidx)

"""
    data(p::Point)

Returns the coordinate data for the point.  If no atlas argument is provided, this is guaranteed
to be in the coordinate system given by `Atlas(p)`.
"""
data(p::Point) = p.data

"""
    patchindex(p::Point, atl::Atlas=Atlas(p))

Returns the index of the coordinate patch of the point in atlas `atl`.
"""
patchindex(p::Point) = 1

datapatchindex(p::Point) = (data(p), patchindex(p))

_datapatchindex(::A, p::Point{M}, ::A) where {M,A<:Atlas{M}} = datapatchindex(p)

function _datapatchindex(atl::Atlas{M}, p::Point{M}, atl′::Atlas{M}) where {M}
    coordtransform_ondata(atl′, atl, data(p), patchindex(p))
end

"""
    datapatchindex(p::Point, atl::Atlas=Atlas(p))

Returns a tuple the first element of which is the coordinate data of the point in atlas `atl` and
the second index of which is the patch index of the point in `atl`.  In some cases this may be
more efficient than calling [`data`](@ref) and [`patchindex`](@ref) separately.
"""
datapatchindex(p::Point, atl::Atlas) = _datapatchindex(Atlas(p), p, atl)

data(p::Point, atl::Atlas) = datapatchindex(p, atl)[1]

patchindex(p::Point, atl::Atlas) = datapatchindex(p, atl)[2]

datatype(p::Point) = typeof(data(p))

# manifold implementations should define this
defaulttype(::Type{Point}, atl::Atlas) = defaulttype(Point, typeof(atl))


_atlas(::A, ::A, p::Point, ::Type{P}) where {M,A<:Atlas{M},P<:Point{M}} = P(p)
function _atlas(atl′::Atlas{M}, atl::Atlas{M}, p::Point{M}, ::Type{P}) where {M,P<:Point{M}}
    P(coordtransform_ondata(atl′, atl, data(p), patchindex(p))...)
end

(atl::Atlas{M})(p::Point{M}, ::Type{P}=defaulttype(Point, atl)) where {M,P<:Point{M}} = _atlas(atl, Atlas(p), p, P)

"""
    constructortype(p::Point)

Returns a type that can be used to construct a point.  Such a type *must* have a method which
takes as an argument only the coordinate data used by `p`.  Defaults to `typeof(p)`.
"""
constructortype(::Type{P}) where {P<:Point} = P
constructortype(p::Point) = constructortype(typeof(p))

# note that this fallback is only for identical types, otherwise you need to define
function Base.:(==)(p::P, q::P) where {P<:Point}
    # strict equality doesn't try to reconcile different patches, since floating point
    # will probably not match anyway
    patchindex(p) == patchindex(q) || return false
    data(q)
end
function Base.:(≈)(p::P, q::P; kw...) where {P<:Point} 
    (pidx, qidx) = (patchindex(p), patchindex(q))
    pidx == qidx || (p = topatch(p, qidx))
    (≈)(data(p), data(q); kw...)
end


function constructortype end

function patchindex end

function topatch end

"""
    normal(timelike, p::Point)

Returns the timelike coordinate vector, in the coordinate representation of ``p`` at ``p``.
"""
function normal end

function squaredistance end

function distance end


struct Coord1Point{M,A<:Atlas{M},D<:StaticVector} <: Point{M}
    data::D

    Coord1Point{M,A}(v::StaticVector, ::Integer=1) where {M,A} = new{M,A,typeof(v)}(v)
end

Atlas(::Type{<:Coord1Point{M,A}}) where {M,A} = A()

datatype(::Type{<:Coord1Point{M,A,D}}) where {M,A,D} = D

constructortype(::Type{<:Coord1Point{M,A}}) where {M,A} = Coord1Point{M,A}

# first arg is npatches
_defaulttype(::Val{1}, ::Type{Point}, ::Type{A}) where {A<:Atlas} = Coord1Point{Manifold(A),A}

function Coord1Point{M,A}(v::AbstractArray1{<:Number}, ::Integer=1) where {M,A}
    Coord1Point{M,A}(convert(SVector{ndimensions(A),eltype(v)}, v))
end


struct CoordPoint{M,A<:Atlas{M},D<:StaticVector} <: Point{M}
    data::D
    patch::Int

    CoordPoint{M,A}(v::StaticVector, idx::Integer=1) where {M,A} = new{M,A,typeof(v)}(v, idx)
end

Atlas(::Type{<:CoordPoint{M,A}}) where {M,A} = A()

datatype(::Type{<:CoordPoint{M,A,D}}) where {M,A,D} = D

constructortype(::Type{<:CoordPoint{M,A}}) where {M,A} = CoordPoint{M,A}

_defaulttype(::Val{np}, ::Type{Point}, ::Type{A}) where {np,A<:Atlas} = CoordPoint{Manifold(A),A}

function CoordPoint{M,A}(v::AbstractArray1{<:Number}, idx::Integer=1) where {M,A}
    CoordPoint{M,A}(convert(SVector{ndimensions(A),eltype(v)}, v), idx)
end

# atlas implementations can override this
defaulttype(::Type{Point}, ::Type{A}) where {A<:Atlas} = _defaulttype(npatches(A), Point, A)


# 1 patch sphere, useful in a lot of places
struct SpherePolarAtlas1{n} <: Atlas{Sphere{n}} end

const SP1Point{n} = Coord1Point{Sphere{n},SpherePolarAtlas1{n}}

distance(p::SP1Point{2}, q::SP1Point{2}) = let (θ₁, ϕ₁) = data(p), (θ₂, ϕ₂) = data(q)
    Δϕ = abs(ϕ₂ - ϕ₁)
    Δθ = abs(θ₂ - θ₁)
    ahav(hav(Δϕ) + hav(Δθ)*(1 - hav(Δϕ) - hav(ϕ₁ + ϕ₂)))
end

squaredistance(p::SP1Point{n}, q::SP1Point{n}) where {n} = distance(p, q)^2
