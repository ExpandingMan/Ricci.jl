
#WARN: a lot of this will probably have to generalized to the case where we have more than one
#patch, or really do any other kind of coordinate checking on the trajectory

"""
    SymplIntegratorSolution

A data structure satisfying the `SymplecticIntegrator` interface for cases where the solution is
explicitly known.  Refers to a single function which gives the solution.
"""
struct SymplIntegratorSolution{F} <: SymplecticIntegrator
    f::F
end

integrate_ondata(int::SymplIntegratorSolution, q0, p0, s::Number) = int.f(q0, p0, s)


"""
    SymplIntegrator <: SymplecticIntegrator

Generic symplectic integrator wrapper allowing a user to provide their own integration function and
integrator object.
"""
struct SymplIntegrator{T<:Number,F,I} <: SymplecticIntegrator
    integrate::F
    integrator::I
    h::T

    function SymplIntegrator(integrate, integrator, h::Real=convert(Float, 0.01))
        new{typeof(h),typeof(integrate),typeof(integrator)}(
            integrate, integrator, h,
        )
    end
end

function integrate_ondata(int::SymplIntegrator, q0, p0, s::Number)
    nsteps = Int((÷)(s, int.h, RoundNearest))
    int.integrate(int.integrator, q0, p0, int.h, nsteps)
end

# fallback default, representations can define their own
function fallbackintegrator(rep::Representation, ::Val{order}=Val(3), h::Number=convert(Float, 0.01);
        ω::Number=one(Float), params...
    ) where {order}
    int = Tao{order}(
        (x, p) -> ∂qhamiltonian_ondata(rep, x, p),
        (x, p) -> ∂phamiltonian_ondata(rep, x, p);
        ω, params...
    )
    SymplIntegrator(FastSymplecticIntegrators.integrate, int, h)
end

defaultintegrator(rep::Representation, args...; kw...) = fallbackintegrator(rep, args...; kw...)


function integrate(int::SymplecticIntegrator, p::OneForm, s::Number)
    x = Point(p)
    (xdat, pdat) = integrate_ondata(int, data(x), data(p), s)
    constructortype(p)(constructortype(x)(xdat), pdat)
end
integrate(int::SymplecticIntegrator, p::Vector, s::Number) = 𝐠inv*integrate(int, 𝐠*p, s)


"""
    CoordChangeSymplIntegrator{A} <: SymplecticIntegrator

A wrapper type for another `SymplecticIntegrator` data structure indicating that the coordinate system
should first be changed to atlas `A()` before trying to integrate.

This is of course *NOT* free, but in some cases it may still be much more efficient by directly
trying to integrate in the initial atlas.
"""
struct CoordChangeSymplIntegrator{A<:Atlas,I<:SymplecticIntegrator} <: SymplecticIntegrator
    integrator::I
end

function CoordChangeSymplIntegrator{A}(int::SymplecticIntegrator) where {A<:Atlas}
    CoordChangeSymplIntegrator{A,typeof(int)}(int)
end

function integrate(int::CoordChangeSymplIntegrator{A}, p::OneForm, s::Number) where {A<:Atlas}
    x = Point(p)
    xdat = data(x, A())
    pdat = data(p, Representation(A(), Holonomic()))
    (ydat, qdat) = integrate_ondata(int.integrator, xdat, pdat, s)
    y = defaulttype(Point, Atlas(p))(ydat)
    q = defaulttype(OneForm, Representation(p))(y, qdat)
    Atlas(p)(q)
end
