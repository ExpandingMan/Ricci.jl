module SchwarzschildImpl

using ..Ricci: @_submodule_imports, @generic_tensor_types
@_submodule_imports

using Infiltrator


# I can't be bothered pretending to do higher dims right now
struct Schwarzschild{rs} <: Lorentzian{4} end

struct Polar1Patch{rs} <: Atlas{Schwarzschild{rs}} end

@generic_tensor_types Schwarzschild{rs} Polar1Patch{rs} P1

# cartesian schild
struct Schild{rs} <: Atlas{Schwarzschild{rs}} end

@generic_tensor_types Schwarzschild{rs} Schild{rs} Sch

# 1 patch polar schild
struct SchildPolar1{rs} <: Atlas{Schwarzschild{rs}} end

@generic_tensor_types Schwarzschild{rs} SchildPolar1{rs} SchP1

# that this is cartesian is implied, polar will be called IsotropicPolar1Patch
struct Isotropic{rs} <: Atlas{Schwarzschild{rs}} end

@generic_tensor_types Schwarzschild{rs} Isotropic{rs} I

const P1Point{rs} = Coord1Point{Schwarzschild{rs},Polar1Patch{rs}}
const SchPoint{rs} = Coord1Point{Schwarzschild{rs},Schild{rs}}
const SchP1Point{rs} = Coord1Point{Schwarzschild{rs},SchildPolar1{rs}}
const IPoint{rs} = Coord1Point{Schwarzschild{rs},Isotropic{rs}}


include("tensors.jl")


end
