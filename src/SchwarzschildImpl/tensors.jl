#====================================================================================================
       \begin{polar1}
====================================================================================================#
@stable function metricdata(::Polar1Patch{rs}, x::AbstractArray1, pidx::Integer=1) where {rs}
    (r, θ) = (x[1], x[2])
    # remember we shift θ to avoid singularity
    @SMatrix eltype(x)[
        1/(1 - rs/r) 0 0 0
        0 r^2 0 0
        0 0 (r*cos(θ))^2 0
        0 0 0 -(1 - rs/r)
    ]
end

@stable function invmetricdata(::Polar1Patch{rs}, x::AbstractArray1, pidx::Integer=1) where {rs}
    (r, θ) = (x[1], x[2])
    # remember we shift θ to avoid singularity
    @SMatrix eltype(x)[
        (1 - rs/r) 0 0 0
        0 r^(-2) 0 0
        0 0 (r*cos(θ))^(-2) 0
        0 0 0 -1/(1 - rs/r)
    ]
end

# stable macro doesn't like to be used on object function calls
@stable function _inner_polar1(g, u::Vector{Schwarzschild{rs}}, v::Vector{Schwarzschild{rs}}) where {rs}
    x = data(Point(g), Polar1Patch{rs}())
    (r, θ) = (x[1], x[2])
    ud = data(u, Representation(g))
    vd = data(v, Representation(g))
    ud[1]*vd[1]/(1 - rs/r) + ud[2]*vd[2]*r^2 + ud[3]*vd[3]*(r*cos(θ))^2 - ud[4]*vd[4]*(1 - rs/r)
end

(g::P1Metric{rs})(u::Vector{Schwarzschild{rs}}, v::Vector{Schwarzschild{rs}}) where {rs} = _inner_polar1(g, u, v)

@stable function _inner_polar1(g, u::OneForm{Schwarzschild{rs}}, v::OneForm{Schwarzschild{rs}}) where {rs}
    x = data(Point(g), Polar1Patch{rs}())
    (r, θ) = (x[1], x[2])
    ud = data(u, Representation(g))
    vd = data(v, Representation(g))
    ud[1]*vd[1]*(1 - rs/r) + ud[2]*vd[2]/r^2 + ud[3]*vd[3]/(r*cos(θ))^2 - ud[4]*vd[4]/(1 - rs/r)
end

function (g::InvMetric{Schwarzschild{rs},<:P1Metric{rs}})(
        u::OneForm{Schwarzschild{rs}}, v::OneForm{Schwarzschild{rs}},
    ) where {rs}
    _inner_polar1(g, u, v)
end

@stable function OneForm(v::P1Vector{rs}) where {rs}
    x = data(Point(v), Polar1Patch{rs}())
    vd = data(v)
    dat = @SMatrix eltype(vd)[
        vd[1]/(1 - rs/x[1]) vd[2]*x[1]^2 vd[3]*(x[1]*cos(x[2]))^2 -vd[4]*(1 - rs/x[1])
    ]
    P1OneForm{rs}(Point(v), dat)
end

@stable function Vector(v::P1OneForm{rs}) where {rs}
    x = data(Point(v), Polar1Patch{rs}())
    vd = data(v)
    dat = @SVector eltype(vd)[
        vd[1]*(1 - rs/x[1]), vd[2]/x[1]^2, vd[3]/(x[1]*cos(x[2]))^2, -vd[4]/(1 - rs/x[1]),
    ]
    P1Vector{rs}(Point(v), dat)
end

@stable function ∂qhamiltonian_ondata(::P1HolRep{rs}, q, p) where {rs}
    r = q[1]
    A = 1 - rs/r
    ∂A = rs/r^2
    (s, c) = sincos(q[2])
    SVector{4,eltype(q)}(
        ((p[4]/A)^2*∂A + ∂A*p[1]^2 - 2*p[2]^2/r^3 - 2*p[3]^2/(r^3*c^2))/2,
        -s*p[3]^2/(r^2*c^3),
        zero(eltype(q)),
        zero(eltype(q)),
    )
end
#====================================================================================================
       \end{polar1}
====================================================================================================#


#====================================================================================================
       \begin{schild}
====================================================================================================#
@stable function _schildl_down(x::StaticVector{4,T}, r::Number) where {T}
    @SVector T[x[1]/r, x[2]/r, x[3]/r, 1]
end
@stable function _schildl_up(x::StaticVector{4,T}, r::Number) where {T}
    @SVector T[x[1]/r, x[2]/r, x[3]/r, -1]
end

@stable function metricdata(::Schild{rs}, x::AbstractArray1, pidx::Integer=1) where {rs}
    r = √(x[1]^2 + x[2]^2 + x[3]^2)
    A = rs/r
    l = _schildl_down(x, r)
    g11 = 1 + A*l[1]^2
    g12 = A*l[1]*l[2]
    g13 = A*l[1]*l[3]
    g14 = A*l[1]*l[4]
    g22 = 1 + A*l[2]^2
    g23 = A*l[2]*l[3]
    g24 = A*l[2]*l[4]
    g33 = 1 + A*l[3]^2
    g34 = A*l[3]*l[4]
    g44 = -1 + A
    # we fill in all components because no good reason not to
    @SMatrix eltype(x)[
        g11 g12 g13 g14
        g12 g22 g23 g24
        g13 g23 g33 g34
        g14 g24 g34 g44
    ]
end

@stable function invmetricdata(::Schild{rs}, x::AbstractArray1, pidx::Integer=1) where {rs}
    r = √(x[1]^2 + x[2]^2 + x[3]^2)
    A = rs/r
    l = _schildl_up(x, r)
    g11 = 1 - A*l[1]^2
    g12 = -A*l[1]*l[2]
    g13 = -A*l[1]*l[3]
    g14 = -A*l[1]*l[4]
    g22 = 1 - A*l[2]^2
    g23 = -A*l[2]*l[3]
    g24 = -A*l[2]*l[4]
    g33 = 1 - A*l[3]^2
    g34 = -A*l[3]*l[4]
    g44 = -1 - A
    # we fill in all components because no good reason not to
    @SMatrix eltype(x)[
        g11 g12 g13 g14
        g12 g22 g23 g24
        g13 g23 g33 g34
        g14 g24 g34 g44
    ]
end

@stable function _inner_schild(g, u::Vector{Schwarzschild{rs}}, v::Vector{Schwarzschild{rs}}) where {rs}
    x = data(Point(g), Schild{rs}())
    u = data(u, Representation(g))
    v = data(v, Representation(g))
    r2 = x[1]^2 + x[2]^2 + x[3]^2
    r = √(r2)
    A = rs/r
    l = _schildl_down(x, r)    
    (u⋅negatelast(v)) + A*(l⋅u)*(l⋅v)
end

(g::SchMetric{rs,T})(u::Vector{Schwarzschild{rs}}, v::Vector{Schwarzschild{rs}}) where {rs,T} = _inner_schild(g, u, v)

@stable function _inner_schild(g, u::OneForm{Schwarzschild{rs}}, v::OneForm{Schwarzschild{rs}}) where {rs}
    x = data(Point(g), Schild{rs}())
    u = data(u, Representation(g))
    v = data(v, Representation(g))
    r2 = x[1]^2 + x[2]^2 + x[3]^2
    r = √(r2)
    A = rs/r
    l = _schildl_up(x, r)
    (u⋅negatelast(v)) - A*(l⋅u)*(l⋅v)
end

function (g::InvMetric{Schwarzschild{rs},<:SchMetric{rs}})(
        u::OneForm{Schwarzschild{rs}}, v::OneForm{Schwarzschild{rs}},
    ) where {rs}
    _inner_schild(g, u, v)
end

@stable function OneForm(v::SchVector{rs}) where {rs}
    p = Point(v)
    x = data(p, Schild{rs}())
    r2 = x[1]^2 + x[2]^2 + x[3]^2
    r = √(r2)
    l = _schildl_down(x, r)
    v = data(v)
    A = rs/r
    o = negatelast(v) + A*(l⋅v)*l
    SchOneForm(p, o)
end

@stable function Vector(u::SchOneForm{rs}) where {rs}
    p = Point(u)
    x = data(p, Schild{rs}())
    r2 = x[1]^2 + x[2]^2 + x[3]^2
    r = √(r2)
    l = _schildl_up(x, r)
    u = data(u)
    A = rs/r
    o = negatelast(u) - A*(l⋅u)*l
    SchVector(p, o)
end
#====================================================================================================
       \end{schild}
====================================================================================================#

#====================================================================================================
       \begin{schild polar}
====================================================================================================#
@inline function _schild_polar_components_down(rs, x)
    (r, θ) = (x[1], x[2])
    A = rs/r
    g11 = 1 + A
    g14 = A
    g22 = r^2
    g33 = (r*cos(θ))^2
    g44 = -1 + A
    (g11, g14, g22, g33, g44)
end

@stable function metricdata(::SchildPolar1{rs}, x::AbstractArray1, pidx::Integer=1) where {rs}
    (g11, g14, g22, g33, g44) = _schild_polar_components_down(rs, x)
    @SMatrix eltype(x)[
        g11   0   0 g14
        0   g22   0 0
        0     0 g33 0
        g14   0   0 g44
    ]
end

@inline function _schild_polar_components_up(rs, x)
    (r, θ) = (x[1], x[2])
    A = rs/r
    g11 = 1 - A
    g14 = A
    g22 = r^(-2)
    g33 = (r*cos(θ))^(-2)
    g44 = -1 - A
    (g11, g14, g22, g33, g44)
end

@stable function invmetricdata(::SchildPolar1{rs}, x::AbstractArray1, pidx::Integer=1) where {rs}
    (g11, g14, g22, g33, g44) = _schild_polar_components_up(rs, x)
    @SMatrix eltype(x)[
        g11   0   0 g14
        0   g22   0 0
        0     0 g33 0
        g14   0   0 g44
    ]
end

@stable function _inner_schild_polar(g, u::Vector{Schwarzschild{rs}}, v::Vector{Schwarzschild{rs}}) where {rs}
    x = data(Point(g), SchildPolar1{rs}())
    ud = data(u, Representation(g))
    vd = data(v, Representation(g))
    (g11, g14, g22, g33, g44) = _schild_polar_components_down(rs, x)
    g11*ud[1]*vd[1] + g22*ud[2]*vd[2] + g33*ud[3]*vd[3] + g44*ud[4]*vd[4] + g14*(ud[1]*vd[4] + vd[1]*ud[4])
end

function (g::SchP1Metric{rs,T})(u::Vector{Schwarzschild{rs}}, v::Vector{Schwarzschild{rs}}) where {rs,T}
    _inner_schild_polar(g, u, v)
end

@stable function _inner_schild_polar(g, u::OneForm{Schwarzschild{rs}}, v::OneForm{Schwarzschild{rs}}) where {rs}
    x = data(Point(g), SchildPolar1{rs}())
    ud = data(u, Representation(g))
    vd = data(v, Representation(g))
    (g11, g14, g22, g33, g44) = _schild_polar_components_up(rs, x)
    g11*ud[1]*vd[1] + g22*ud[2]*vd[2] + g33*ud[3]*vd[3] + g44*ud[4]*vd[4] + g14*(ud[1]*vd[4] + vd[1]*ud[4])
end

function (g::InvMetric{Schwarzschild{rs},<:SchP1Metric{rs,T}})(
        u::OneForm{Schwarzschild{rs}}, v::OneForm{Schwarzschild{rs}},
    ) where {rs,T}
    _inner_schild_polar(g, u, v)
end

@stable function OneForm(v::SchP1Vector{rs}) where {rs}
    x = data(Point(v), SchildPolar1{rs}())
    vd = data(v)
    (g11, g14, g22, g33, g44) = _schild_polar_components_down(rs, x)
    dat = @SMatrix eltype(vd)[
        g11*vd[1] + g14*vd[4];;
        g22*vd[2];;
        g33*vd[3];;
        g14*vd[1] + g44*vd[4];;
    ]
    SchP1OneForm{rs}(Point(v), dat)
end

@stable function Vector(v::SchP1OneForm{rs}) where {rs}
    x = data(Point(v), SchildPolar1{rs}())
    vd = data(v)
    (g11, g14, g22, g33, g44) = _schild_polar_components_up(rs, x)
    dat = @SVector eltype(vd)[
        g11*vd[1] + g14*vd[4],
        g22*vd[2],
        g33*vd[3],
        g14*vd[1] + g44*vd[4],
    ]
    SchP1Vector{rs}(Point(v), dat)
end
#====================================================================================================
       \end{schild polar}
====================================================================================================#

#====================================================================================================
       \begin{isotropic}
====================================================================================================#

# radius for cartesian isotropic, should work for vectors and co-vectors
@inline _isoradius(x::AbstractArray) = √(x[1]^2 + x[2]^2 + x[3]^2)

@stable function metricdata(::Isotropic{rs}, x::AbstractArray1, pidx::Integer=1) where {rs}
    r = _isoradius(x)
    f1 = 1 + rs/r
    f14 = f1^4
    f2 = 1 - rs/r
    @SMatrix eltype(x)[
        f14 0 0 0
        0 f14 0 0
        0 0 f14 0
        0 0 0 -(f2/f1)^2
    ]
end

@stable function invmetricdata(::Isotropic{rs}, x::AbstractArray1, pidx::Integer=1) where {rs}
    r = _isoradius(x)
    f1 = 1 + rs/r
    f14 = f1^(-4)
    f2 = 1 - rs/4
    @SMatrix eltype(x)[
        f14 0 0 0
        0 f14 0 0
        0 0 f14 0
        0 0 0 -(f1/f2)^2
    ]
end

@stable function _inner_isotropic(g, u::Vector{Schwarzschild{rs}}, v::Vector{Schwarzschild{rs}}) where {rs}
    x = data(Point(g), Isotropic{rs}())
    r = _isoradius(x)
    f1 = 1 + rs/r
    f14 = f1^4
    f2 = 1 - rs/r
    ud = data(u, Representation(g))
    vd = data(v, Representation(g))
    f14*(ud[1]*vd[1] + ud[2]*vd[2] + ud[3]*vd[3]) - (f2/f1)^2*ud[4]*vd[4]
end

(g::IMetric{rs})(u::Vector{Schwarzschild{rs}}, v::Vector{Schwarzschild{rs}}) where {rs} = _inner_isotropic(g, u, v)

@stable function _inner_isotropic(g, u::OneForm{Schwarzschild{rs}}, v::OneForm{Schwarzschild{rs}}) where {rs}
    x = data(Point(g), Isotropic{rs}())
    r = _isoradius(x)
    f1 = 1 + rs/r
    f14 = f1^(-4)
    f2 = 1 - rs/r
    ud = data(u, Representation(g))
    vd = data(v, Representation(g))
    f14*(ud[1]*vd[1] + ud[2]*vd[2] + ud[3]*vd[3]) - (f1/f2)^2*ud[4]*vd[4]
end

function (g::InvMetric{Schwarzschild{rs},<:Isotropic{rs}})(
        u::OneForm{Schwarzschild{rs}}, v::OneForm{Schwarzschild{rs}},
    ) where {rs}
    _inner_isotropic(g, u, v)
end

@stable function OneForm(v::IVector{rs}) where {rs}
    x = data(Point(v), Isotropic{rs}())
    r = _isoradius(x)
    f1 = 1 + rs/r
    f14 = f1^4
    f2 = 1 - rs/r
    vd = data(v)
    dat = @SMatrix eltype(vd)[
        f14*vd[1] f14*vd[2] f14*vd[3] -(f2/f1)^2*vd[4]        
    ]
    IOneForm{rs}(Point(v), dat)
end

@stable function Vector(u::IOneForm{rs}) where {rs}
    x = data(Point(u), Isotropic{rs}())
    r = _isoradius(x)
    f1 = 1 + rs/r
    f14 = f1^(-4)
    f2 = 1 - rs/r
    ud = data(u)
    dat = @SVector eltype(ud)[
        f14*ud[1], f14*ud[2], f14*ud[3], -(f1/f2)^2*ud[4],
    ]
    IVector{rs}(Point(u), dat)
end
#====================================================================================================
       \end{isotropic}
====================================================================================================#
