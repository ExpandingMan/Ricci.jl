
"""
    @generic_tensor_types manifold atlas prefix

A helper macro for defining generic types for an atlas implementation with a convenient abbreviated prefix.
The manifold and the atlas must take the same parameters for this to work.  The atlas and manifold are
not defined by this macro and should already be defined.  For example
```julia
@generic_tensor_types deSitter{a} ConfFlatCartesian{a} CFC
```
This will make appropriate definitions including `CFCTensor`, `CFCVector` and `CFCOneForm`.  One should
use `macroexpand` if they are unsure exactly what this will define.
"""
macro generic_tensor_types(manifold, atlas, prefix)
    if !(atlas isa Symbol) && (atlas.head ≠ :curly)
        error("invalid atlas given for generic tensor type generator for atlas $atlas")
    end
    atlname = atlas isa Symbol ? atlas : atlas.args[1]
    params = atlas isa Symbol ? Symbol[] : atlas.args[2:end]
    holrepname = Symbol(string(prefix,"HolRep"))
    holrep = :($holrepname{$(params...)})
    orthrepname = Symbol(string(prefix,"OrthRep"))
    tensorname = Symbol(string(prefix,"Tensor"))
    vecname = Symbol(string(prefix,"Vector"))
    oneformname = Symbol(string(prefix,"OneForm"))
    metricname = Symbol(string(prefix,"Metric"))
    quote
        const $holrepname{$(params...)} = Representation{$manifold,$atlas,Holonomic}
        const $orthrepname{$(params...)} = Representation{$manifold,$atlas,Orthonormal}

        const $tensorname{$(params...),r,s,P,D} = GTensor{$manifold,$holrep,r,s,P,D}
        const $vecname{$(params...),r,s,P,D} = GVector{$manifold,$holrep,P,D}
        const $oneformname{$(params...),r,s,P,D} = GOneForm{$manifold,$holrep,P,D}

        const $metricname{$(params...)} = GMetric{$manifold,$holrep}
        
        function $vecname(p::Point{$manifold}, data::AbstractArray1) where {$(params...)}
            $vecname{$(params...)}(p, data)
        end
        function $oneformname(p::Point{$manifold}, data::AbstractArray) where {$(params...)}
            $oneformname{$(params...)}(p, data)
        end
        function $metricname(p::Point{$manifold}) where {$(params...)}
            $metricname{$(params...)}(p)
        end
    end |> esc
end
