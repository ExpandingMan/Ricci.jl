module deSitterImpl

using ..Ricci: @_submodule_imports, @generic_tensor_types
@_submodule_imports

using Infiltrator


# a is curvature parameter, horizon size
struct deSitter{a} <: Lorentzian{4} end

#====================================================================================================
    conformally flat       

Cartesian covers entire manifold except the surface but is singular at boundary
η > 0 and η < 0 are both part of the manifold but this surface is boundary
====================================================================================================#
struct ConfFlatCartesian{a} <: Atlas{deSitter{a}} end

@generic_tensor_types deSitter{a} ConfFlatCartesian{a} CFC


struct ConfFlatPolar1Patch{a} <: Atlas{deSitter{a}} end

@generic_tensor_types deSitter{a} ConfFlatPolar1Patch{a} CFP1



const CFCPoint{a} = Coord1Point{deSitter{a},ConfFlatCartesian{a}}
const CFP1Point{a} = Coord1Point{deSitter{a},ConfFlatPolar1Patch{a}}
#===================================================================================================#


#====================================================================================================
    static       

Cartesian only covers half the manifold, however the coordinate singularity at |x| = a
is an event horizon (to origin)
====================================================================================================#
struct StaticCartesian{a} <: Atlas{deSitter{a}} end

@generic_tensor_types deSitter{a} StaticCartesian{a} SC

struct StaticPolar1Patch{a} <: Atlas{deSitter{a}} end

@generic_tensor_types deSitter{a} StaticPolar1Patch{a} SP1
#===================================================================================================#


include("tensors.jl")


end
