#====================================================================================================
    \begin{conformally flat cartesian}
====================================================================================================#
@stable function metricdata(::ConfFlatCartesian{a}, x::AbstractArray1, pidx::Integer=1) where {a}
    η = x[4]
    k = (a/η)^2
    @SMatrix eltype(x)[
        k 0 0 0
        0 k 0 0
        0 0 k 0
        0 0 0 -k
    ]
end

@stable function invmetricdata(::ConfFlatCartesian{a}, x::AbstractArray1, pidx::Integer=1) where {a}
    η = x[4]
    km1 = (η/a)^2
    @SMatrix eltype(x)[
        km1 0 0 0
        0 km1 0 0
        0 0 km1 0
        0 0 0 -km1
    ]
end

function (g::CFCMetric{a})(u::Vector{deSitter{a}}, v::Vector{deSitter{a}}) where {a}
    x = data(Point(g), ConfFlatCartesian{a}())
    η = x[4]
    k = (a/η)^2
    udat = data(u, Representation(g))
    vdat = data(v, Representation(g))
    k * (udat ⋅ negatelast(vdat))
end

function (g::InvMetric{deSitter{a},<:CFCMetric{a}})(u::OneForm{deSitter{a}}, v::OneForm{deSitter{a}}) where {a}
    x = data(Point(g), ConfFlatCartesian{a}())
    η = x[4]
    km1 = (η/a)^2
    udat = data(u, Representation(g))
    vdat = data(v, Representation(g))
    km1 * (udat ⋅ negatelast(vdat))
end

@stable function OneForm(v::CFCVector{a}) where {a}
    x = data(Point(v), ConfFlatCartesian{a}())
    η = x[4]
    k = (a/η)^2
    vd = data(v)
    CFCOneForm(Point(v), k*negatelast(vd))
end

@stable function Vector(u::CFCOneForm{a}) where {a}
    x = data(Point(u), ConfFlatCartesian{a}())
    η = x[4]
    km1 = (η/a)^2
    ud = data(u)
    CFCVector(Point(u), km1*negatelast(ud))
end

@stable function ∂qhamiltonian_ondata(::CFCHolRep{a}, q, p) where {a}
    η = q[4]
    SVector{4,eltype(q)}(0, 0, 0, -2*η*minkowski_inner(p, p)/a)
end
#====================================================================================================
    \end{conformally flat cartesian}
====================================================================================================#

#====================================================================================================
    \begin{conformally flat polar 1 patch}
====================================================================================================#
@stable function metricdata(::ConfFlatPolar1Patch{a}, x::AbstractArray1, pidx::Integer=1) where {a}
    (r, θ, η) = (x[1], x[2], x[4])
    k = (a/η)^2
    @SMatrix eltype(x)[
        k 0 0 0
        0 k*r^2 0 0
        0 0 k*(r*cos(θ))^2 0
        0 0 0 -k
    ]
end

@stable function invmetricdata(::ConfFlatPolar1Patch{a}, x::AbstractArray1, pidx::Integer=1) where {a}
    (r, θ, η) = (x[1], x[2], x[4])
    km1 = (η/a)^2
    @SMatrix eltype(x)[
        km1 0 0 0
        0 km1/r^2 0 0
        0 0 km1/(r*cos(θ))^2 0
        0 0 0 -km1
    ]
end

function (g::CFP1Metric{a})(u::Vector{deSitter{a}}, v::Vector{deSitter{a}}) where {a}
    x = data(Point(g), ConfFlatPolar1Patch{a}())
    (r, θ, η) = (x[1], x[2], x[4])
    k = (a/η)^2
    ud = data(u, Representation(g))
    vd = data(v, Representation(g))
    k * (ud[1]*vd[1] + ud[2]*vd[2]*r^2 + ud[3]*vd[3]*(r*cos(θ))^2 - ud[4]*vd[4])
end

function (g::InvMetric{deSitter{a},<:CFP1Metric{a}})(u::OneForm{deSitter{a}}, v::OneForm{deSitter{a}}) where {a}
    x = data(Point(g), ConfFlatPolar1Patch{a}())
    (r, θ, η) = (x[1], x[2], x[4])
    km1 = (η/a)^2
    ud = data(u, Representation(g))
    vd = data(v, Representation(g))
    km1 * (ud[1]*vd[1] + ud[2]*vd[2]/r^2 + ud[3]*vd[3]/(r*cos(θ))^2 - ud[4]*vd[4])
end

@stable function Vector(u::CFP1OneForm{a}) where {a}
    x = data(Point(u), ConfFlatPolar1Patch{a}())
    (r, θ, η) = (x[1], x[2], x[4])
    km1 = (η/a)^2
    ud = data(u)
    dat = @SVector eltype(ud)[
        km1*ud[1], km1*r^2*ud[2], km1*(r*cos(θ))^2*ud[3], -km1*ud[4]
    ]
    CFP1Vector{a}(Point(u), dat)
end

@stable function OneForm(v::CFP1Vector{a}) where {a}
    x = data(Point(u), ConfFlatPolar1Patch{a}())
    (r, θ, η) = (x[1], x[2], x[4])
    k = (a/η)^2
    vd = data(v)
    dat = @SMatrix eltype(vd)[
        k*vd[1];;
        k*r^2*vd[2];;
        k*(r*cos(θ))^2*vd[3];;
        k*vd[4];;
    ]
    CFP1OneForm{a}(Point(v), dat)
end
#====================================================================================================
    \end{conformally flat polar 1 patch}
====================================================================================================#
