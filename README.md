# Ricci

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Ricci.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/Ricci.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/Ricci.jl/-/pipelines)

Computational differential geometry, with a particular emphasis on general relativity.

The intended functionality is similar to
[Manifolds.jl](https://github.com/JuliaManifolds/Manifolds.jl), except for the focus on GR.  In
particular, this package does not depend on embedding manifolds into higher dimensional manifolds.
