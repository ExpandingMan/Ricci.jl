using Ricci
using LinearAlgebra, StaticArrays, BenchmarkTools
using ADTypes
import DifferentiationInterface as DI
using StaticDiffInterface
import Enzyme, ForwardDiff

using Ricci: Vector

using Ricci.SchwarzschildImpl; const Sch = SchwarzschildImpl
using Ricci.SchwarzschildImpl: Schwarzschild, P1Metric, Polar1Patch, P1Point, P1Vector, P1OneForm
using Ricci.SchwarzschildImpl: Schild, SchPoint, SchMetric, SchVector, SchOneForm
using Ricci.SchwarzschildImpl: SchP1Point, SchP1Vector, SchP1OneForm
using Ricci.SchwarzschildImpl: Isotropic, IPoint, IVector, IOneForm


point_1() = IPoint{1.0}(@SVector(Float64[10,0,0,2]))

scalar_field_1() = ScalarField{Schwarzschild{1.0}}() do x
    data(x, Isotropic{1.0}())[4]^2
end

vector_field_1() = VectorField{Schwarzschild{1.0}}() do x
    xdat = data(x, Isotropic{1.0}())
    T = dataeltype(x)
    Vector(Isotropic{1.0}(), x, @SVector(T[0,0,0,xdat[4]^2])) |> normalize
end

oneform_field_1() = OneFormField{Schwarzschild{1.0}}() do x
    xdat = data(x, Isotropic{1.0}())
    T = dataeltype(x)
    OneForm(Isotropic{1.0}(), x, @SVector(T[0,0,0,xdat[4]^2])) |> normalize
end

function check_christoffel_1(diffbackend::AbstractADType)
    x = point_1()
    o = Ricci.christoffel1data(x; diffbackend)
    b = @benchmark Ricci.christoffel1data($x; diffbackend=$diffbackend)
    (b, o)
end

function check_riemann_1(diffbackend::AbstractADType)
    x = point_1()
    o = Ricci.riemanndata(x; diffbackend)
    b = @benchmark Ricci.riemanndata($x; diffbackend=$diffbackend)
    (b, o)
end

function check_case_1(diffbackend::AbstractADType)
    ϕ = scalar_field_1()
    x = point_1()
    o = ∇(ϕ, x; diffbackend)
    b = @benchmark ∇($ϕ, $x; diffbackend=$diffbackend)
    (b, o)
end

function check_case_2(diffbackend::AbstractADType)
    v = vector_field_1()
    x = point_1()
    o = ∇(v, x; diffbackend)
    b = @benchmark ∇($v, $x; diffbackend=$diffbackend)
    (b, o)
end

function check_case_3(diffbackend::AbstractADType)
    u = oneform_field_1()
    x = point_1()
    o = ∇(u, x; diffbackend)
    b = @benchmark ∇($u, $x; diffbackend=$diffbackend)
    (b, o)
end


