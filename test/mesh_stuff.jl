using LinearAlgebra, StaticArrays
using BenchmarkTools
using Ricci
using ForwardDiff, Enzyme, ADTypes
using StaticFiniteDiff; const SFD = StaticFiniteDiff
using GLMakie

using FastSymplecticIntegrators; const FSI = FastSymplecticIntegrators

using Ricci: Vector, Point, exponential

using Ricci.MinkowskiImpl; const Mink = MinkowskiImpl
using Ricci.MinkowskiImpl: Minkowski4, CartesianRep4, Cartesian4, Polar1Patch4, P1HolRep4
using Ricci.MinkowskiImpl: MC4Point, MC4Vector, MC4OneForm, MC4Metric
using Ricci.MinkowskiImpl: MP1Point4, MP1H4Vector, MP1H4OneForm, MP1H4Metric
using Ricci.MinkowskiImpl: MP1O4Vector, MP1O4OneForm

using Ricci.SchwarzschildImpl; const Sch = SchwarzschildImpl
using Ricci.SchwarzschildImpl: Schwarzschild, P1Metric, Polar1Patch, P1Point, P1Vector, P1OneForm
using Ricci.SchwarzschildImpl: Schild, SchPoint, SchMetric, SchVector, SchOneForm
using Ricci.SchwarzschildImpl: SchP1Point, SchP1Vector, SchP1OneForm
using Ricci.SchwarzschildImpl: IPoint, IVector, IOneForm

#using Ricci.deSitterImpl: deSitter, ConfFlatCartesian
#using Ricci.deSitterImpl: CFCPoint, CFCTensor, CFCVector, CFCOneForm, CFCMetric

using Ricci: ∂metricdata, christoffel1data, christoffel2data, ∂christoffel2data, riemanndata

using Ricci.Meshes
using Ricci.Meshes: UniformCoordMesh, SpatialMeshIndex, MeshPoint
using Ricci.Meshes: makeranges
using Ricci.Meshes: MeshedScalarField, MeshedVectorField, MeshedOneFormField


#====================================================================================================
this scheme basically seems to work

next step is to concoct somewhat more realistic examples
then will need to implement tensor fields which won't be much more complicated in principle,
but which will be way more complicated in implementation
====================================================================================================#

src1() = quote
    x₀ = IPoint{1.0}(zeros(4))
    x = IPoint{1.0}(Float64[0.5,-0.2,0,0])

    # this grid spacing is super ridiculous but we're still testing
    me = UniformCoordMesh(x₀, 10.0^(-2))

    xm = MeshPoint(me, x)

    v = VectorField{Schwarzschild{1.0}}() do x
        IVector(x, @SVector Float64[1,0,0,0])
    end

    ϕ = MeshedScalarField(me, makeranges(-100:100)) do x
        xdat = data(x)
        r = √(xdat[1]^2 + xdat[2]^2 + xdat[3]^2)
        1/r
    end
end

src2() = quote
    x₀ = IPoint{1.0}(zeros(4))
    x = IPoint{1.0}(Float64[0.5,-0.2,0,0])

    me = UniformCoordMesh(x₀, 10.0^(-2))

    xm = MeshPoint(me, x)

    v = MeshedVectorField(me, makeranges(-100:100)) do x
        xdat = data(x)
        IVector(x, @SVector([xdat[1], 2*xdat[2], 3*xdat[3], 0.0]))
    end

    u = MeshedOneFormField(me, makeranges(-100:100)) do x
        xdat = data(x)
        IOneForm(x, @SVector([xdat[1], 2*xdat[2], 3*xdat[3], 0.0]))
    end
end

src3() = quote
    #wtf = Ricci.∂data(ϕ, xm)
    
    (y, dy) = Ricci.data∂data(v, xm, mesh_difftype=AutoStaticFiniteDiff(SFD.Central{2}))
end

