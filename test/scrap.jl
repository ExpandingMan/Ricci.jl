using LinearAlgebra, StaticArrays
using BenchmarkTools
using Ricci
using ForwardDiff, Enzyme, ADTypes
using StaticDiffInterface; const SDI = StaticDiffInterface
using GLMakie

using FastSymplecticIntegrators; const FSI = FastSymplecticIntegrators

using Ricci: Vector, Point, exponential

using Ricci.MinkowskiImpl; const Mink = MinkowskiImpl
using Ricci.MinkowskiImpl: Minkowski4, CartesianRep4, Cartesian4, Polar1Patch4, P1HolRep4
using Ricci.MinkowskiImpl: MC4Point, MC4Vector, MC4OneForm, MC4Metric
using Ricci.MinkowskiImpl: MP1Point4, MP1H4Vector, MP1H4OneForm, MP1H4Metric
using Ricci.MinkowskiImpl: MP1O4Vector, MP1O4OneForm

using Ricci.SchwarzschildImpl; const Sch = SchwarzschildImpl
using Ricci.SchwarzschildImpl: Schwarzschild, P1Metric, Polar1Patch, P1Point, P1Vector, P1OneForm
using Ricci.SchwarzschildImpl: Schild, SchPoint, SchMetric, SchVector, SchOneForm
using Ricci.SchwarzschildImpl: SchP1Point, SchP1Vector, SchP1OneForm
using Ricci.SchwarzschildImpl: IPoint, IVector, IOneForm

#using Ricci.deSitterImpl: deSitter, ConfFlatCartesian
#using Ricci.deSitterImpl: CFCPoint, CFCTensor, CFCVector, CFCOneForm, CFCMetric

using Ricci: ∂metricdata, christoffel1data, christoffel2data, ∂christoffel2data, riemanndata


src1() = quote
    p = MC4Point(Float32[1,1,0,0])
    q = MC4Point(Float32[0,0,0,1])

    u = MC4Vector(p, Float32[1,0,0,1])
    v = MP1H4Vector(p, Float32[1,0,0,1])

    g = MC4Metric(p)
end

src2() = quote
    x = SchPoint{1.0}(Float64[5,0,0,6])

    M = typeof(Manifold(x))

    v0 = SchVector{1.0}(x, Float64[1,0,0,1]) |> normalize

    atl = Atlas(x)

    basis = ADMCoordBasis(Atlas(x))
    sco = SpacelikeComponents(basis)

    #n = conormal(timelike, x)
    n = VectorField{M}(x -> conormal(timelike, x))

    t = timevector(basis, x)

    g = Metric(x)

    γ = spatialmetric(x)
    γinv = invspatialmetric(x)
    
    nup = normal(timelike, x)
    ndown = conormal(timelike, x)
   
    N = temporalprojector(x)

    K = extcurvature(x)

    e = BasisVectorField{4}(atl)

    ϵ = LeviCivita(x)

    ϕ = ScalarField{Schwarzschild{1.0}}() do ξ
        sum(data(ξ, Schild{1.0}()))
    end

    v = VectorField{Schwarzschild{1.0}}() do ξ
        ξdat = data(ξ, Schild{1.0}())
        Vector(Schild{1.0}(), ξ, @SVector Float64[0, 0, 0, ξdat[4]^2])
    end

    u = OneFormField{Schwarzschild{1.0}}() do ξ
        ξdat = data(ξ, Schild{1.0}())
        OneForm(Schild{1.0}(), ξ, @SVector Float64[0, 0, 0, ξdat[4]^2])
    end

    t = TensorField{Schwarzschild{1.0},0,2}() do ξ
        ξdat = data(ξ, Schild{1.0}())
        Tensor{Schwarzschild{1.0},0,2}(ξ, @SMatrix Float64[0 0 0 0; 0 0 0 0; 0 0 0 0; 0 0 0 ξdat[4]^2])
    end
end

src3() = quote
    x = MC4Point(Float64[5,0,0,0])

    γ = spatialprojector(x)

    u = OneFormField{Minkowski4}() do x
        xdat = data(x)
        MC4OneForm(x, @SVector [xdat[1]^2, 0, 0, xdat[4]^2])
    end
end


