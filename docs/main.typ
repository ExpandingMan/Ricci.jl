#import "lib.typ": *

#let abstract = [
  Ricci.jl is a software package for computational differential geometry written in the Julia
  programming language with an emphasis on 4-dimensional Minkowskian manifolds suitable for
  numerical relativity calculations.
]

#show: doc => conf(
  title: [Ricci.jl],
  authors: "Michael Savastio",
  abstract: abstract,
  contents: true,
  page-numbers: "1",
  doc,
)

#TODO[This is very rough, and very far from done, for now just want to get in some notes on major points.]


#include("body.typ")
#include("appendix.typ")


#bibliography("refs.yml", full: true)
