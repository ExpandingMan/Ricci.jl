```@meta
CurrentModule = Ricci
```

# Ricci

Documentation for [Ricci](https://gitlab.com/ExpandingMan/Ricci.jl).

```@index
```

```@autodocs
Modules = [Ricci]
```
