using Ricci
using Documenter

DocMeta.setdocmeta!(Ricci, :DocTestSetup, :(using Ricci); recursive=true)

makedocs(;
    modules=[Ricci],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    sitename="Ricci.jl",
    format=Documenter.HTML(;
        canonical="https://ExpandingMan.gitlab.io/Ricci.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
