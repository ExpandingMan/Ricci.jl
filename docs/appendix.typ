#import "lib.typ": *


#show: appendix


#show rect: r => align(center, r)


#let smproj(a, b) = $lr(gamma^#a)_#b$

= The $3+1$ (ADM) Formalism
To integrate equations of motion in curved spacetime numerically, it is essential to choose a
particular basis of the tangent spaces.  The $3+1$ (ADM) formalism was developed to do this in a
covariant way, we present a brief review of the major points here for convenience.  We will largely
follow the presentation of @baumgarte.

== Fundamentals

Let $t : M -> RR$ be a timelike coordinate function meaning that it parameterizes a family of
disjoint spacelike 3-surfaces $Sigma_t$.  We define the closed 1-form

$ Omega_a := nabla_a t $

and the *lapse function* $alpha$ by

$ g^(a b) nabla_a t thin nabla_b t = - 1/alpha^2 $

The lapse function indicates the amount of proper time elapsed between $Sigma_t$ and $Sigma_(t +
delta t)$, and by construction $alpha > 0$.  Next we define the unit normal vector field to the
spacelike surfaces

$ n^a :=  -alpha Omega^a $<adm-normal>

By construction $n^a n_a = -1$.  The $-$ sign in @adm-normal is chosen so that $n^a$ points in the
direction of increasing $t$.  We can project tensors onto $Sigma_t$ by subtracting out the
components parallel to $n^a$.  To this end we define

$ gamma_(a b) := g_(a b) + n_a n_b $<spatial-metric>

We can see that $gamma_(a b)$ is the metric tensor on $Sigma_t$ by considering its action on tensors
the lie entirely within these surfaces (the $+$ in @spatial-metric occurs because $n^a$ is
timelike).  We therefore refer to it as the *spatial metric*.  The "inverse" spatial metric is
defined as

$ gamma^(a b) := g^(a c) g^(b d) gamma_(c d) = g^(a b) + n^a n^b $

Note however that $gamma^(a b)$ is not the true inverse of $gamma_(b c)$ in that

$ gamma^(a b) gamma_(b c) = lr(delta^a)_c + n^a n_c = lr(gamma^a)_c $

We also define the projection onto the timelike normal

$ lr(N^a)_b = -n^a n_b $

The *spatial gradient* operator is defined as

$ D_a T_(b_1 dots.c b_n) =
  lr(gamma_a)^c lr(gamma_(b_1))^(c_1) dots.c lr(gamma_(b_n))^(c_n) nabla_c T_(c_1 dots.c c_n)
$

Note that because computing the spatial gradient involves projecting _all_ of its indices, and
$gamma_(a b)$ does not commute with the gradient operator $nabla_a$, expressing gradients in terms
of $D_a$ should be expected to generate a lot of new terms.

$D_a$ is the true metric compatible gradient on tensors lying entirely within the hypersurfaces in
that it is compatible with the spatial metric $D_a gamma_(b c) = 0$, however be warned that the
product rule with respect to $D_a$ does not apply to tensors with components along $n^a$.

We define the *extrinsic curvature* as the Lie derivative of $gamma_(a b)$ along $n^a$
$ K_(a b) := - 1/2 cal(L)_bold(n) gamma_(a b) =
  -lr(gamma_a)^c lr(gamma_b)^d nabla_(paren.l c) n_(d paren.r)
$<extrinsic-curvature>

It can be shown that the spatial projection of $nabla_a n_b$ is already symmetric, so that the
symmetrization brackets in @extrinsic-curvature can be dropped.  As a result, we also have $K_(a b)
= - lr(gamma_a)^c lr(gamma_b)^d nabla_c n_d$. The extrinsic curvature $K^(a b)$ is also the
canonical momentum conjugate to the spatial metric $gamma_(a b)$ in the Einstein-Hilbert action.

Sometimes it is convenient to refer to the
*acceleration* of $n^a$

$ a_a := n^b nabla_b n_a = D_a log(alpha) $

in terms of which the extrinsic curvature can be written

$ K_(a b) = - nabla_a n_b - n_a a_b $

The trace of the extrinsic curvature is

$ K := g^(a b) K_(a b) = gamma^(a b) K_(a b) $

In order to write Einstein's equations with these conventions it will be convenient to introduce a
vector $t^a$ which is dual to the coordinate basis 1-form $dif t$, in that $t^a nabla_a t = 1$.
Since $n^a nabla_a t = 1/alpha$ we define
$ t^a := alpha n^a + beta^a $
where $beta^a$ is an arbitrary, purely spatial (i.e. $n^a beta_a = 0$) vector which we will refer to
as the *shift vector*.  The freedom to choose $beta^a$ can be thought of as reflecting a gauge
freedom.

The Riemann tensor on the submanifolds $Sigma_t$, which we will denote $r_(a b c d)$, can be defined
in terms of the spatial gradient operator $D_a$
$ lr(r_(a b c))^d v_d = 2 D_(bracket.l a) D_(b bracket.r) v_c $

A straightforward but tedious calculation yields $r_(a b c d)$ in terms of the Riemann tensor on
$M$, $R_(a b c d)$


$ r_(a b c d) + K_(a c) K_(b d) - K_(a d) K_(c b) =
  smproj(p, a) smproj(q, b) smproj(r, c) smproj(s, d) R_(p q r s)
$

We will denote by $r_(a b) = lr(r_(a c b))^c$ and $r = gamma^(a b) r_(a b)$ the spatial hypersurface
Ricci tensor and scalar, respectively.  By computing the gradient

$ D_(bracket.l a) K_(b bracket.r c) =
  -smproj(p, a) smproj(q, b) smproj(r, c) nabla_(bracket.l p) nabla_(q bracket.r) n_r
$

it follows from the definition of the Riemann tensor

$ D_(bracket.l b) K_(a bracket.r c) = smproj(p, a) smproj(q, b) smproj(r, c) n^s R_(p q r s) $

To derive numerically integrable evolution equations, one must isolate $partial_t$ terms.  To do
this, it is useful to note that in ADM coordinates
$ cal(L)_bold(t) = partial_t #h(6em) "(ADM coordinates)" $
This follows first from the general fact that the geometric terms cancel in Lie derivatives so that
one may replace $nabla_mu -> partial_mu$ and from the fact that, by construction, $t^mu =
(1,0,0,0)^upright(T)$ is constant in the ADM coordinates.


== Einstein's Equations
To write Einstein's equations in our space-time decomposition scheme, we will need to decompose the
stress-energy tensor 
$ rho :=& n^a n^b T_(a b) \
  S_a :=& -smproj(b, a) n^c T_(b c) \
  S_(a b) :=& smproj(c, a) smproj(d, b) T_(c d)
$

These correspond to the usual notions of energy density, momentum density and stress respectively.
We can therefore decompose $T_(a b)$ as
$ T_(a b) = S_(a b) + 2 n_(paren.l a) S_(b paren.r) + n_a n_b rho $

Writing Einstein's equations is now a straightforward matter but quite tedious.  For details we
refer the reader to @baumgarte, and we will suffice to simply state the result.

The first two equations are referred to as *constraint equations* because they involve _only_ the
spacelike projected derivative operators $D_a$ and therefore do not involve time evolution in our
chosen basis.

#boxeq[$
  r + K^2 - K_(a b) K^(a b) = 16 pi G rho
$]

#boxeq[$
  D_b lr(K^b)_a - D_a K = 8 pi G S_a
$]

Time derivatives can be expressed in a covariant way as Lie derivatives, in particular, by
construction $cal(L)_bold(t)$, the Lie derivative with respect to the vector field $t^a$, is the
same as $partial_t$ up to a gauge choice which is parameterized by $beta^mu$.

#boxeq[$
  (cal(L)_bold(t) - cal(L)_bold(beta)) K_(a b) = - D_a D_b alpha +
  alpha (r_(a b) - 2 K_(a c) lr(K^c)_b + K K_(a b)) -
  8 pi G alpha (S_(a b) - 1/2 gamma_(a b) (S - rho))
$]

#boxeq[$
  (cal(L)_bold(t) - cal(L)_bold(beta)) gamma_(a b) = - 2 alpha K_(a b)
$]


== Coordinate Expressions
It is natural to use a coordinate basis where $partial_t$ is the timelike basis vector.  Then our
definitions $Omega_a = nabla_a t$, $n_a = -alpha Omega_a$ imply
$ n_mu = vec(-alpha, 0, 0, 0) $

This, however, does not by itself uniquely determine the components $n^mu$.  For this we will use
that, by construction, $t^a Omega_a = -(t^a n_a)/alpha = 1$, which implies
$ n^mu = 1/alpha vec(1, -beta^i) $
where $beta^i$ are the three non-zero components of $beta^a$. #footnote[This is a rather egregious
  abuse of notation because we differentiate abstract indices such as $beta^a$ from coordinate
  indices such as $beta^i$ merely by starting at a different letter of the alphabet.  However, we
  entirely avoid using them together.  This sin is also committed in most of the literature.]
Recall that we have defined $beta^a$ by insisting that $beta^a n_a = 0$, so we can be confident that
it contributes $0$ to the time slot.  For the same reason we see that $gamma_(a b) beta^b = g_(a b)
beta^b = beta_b$.  Note, however, that the temporal component of $beta_a$ is in general non-zero.

It is now easy to see that the metric can be written
$ dif s^2 = - alpha^2 dif t^2 + gamma_(i j) (dif x^i + beta^i dif t)(dif x^j + beta^j dif t) $

Equivalently

$ g_(mu nu) = mat(-alpha^2 + beta_k beta^k, beta_i; beta_i, gamma_(i j)) #h(6em)
  g^(mu nu) = mat(-1/alpha^2, beta^i/alpha^2; beta^i/alpha^2, gamma^(i j) - (beta^i beta^j)/alpha^2)
$
Where $beta_i = gamma_(i j) beta^j$.  We can now see that the $beta_i$ are the spatial components of
$beta_a$, but we remind the reader that the temporal component of $beta_a$ need not vanish.

As we have been careful to formulate the $3+1$ formalism in a fully covariant way, we can write
$K_(a b)$, $D_a$ and the other objects in Einstein's equations simply by replacing the abstract
indices with the spatial coordinate component indices.

Note that we are also free to replace the Lie derivatives with respect to $bold(t)$ that appear in
Einstein's equations with $partial_t$ becuase the tensors they act on have only spatial components.
That is, the evolution equations can be written
$ partial_t K_(i j) =&
  - D_i D_j alpha + beta^k partial_k K_(i j) + K_(i k) partial_j beta^k + K_(k j) partial_i beta^k  
  + alpha (r_(i j) + K K_(i j) - 2 K_(i k) lr(K^k)_j) - \
  & 8 pi G alpha (S_(i j) - 1/2 gamma_(i j) (S - rho))
$

$ partial_t gamma_(i j) = - 2 alpha K_(i j) + D_i beta_j + D_j beta_i $

