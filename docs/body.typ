#import "lib.typ": *


= Introduction
#TODO[]

== Conventions
In this note we use a $(-,+,+,+)$ metric signature with units for which $c = 1$, and in cases where
we explicilty label the temporal coordinate we assign it an index of $0$.  The software package
Ricci.jl itself however uses a $(+,+,+,-)$ metric signature with the temporal component transposed
to index $4$.  This is done to conform to Julia's 1-based indexing system with the simplest possible
implementation.


= Coordinate Atlases
A coordinate atlas is represented by types of the `Atlas{M}` abstract type, the single type
parameter `M` being the manifold.  This type represents both an abstract coordinate atlas on the
manifold and a particular implementation of coordinates on the manifold.  Each coordinate atlas
represented by an `Atlas` has a number of coordinate patches which are known at compile time.  Such
a coordinate atlas need not be a true atlas in that it is not strictly required to cover the entire
manifold.

== Incomplete "Atlases"
The Ricci.jl API makes no guarantees about an incomplete atlas, in other words the user is
entirely responsible for dealing with singularities or other forms of incompleteness in an
incomplete atlas.  For example, the `Polar1Patch4{Minkowski4}` atlas implements conventional polar
spherical coordinates in the Minkowski spacetime.  As such it has the usual axial singularity, so
users should fully expect numerical instabilities near the axis as well as propagation of floating
point `NaN` or `Inf` resulting from computation of the metric components there.  By contrast the
`Polar2Patch4{Minkowski4}` is incomplete only at the origin, so internal Ricci.jl methods should at
least attempt to switch between patches appropriately away from the origin.


= Manifold Points
A point on a manifold `p in M` is represented with a `Point{M}` object.  These are to be considered
abstract objects not dependent on a specific coordinate system, however, since most implementations
store coordinate data explicitly in memory, we allow `Point` to define a default `Atlas`, which is
accessible via the methods `Atlas(p::Point)`.

== Point Coordinate Data
The coordinate data of a point is returned from the methods
```julia
data(p::Point, atl::Atlas)
```
which returns the coordinate data in the atlas described by `atl`.  Likewise
```julia
patchindex(p::Point, atl::Atlas)
```
returns the index of the atlas patch of the data returned by the corresponding `data` method for the
point.

For convenience, the methods `data(p::Point)` and `patchindex(p::Point)` return the coordinate data
and patch index in the default atlas for the point.


= Tensors
Tensors on the tangent spaces $T_p M$ are represented by objects of abstract type `Tensor{M,r,s}`.
Implementations of this abstract type are required to define `Point(t::Tensor)` returning the
`Point{M}` object which represents the point $p in M$ to which $T_p M$ belongs.  The integer
parameters `r` and `s` represent the number of up-index (contravariant) and down-index (covariant)
components of the tensor respectively.  In Ricci.jl all tensors are required to have indices ordered
in such a way that the first `r` are contravariant and the subsequent `s` are covariant.  For
example, the Riemann tensor can be represented in Ricci.jl as $lr(R^mu)_(nu tau sigma)$, in which
case it is of type `Tensor{M,1,3}` but _not_ as $lr(R_(mu nu tau))^sigma$.  This restriction allows
all tensor types to be parameterized with only the two integer type parameters `r` and `s` rather
than arbitrarily many boolean type parameters.  While a somewhat unfortunate restriction, this
drastically simplifies the type system and greatly reduces the needed for complicated `@generated`
functions.

We consider tensors to be generalized objects which include all vectors, dual vectors and
differential forms.  For example, vectors are represented by `Tensor{M,1,0}` while 1-forms (dual
vectors) are represented by `Tensor{M,0,1}`.

To specify the component data of a tensor one must provide a coordinate system in the form of an
`Atlas` as well as a basis type.  For the latter Ricci.jl provices `BasisType` objects.  As of
writing, the only allowed basis types are `Holonomic()` and `Orthonormal()`.  The bases these define
satisfy the usual conditions, that is
$ [ e^a_mu, e^b_nu ] = 0 $
for the holonomic basis and
$ eta_(a b) e^a_mu e^b_nu = g_(mu nu) $
for the orthonormal basis.  Holonomic bases are preferred by default where applicable.

The components of a tensor on $T_p M$ can be fully determined by specifying a `Representation`
object.  These are simple convenience wrappers for combining the `Atlas` and `BasisType`.

As with points, most tensor implementations work by representing coordinate components explicitly in
memory.  Therefore, as a convenience and in analogy to `Point` and `Atlas`, we allow `Tensor`
objects to define a default `Representation`.

== Tensor Data
The component data of a tensor is returned by the methods
```julia
data(t::Tensor, rep::Representation)
```
where `rep` is the `Representation` the returned components should appear in.  Tensor
implementations must guarantee that they return their data in their default `Representation`, but
other methods of this must rely on coordinate transformations and may not be defined.


= Diffeomorphisms


